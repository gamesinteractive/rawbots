# Rawbots

## Word of warning first before you begin.

This fork is only allowed to live so long as the AGPL license and the root commit of the master branch is still from the forked repository.
Under, **NO** circumstance you are allowed to : 

- ``push -f`` to overwrite the root commit.

- modify the AGPL license in any way, shape, or form.

- remove the following from the members list.

    - @Cupid_The_Conqueror

    - @Darth_Dan

    - @MarvinRawbots

    - @maciekniewielki

    - @pressureline

    - @z26

It is in our best interest to keep our relationship with the Open Source community active and thriving.
