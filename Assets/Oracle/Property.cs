using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Oracle {

    public interface IProperties {

        void ForProperty< T > ( string name, System.Action< T > action );

        void SetProperty ( string name, object value );

        Property RemoveProperty ( string name );
    }

    public class Property {

        public Property () {
        }

        public Property ( string name, object value ) {
            Name = name;
            this.value = value;
        }

        public string Name {
            get { return name; }
            set { name = value; }
        }

        public object Value {
            get { return value; }
            set {
                Debug.Assert(
                    value.GetType() == typeof( string ) ||
                    value.GetType() == typeof( int ) ||
                    value.GetType() == typeof( float ) );
                this.value = value;
            }
        }

        public override string ToString () {
            return ". " + name + " " + value;
        }

        string name = string.Empty;
        object value;
    }
}
