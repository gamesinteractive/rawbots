using UnityEngine;
using System.Collections;

public class Antenna : MonoBehaviour
{

    //public Renderer ledRenderer;
    //int ticks = 3;
    //float t = 0.02f;
    //Color offColor = new Color( 0.5f, 0.5f, 0.5f, 1 );

    //public IEnumerator VisualFeedBack ( Color freq ) {
    //    for ( int i = 0; i < ticks; ++i ) {
    //        ledRenderer.material.SetColor( "_Color", offColor );
    //        yield return new WaitForSeconds(t);
    //        ledRenderer.material.SetColor( "_Color", freq );
    //        yield return new WaitForSeconds(t);
    //    }
    //}

    void Update()
    {

    }

    void OnDrawGizmos()
    {
        var up = GetComponent<PhysicalObject>().Up();
        Gizmos.DrawLine(transform.position, transform.position + up * 3f);
    }
}