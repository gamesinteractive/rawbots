using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Oracle;
using Operants;

public class Bot : MonoBehaviour {

    void Start () {
        var xform = gameObject.GetComponent< XForm >();
        if ( xform == null ) {
            xform = gameObject.AddComponent< XForm >();
        }
        Gaia.Instance().Load( xform );
    }

}
