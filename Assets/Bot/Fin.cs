using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class Fin : MonoBehaviour {

    public Renderer fin;
    public GameObject fx;
    Vector3 drag = Vector3.zero;

    public BulletRigidBody body;
	new Transform transform;
	
	void Awake(){
		
		transform = gameObject.transform;
	}
    void FixedUpdate () {
        drag = Vector3.zero;
        var right = transform.up;
        var left = -transform.up;
        var rightDot = Vector3.Dot( body.GetVelocity(), right );
        var leftDot = Vector3.Dot( body.GetVelocity(), left );
		//Debug.Log(Vector3.Angle(body.GetVelocity(),right) + " "+Vector3.Angle(body.GetVelocity(),left));
        if ( rightDot > 0 ) {
            drag = Vector3.Dot( body.GetVelocity() * 10.0f, right ) * left;
			
        }
        else if ( leftDot > 0 ) {
            drag = Vector3.Dot( body.GetVelocity() * 10.0f, left ) * right;
        }
        if(!GetComponent<Part>().isProtected){
			//Debug.Log(drag.magnitude);
			//drag = Vector3.ClampMagnitude(drag,500);
			Debug.DrawLine(transform.position,transform.position+ drag,Color.red);
			Debug.DrawLine(transform.position,transform.position+ body.GetVelocity(),Color.white);
            body.AddForce( drag,Vector3.zero);
        }
    }

}
