using UnityEngine;
using System.Collections;
using Operants;
using System.Linq;

public class PlasmaCannon : MonoBehaviour {

    public GameObject hitFlashAsset;
    public MuzzleFlash muzzleFlash;
    public Transform muzzle;
    public Transform spawnPoint;
    public float destroyTime = 5.0f; //Reverted. the standard value is 
    public PartProxy inputProxy;
    FluxCapacitor charger;
    float maxForce = 24000; //Set to XFM 0.5 value - PL
    float minForce = 3000; //Set to XFM 0.5 value - PL
    float lastTimeFired = 0;
    float minFireDelay = 0.45f;
    public System.Action<float> produceShootForce = f => {};

    public void TryShoot () {
        //Only accept flux caps connected to your bots as an energy source. SHOULDNT DO THIS EACH FRAME
        if ( inputProxy is Operants.FluxCapacitorProxy ) {
            var fcProxy = (FluxCapacitorProxy)inputProxy;
            charger = fcProxy.fluxCapacitor;
        } else {
            return;
        }

        var cannonProxy = GetComponent<NodeView>().proxy as PartProxy;
        if (!cannonProxy.PhysicallyLinked().Contains(inputProxy)) {
            return;
        }

        //var mincharge = 48;//50 correspond to 1 cap, set it slightly below to be sure.

        if ( Time.time - lastTimeFired > minFireDelay) {

            lastTimeFired = Time.time;
            var charge = charger.DecreaseCharge(charger.maxCharge);
            Flash();
            var recoil = -transform.up * charge / 0.04f; //Set to XFM 0.5 value - PL
            var bulletRigidbody = GetComponent<BulletRigidBody>();
            bulletRigidbody.AddForce(recoil,Vector3.zero);
            var shot = GameObject.Instantiate( Resources.Load( "FX/PlasmaBlast" ),//pline told me this line allows to shoot anything, even invalid objects -z26
                spawnPoint.position, transform.rotation ) as GameObject;
            
            var plasmaBlast = shot.GetComponent< PlasmaBlast >();
            plasmaBlast.energy = charge;
            var shootForce = transform.up * Util.MapValue0X( charge, 1000.0f, minForce, maxForce ); //Set to XFM 0.5 value - PL
            plasmaBlast.bulletRigidbody.AddForce(shootForce,Vector3.zero);
            plasmaBlast.bulletRigidbody.SetVelocity(bulletRigidbody.GetVelocity());
            plasmaBlast.initialPosHolder = spawnPoint;
            produceShootForce(shootForce.magnitude);
        }
    }

    void Flash () {
        muzzleFlash.Show();
    }
}
