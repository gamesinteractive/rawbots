using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class PressurePlate : MonoBehaviour {

    public BulletRigidBody press;
    public BulletRigidBody pressurePlate;
    public Transform top;
    public Transform bottom;
    public float force = 0.1f;
    public PhysicalObject physicalObject;



//    void OnMouseDownCustom ( TouchData touch ) {
//        if ( DevTools.IsOptionEnabled( DevTools.Options.PressurePlateClick ) ) {
//            StartCoroutine( "DevToolsClick" );
//        }
//    }
//
//    IEnumerator DevToolsClick () {
//        while ( true ) {

//            press.AddForce( -press.transform.up * force * 1000 );
//            yield return new WaitForFixedUpdate();
//        }
//    }
//
//    void OnMouseUpCustom ( TouchData touch ) {
//        StopCoroutine( "DevToolsClick" );
//    }

    public float GetPosition () {
        var total = ( bottom.position - top.position ).magnitude;
        var position = ( press.transform.position - top.position ).magnitude;
        return Mathf.Clamp( position / total, 0, 1 );
    }
}
