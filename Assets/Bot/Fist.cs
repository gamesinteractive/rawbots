using UnityEngine;
using System.Collections;
using Operants;

//class by z26, based on Part.cs and PlasmaBlast.cs
//TODO: Add distinct visual/audio effect, right now there's no signs damage has been dealt.
//Also, I havent tested or balanced this thing a ton yet.

public class Fist : MonoBehaviour {

    public BulletRigidBody bulletRigidbody;
    public float speedThreshold = 25;
    public float dmgMultiplier = 25f;
    float minDelay = 0.5f;
    float timer = 0;
    public float lastHit = 0;
    
    void Start () {
        bulletRigidbody.AddOnCollisionDelegate(OnCollision);
        //Invoke("test", 1f);
    }

    public void OnCollision ( Vector3 relativeVelocity,BulletRigidBody other,Vector3 contactPoint,Vector3 lineOfAction ) {
        
        float sqrSpeed = relativeVelocity.sqrMagnitude;
        float damage = GetDamage(sqrSpeed);

            //if treshold is exceeded, call OnHit on other part
        if ( damage > 0 && Time.time - timer > minDelay) {
            //I used time.time like the former devs, but unity manual says to avoid calling it every frame.
            //The AND should stop that from happening, right?
            other.gameObject.SendMessage( "OnHit", damage, SendMessageOptions.DontRequireReceiver );
            GameObject.Instantiate( Resources.Load( "FX/BlastImpact" ), contactPoint, transform.rotation );
            timer = Time.time; //this is to avoid rapid damage to happen several frame, just in case.
            if (other.gameObject.GetComponent<NodeView>().proxy is PartProxy) {
                lastHit = damage;
            }
            
  
            
            
        }
    }

    public float PredictDamage() {
    return GetDamage(bulletRigidbody.GetVelocity().sqrMagnitude);
    }

    float GetDamage(float sqrSpeed) {

        float speed = 1.0f / Util.FastInvSqrt( sqrSpeed );
        if (speed > speedThreshold) {
            return speed * dmgMultiplier;
        } else {
            return 0;
        }
    }
    
    
}
