using UnityEngine;
using System.Collections;

public class PointLight : MonoBehaviour {

    public Renderer bulb;
    public Transform halo;
    public new Light light;
    public float intensity = 0;
    public Color offColor;
    Color bulbColor;
    Material bulbMaterial;

    void Awake () {
        bulbColor = Color.white;
        bulbMaterial = bulb.material;
        SetIntensity( intensity );
        SetTintColor( offColor );
    }

    public void SetIntensity ( float value ) {
        intensity = Mathf.Clamp01( value );
        var scale = Vector3.one * 20;
        halo.localScale = scale * intensity;
        light.intensity = intensity * 8;

        var hsb = new HSBColor( bulbColor );
        hsb.s *= 0.75f;
        hsb.b *= 0.85f;
        hsb = HSBColor.Lerp( new HSBColor( offColor ), hsb, value );
        bulbMaterial.SetColor( "_TintColor", hsb.ToColor() );
    }

    public void SetTintColor ( Color bulbColor ) {
        var sum = bulbColor.r + bulbColor.g + bulbColor.b;
        var offSum = offColor.r + offColor.g + offColor.b;
        if ( sum > offSum ) {
            light.color = bulbColor;
            this.bulbColor = bulbColor;
            SetIntensity( intensity );
        }
    }

}
