using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class HexTrigger : MonoBehaviour {

    public Transform spawnPoint;
    public GameObject selectedGhost;
    private string type = "";
    public Operants.HexEarthProxy.Faces face;

    void Awake () {
        selectedGhost = default(GameObject);
    }

    public void Show ( string type, float angle ) {

        if ( this.type != type  ) {

            Hide();
            selectedGhost = ( GameObject )Instantiate( Resources.Load( "Ghosts/" + type ) );
            selectedGhost.transform.parent = spawnPoint.transform;
            selectedGhost.transform.position = spawnPoint.transform.position;
            selectedGhost.transform.rotation = spawnPoint.transform.rotation;
            this.type = type;

        }
        selectedGhost.transform.localEulerAngles = new Vector3( 0, angle, 0 );
    }

    public void Hide () {
        if ( selectedGhost != default(GameObject) ) {
            Destroy( selectedGhost );
            type = "none";
        }
    }
}
