using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class IslandBot : MonoBehaviour {

    static public int seed = 0;
    public GameObject hexTile;
    public List< GameObject > rocks;
    public List< Light > lights;


    void Start(){
        //StartCoroutine(CreateIsland());
    }

    IEnumerator CreateIsland () {
        yield return new WaitForSeconds(2);
        if ( seed == 0 ) {
            Random.seed = seed = 102;
        }
        var tileRands = new Queue< int >();
        for ( var i = 0; i < 20000; ++i ) {
            tileRands.Enqueue( Random.Range( 0, 10 ) );
        }

        var scale = 1;
        var stride = new Vector3( 70.391f, 42, 81.283f ) * scale;
        var wide = 4;

//        Operants.PartProxy partProxy = GetComponent<NodeView>().proxy as Operants.PartProxy;
//        int id_counter = 0;
        for ( var x = -wide; x <=wide; ++x ) {
            for ( var z = -wide; z <=wide; ++z ) {

                if ( Mathf.Abs( x ) > 0.5f * wide && Mathf.Abs( z ) > 0.5f * wide ) {
                    continue;
                }

                bool yHasFloor = false;

                for ( var y = -4; y <= 3; ++y ) {

                    var position = new Vector3( x * stride.x, y * stride.y, z * stride.z );
                    if ( x % 2 != 0 ) {
                        position += new Vector3( 0, 0, stride.z / 2 );
                    }

                    if ( tileRands.Dequeue() > 6 ) {
                        if ( yHasFloor ) {
                            for ( var i = 0; i < Random.Range( 0, 4 ); ++i ) {
                                /*
                             var rock = Instantiate( rocks[ Random.Range( 0, rocks.Count ) ] ) as GameObject;
                             rock.transform.parent = transform;
                             rock.transform.localPosition = position + 
                                 Vector3.forward * Random.Range( -16, 16 ) +
                                 Vector3.right * Random.Range( -16, 16 ) + 
                                 Vector3.up * 0;
                             rock.transform.localRotation = Quaternion.Euler( -90, 0, 0 );
                             rock.transform.localScale *= Random.Range( 1, 25 );
                             rock.transform.parent = null;
                             */
                            }

//                            var light = Instantiate( lights[ Random.Range( 0, lights.Count ) ] ) as Light;
//                            light.transform.parent = transform;
//                            light.transform.localPosition = position + Random.onUnitSphere * Random.Range( -5, 5 );
//                            light.transform.localRotation = Quaternion.identity;
                        }
                        continue;
                    }

                    yHasFloor = true;

//                    var tile = Instantiate( hexTile ) as GameObject;
//                    tile.transform.parent = transform;
//                    tile.transform.localPosition = position;
//                    tile.transform.localRotation = Quaternion.identity;

                    //TODO: restore load for island
//                    partProxy.bot.LoadTemplate( "HexEarth", "earth_block_" + id_counter++, position + Vector3.up, Vector3.zero );
                    yield return 0;
                }
            }
        }
        yield return 0;
//        partProxy.bot.Rebuild();

    }
 
}
