using UnityEditor;
using UnityEngine;

public class FindMissingScripts : EditorWindow {

    [MenuItem("Window/FindMissingScripts")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(FindMissingScripts));
    }

    public void OnGUI()
    {
     if (GUI.Button(new Rect(0.0f, 60.0f, 100.0f, 20.0f), "Find Missing Scripts"))
     {
        GameObject[] go = Selection.gameObjects;
        foreach (GameObject g in go)
        {

            Component[] components = g.GetComponents<Component>();
            for (int i = 0; i < components.Length; i++)
            {
                if (components[i] == null)
                {
                    Debug.Log(g.name + " has an empty script attached in position: " + i);
                }
            }

        }
     }
  } 
}