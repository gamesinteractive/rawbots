using UnityEngine;
using UnityEditor;
using System.Collections;


[CustomEditor(typeof(BulletRigidBody))]
    public class BulletEditor : Editor {
        public override void OnInspectorGUI(){
        base.OnInspectorGUI();
        var bulletRigidbody = (BulletRigidBody)target;
        bulletRigidbody.mask = (BulletLayers) EditorGUILayout.EnumMaskField("mask",bulletRigidbody.mask);
        if (GUI.changed){
            EditorUtility.SetDirty (target);
        }
    }
}


