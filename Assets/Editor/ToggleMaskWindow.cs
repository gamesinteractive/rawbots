using UnityEngine;
using UnityEditor;

public class ToggleMaskWindow : EditorWindow {

    public static uint permissions;
    bool connect = false;
    bool disconnect = false;
    bool edit = false;
    bool drag = false;
    string readPasscode = "";
    string setPasscode = "";
    uint readPermission = 0;

    [MenuItem("Security/Toggle Permissions")]
    public static void ShowWindow () {
        EditorWindow.GetWindow( typeof( ToggleMaskWindow ) );
    }

    void OnGUI () {
        GUILayout.Label( "Select Permissions", EditorStyles.boldLabel );
        connect = EditorGUILayout.Toggle( "Connect", connect );
        disconnect = EditorGUILayout.Toggle( "Disconnect", disconnect );
        edit = EditorGUILayout.Toggle( "Edit", edit );
        drag = EditorGUILayout.Toggle( "Drag", drag );

        GUILayout.BeginHorizontal( GUILayout.Width( 300 ) );
        GUILayout.Label( "Read Passcode " + readPasscode, EditorStyles.boldLabel );
        GUILayout.Label( "Read Permissions " +
            System.String.Format( new BinaryFormatter(), "{0:B}", System.Convert.ToByte( readPermission ) )
            , EditorStyles.boldLabel );
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal( GUILayout.Width( 250 ) );
        GUILayout.Label( "Passcode to be Set ", EditorStyles.boldLabel );
        setPasscode = GUILayout.TextField( setPasscode, 4, GUILayout.Width( 100 ) );
        GUILayout.EndHorizontal();
    }

    public void Update () {
        Repaint();
        readPasscode = Sec.readPassCode;
        Sec.setPasscode = setPasscode;
        readPermission = Sec.editorReadMask;

        if ( connect ) {
            Sec.editorMask = Sec.SetPermissionOn( Sec.editorMask, Sec.Permission.connect );
        }
        else {
            Sec.editorMask = Sec.SetPermissionOff( Sec.editorMask, Sec.Permission.connect );
        }
        if ( disconnect ) {
            Sec.editorMask = Sec.SetPermissionOn( Sec.editorMask, Sec.Permission.disconnect );
        }
        else {
            Sec.editorMask = Sec.SetPermissionOff( Sec.editorMask, Sec.Permission.disconnect );
        }
        if ( edit ) {
            Sec.editorMask = Sec.SetPermissionOn( Sec.editorMask, Sec.Permission.edit );
        }
        else {
            Sec.editorMask = Sec.SetPermissionOff( Sec.editorMask, Sec.Permission.edit );
        }
        if ( drag ) {
            Sec.editorMask = Sec.SetPermissionOn( Sec.editorMask, Sec.Permission.drag );
        }
        else {
            Sec.editorMask = Sec.SetPermissionOff( Sec.editorMask, Sec.Permission.drag );
        }
    }
}