using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;
 
public class DevToolsEditor : ScriptableObject {

    [UnityEditor.MenuItem ( "Dev Tools/Toggle God Mouse")]
    static void MenuDevToolsToggleGodMouse () {
        DevTools.ToggleOption( DevTools.Options.GodMouse );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Toggle HitDamageSuppressed")]
    static void MenuDevToolsToggleHitDamageSuppressed () {
        DevTools.ToggleOption( DevTools.Options.HitDamageSuppressed );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Toggle DisruptiveDelete")]
    static void MenuDevToolsToggleDisruptiveDelete () {
        DevTools.ToggleOption( DevTools.Options.DisruptiveDelete );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Toggle CameraFollowDistanceUnlimited")]
    static void MenuDevToolsToggleCameraFollowDistanceUnlimited () {
        DevTools.ToggleOption( DevTools.Options.CameraFollowDistanceUnlimited );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Toggle CameraCycleFPS")]
    static void MenuDevToolsToggleCCameraCycleFPS () {
        DevTools.ToggleOption( DevTools.Options.CameraCycleFPS );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Take Screenshot")]
    static void MenuDevToolsTakeScreenShot () {
        Debug.Log("screenshot");
        ScreenCapture.CaptureScreenshot("screenShot ("+ Time.time+").jpg",10);
    }

    [UnityEditor.MenuItem ( "Dev Tools/Clear Camera Unlocks")]
    static void MenuDevToolsToggleClearCameraUnlocks () {
        var gaia = Gaia.Instance();
        if ( gaia == default( Gaia ) ) {
            Debug.LogError( "Gaia needs to be loaded." );
        }
        else {
            var unlocks = gaia.entities[ "unlocked" ].Relationships().Where( r => r.Predicate == "was" ).ToList();
            foreach ( var unlock in unlocks ) {
                unlock.Break();
            }
            Debug.Log( "Cleared " + unlocks.Count + " camera unlocks." );
        }
    }

    [UnityEditor.MenuItem ( "Dev Tools/Toggle MusicSuppressed")]
    static void MenuDevToolsToggleMusicSuppressed () {
        DevTools.ToggleOption( DevTools.Options.MusicSuppressed );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Toggle DayCycleSuppressed")]
    static void MenuDevToolsToggleDayCycleSuppressed () {
        DevTools.ToggleOption( DevTools.Options.DayCycleSuppressed );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Toggle PressurePlateClick")]
    static void MenuDevToolsTogglePressurePlateClick () {
        DevTools.ToggleOption( DevTools.Options.PressurePlateClick );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Toggle HexEarthEdit")]
    static void MenuDevToolsToggleHexEarthEdit () {
        DevTools.ToggleOption( DevTools.Options.HexEarthEdit );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Toggle AskForPasscodeSupressed")]
    static void MenuDevToolsToggleAskForPasscodeSupressed () {
        DevTools.ToggleOption( DevTools.Options.AskForPasscodeSupressed );
    }
    
    [UnityEditor.MenuItem ( "Dev Tools/Toggle CheckPermissionSupressed")]
    static void MenuDevToolsToggleCheckPermissionSupressed () {
        DevTools.ToggleOption( DevTools.Options.CheckPermissionSupressed );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Toggle BotGenerator")]
    static void MenuDevToolsToggleBotGeneratorSupressed () {
        DevTools.ToggleOption( DevTools.Options.BotGenerator );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Clear PlayerPrefs")]
    static void MenuDevToolsClearPlayerPrefs () {
        PlayerPrefs.DeleteAll();
        Debug.Log( "PlayerPrefs cleared" );
    }

    [UnityEditor.MenuItem ( "Dev Tools/Clear Passcodes Keychain")]
    static void MenuDevToolsClearPasscodes () {
        var gaia = Gaia.Instance();
        if ( gaia == default( Gaia ) ) {
            Debug.LogError( "Gaia needs to be loaded." );
        }
        else {
            Entity settingsCodes;
            if ( gaia.entities.TryGetValue( "settings_codes", out settingsCodes ) ) {
                foreach ( var codes in settingsCodes.Tails( p => p == "as" ).ToList() ) {
                    codes.SetProperty( "codes", "#0000" );
                }
            }
            Debug.Log( "Passcodes cleared" );
        }
    }

    [UnityEditor.MenuItem ( "Dev Tools/Deprecate Orphans")]
    static void MenuDevToolsDeprecateOrphans () {
        var gaia = Gaia.Instance();
        if ( gaia == default( Gaia ) ) {
            Debug.LogError( "Gaia needs to be loaded." );
        }
        else {
            Debug.Log( "If you seen this message, you have been selected for implementing this feature" );
            var allEntities = gaia.entities.Values.ToList();
            var visitedList = new Dictionary<Entity,int>();

            for ( int i = 0; i< allEntities.Count; i++ ) {
                var entity = allEntities[ i ];
                var isInstance = entity.Heads( p => p == "as" ).Any();
                if ( isInstance ) {
                    visitedList.Clear();
                    var isConnectedToPartOrSettings = IsConnectedToPartOrSettings( entity, visitedList );
                    if ( !isConnectedToPartOrSettings ) {
                        entity.deprecated = true;
                    }
                }
            }
        }
    }

    static bool IsConnectedToPartOrSettings ( Entity entity, Dictionary<Entity,int> visited ) {
        bool partFound = false;
        if ( visited.ContainsKey( entity ) ) {
            return false;
        }
        visited.Add( entity, 0 );
        foreach ( var r in entity.Relationships().Where( r =>
                    r.Predicate == "input_for" ||
                    r.Predicate == "output_for" ||
                    r.Predicate == "slot_for" ||
                    r.Predicate == "local_to" ||
                    r.Predicate == "consumes" ) ) {
            var e = r.Head == entity ? r.Tail : r.Head;
            var isPart = false;
            var isSettings = false;
            e.ForInferredHead( "part", Util.AsIs, (p) => {
                isPart = true;
            } );
            e.ForInferredHead( "settings", Util.AsIs, (p) => {
                isSettings = true;
            } );
            if ( isPart || isSettings ) {
                partFound = true;
                break;
            }
            else if ( IsConnectedToPartOrSettings( e, visited ) ) {
                partFound = true;
                break;
            }
        }
        return partFound;
    }
}
