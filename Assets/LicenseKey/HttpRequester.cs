using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.IO;
using System.Security.Cryptography;

public class HttpsRequester {
    static string SuperSecretKey = "rawbots3110";
    public static string GenerateHMAC(byte[] data)
    {
        System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
        HMACSHA1 hasher = new HMACSHA1(encoding.GetBytes(SuperSecretKey));
        byte[] postDataHMAC = hasher.ComputeHash(data);
        return BitConverter.ToString(postDataHMAC).Replace("-", string.Empty).ToLower();
    }
    public static string GenerateHMAC(string msg)
    {
        System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
        byte[] data = encoding.GetBytes(msg);
        return GenerateHMAC(data);
    }	
}