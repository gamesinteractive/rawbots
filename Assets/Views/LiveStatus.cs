using UnityEngine;
using System.Collections;

public partial class LiveStatus : LiveWidget {

    public string label;
    public Color color;
    public float time = float.MaxValue;
    public string value = "";
    public TextMesh labelMesh;
    public TextMesh valueMesh;

    void Start () {
    }

    void Update () {

        if ( Time.time > time ) {
            Destroy( gameObject );
            return;
        }

        WidgetUpdate();

        if ( !isVisible ) {
            return;
        }

        labelMesh.text = label;
        valueMesh.text = value;
        labelMesh.GetComponent<Renderer>().material.color = color;
    }
}
