using UnityEngine;
using System.Collections;

public class TimeLineView : MonoBehaviour {

    public GameObject buttonActive;
    public GameObject buttonDown;
    public GameObject buttonOver;
    public GameObject buttonInActive;
    public GameObject keyActive;
    public GameObject keyInActive;
    public GameObject RecDownButton;
    public GameObject RedNormal;
    public GameObject RecOver;
    public GameObject[] activeButtons;
    public GameObject[] downButtons;
    public GameObject[] overButtons;
    public GameObject[] inActiveButtons;
    public GameObject[] activeKeys;
    public GameObject[] inactiveKeys;
    public int[] hola;

    void Start () {

        buttonActive.SetActive(false);
        buttonDown.SetActive(false);
        buttonOver.SetActive(false);
        buttonInActive.SetActive(false);
        keyActive.SetActive(false);
        keyInActive.SetActive(false);

        RecDownButton.SetActive(false);
        RecOver.SetActive(false);


        activeButtons = new GameObject[8];
        activeKeys = new GameObject[8];
        inActiveButtons = new GameObject[8];
        inactiveKeys = new GameObject[8];

        for ( int i = 0; i< 8; i++ ) {
            activeButtons[ i ] = ( GameObject )Instantiate( buttonActive );
            activeButtons[ i ].transform.parent = gameObject.transform;
            activeButtons[ i ].transform.localPosition = Vector3.zero;
            activeButtons[ i ].transform.localEulerAngles = new Vector3( -90, 0, 45 * i + 45f / 2 );
            activeButtons[ i ].SetActive(false);

            activeKeys[ i ] = ( GameObject )Instantiate( keyActive );
            activeKeys[ i ].transform.parent = gameObject.transform;
            activeKeys[ i ].transform.localPosition = Vector3.zero;
            activeKeys[ i ].transform.localEulerAngles = new Vector3( -90, 0, 45 * i + 45f / 2 );
            activeKeys[ i ].SetActive(false);

            inactiveKeys[ i ] = ( GameObject )Instantiate( keyInActive );
            inactiveKeys[ i ].transform.parent = gameObject.transform;
            inactiveKeys[ i ].transform.localPosition = Vector3.zero;
            inactiveKeys[ i ].transform.localEulerAngles = new Vector3( -90, 0, 45 * i + 45f / 2 );


            inActiveButtons[ i ] = ( GameObject )Instantiate( buttonInActive );
            inActiveButtons[ i ].transform.parent = gameObject.transform;
            inActiveButtons[ i ].transform.localPosition = Vector3.zero;
            inActiveButtons[ i ].transform.localEulerAngles = new Vector3( -90, 0, 45 * i + 45f / 2 );
        }

    }

    public void ToggleFrameView ( int index, bool viewState ) {
        activeButtons[ 7 - index ].SetActive(viewState);
        inActiveButtons[ 7 - index ].SetActive(!viewState);
    }

    public void ToggleKeyView ( int index, bool viewState ) {
        activeKeys[ 7 - index ].SetActive(viewState);
        inactiveKeys[ 7 - index ].SetActive(!viewState);
    }

    public void ToogleRecButton ( int state ) {

        if ( state == 0 ) {
            RecDownButton.SetActive(false);
            RecOver.SetActive(true);
        }
        else if ( state == 1 ) {
            RecDownButton.SetActive(true);
            RecOver.SetActive(false);
        }else if ( state == 2 ) {
            RecDownButton.SetActive(false);
            RecOver.SetActive(false);
        }


    }

}
