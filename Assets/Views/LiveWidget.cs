using UnityEngine;
using System.Collections;

public partial class LiveWidget : MonoBehaviour {

    public string channel;
    protected bool isVisible = true;
    protected Renderer[] renderers;

    void Start () {
    }

    protected void WidgetUpdate () {

        if ( renderers == default( Renderer[] ) ) {
            renderers = GetComponentsInChildren< Renderer >();
        }

        if ( showDebug && !isVisible ) {
            isVisible = true;
            foreach ( var r in renderers ) {
                r.enabled = true;
            }
        }
        else if ( !showDebug && isVisible ) {
            isVisible = false;
            foreach ( var r in renderers ) {
                r.enabled = false;
            }
        }
    }
}
