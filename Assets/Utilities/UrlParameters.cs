using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Web;

public class UrlParameters : MonoBehaviour {
    public TextMesh text;
    Dictionary<string,string> urlParams;

    void Start () {
        urlParams = new Dictionary<string, string>();
        var data = System.Environment.GetCommandLineArgs();
        //data = new string[]{"","rawbots://world1/map1/algo?load=vr100&player=parker"};
        var allParameters = "";

        for ( int i = 0; i<data.Length; i++ ) {
            if ( i == 1 ) {
                allParameters = data[ i ];
            }
        }

        try{
            var tmp = new System.Uri( allParameters );

            if ( tmp.IsWellFormedOriginalString() ) {
                text.text = tmp.Scheme + " <> " + tmp.Host + " <> " + tmp.Query + " \n ";
                string[] parts = tmp.Query.Split( new char[] {'?','&'} );
                foreach ( string x in parts ) {
                    if ( x.Length < 2 ) {
                        continue;
                    }
                    var pair = x.Split( '=' );
                    urlParams.Add( pair[ 0 ], pair[ 1 ] );
                }
                foreach ( var keypair in urlParams ) {
                    text.text += string.Format( "\tparams :key: {0}, value: {1}\n", keypair.Key, keypair.Value );
                }
            }
        }catch{

            Debug.Log("Invalid uri");
        }

    }
 

}
