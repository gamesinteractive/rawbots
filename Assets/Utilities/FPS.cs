using UnityEngine;
using System.Collections;

public class FPS : MonoBehaviour {

    public TextMesh textMesh;
    int frames = 0;
    float timer = 0;
    
    void Update () {
        ++frames;
        timer -= Time.deltaTime;
        if ( timer < 0 ) {
            int roundedTimer = ( int )timer;
            timer = 1.0f + timer - roundedTimer;
            textMesh.text = frames.ToString();
            frames = 0;
        }
    }

}
