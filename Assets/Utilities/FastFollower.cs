using UnityEngine;
using System.Collections;
using Oracle;
using Operants;
public class FastFollower : MonoBehaviour {
	
	
	public PID followPid;
	public BulletRigidBody bulletRigidbody;
	
	public void Start(){
		bulletRigidbody.AddOnCollisionDelegate(OnTrigger);
	}
	
	public void Calculate (Vector3 targetPos) {
		followPid.setpoint = targetPos;
		var distanceForce = followPid.Compute (transform.position, Time.deltaTime);
		bulletRigidbody.AddForce (distanceForce, Vector3.zero);	
	}
	
	
	public void OnTrigger (Vector3 relativeVelocity, BulletRigidBody other, Vector3 contactPoint, Vector3 lineOfAction)
	{
		GhostCamera.instance.obstacleFound = true;
	}
	
}
