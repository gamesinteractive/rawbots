using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class KeyBinder : MonoBehaviour {
    /*
//    public IOComponent io;
    public NodeView nodeView;
    List< KeyValuePair< Inbox, KeyBinding > > keyBindings;

    void Awake () {
        keyBindings = new List< KeyValuePair< Inbox, KeyBinding > >();
    }
 
    void OnInbox ( Inbox inbox ) {
        inbox.nodeView.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
         
         inbox.For< List< KeyStroke > >( property == "keystrokes", value => {
             CheckBindings( value );
            } );
             
        } ); 
    }
 
    void CheckBindings ( List<KeyStroke> strokes ) {
     var modifierKey = strokes.FirstOrDefault( 
         s => strokes.Count <= 2 && s.keyCode.IsModifier() 
     );
     var activationKey = strokes.FirstOrDefault( 
         s => strokes.Count <= 2 && s.keyCode != modifierKey.keyCode 
     );
     
        foreach ( var keyBinding in keyBindings.Where( kvp => 
                 kvp.Value.modifierKey == modifierKey.keyCode && 
                 kvp.Value.activatorKey == activationKey.keyCode &&
             kvp.Value.activatorKey != KeyCode.None ) ) {
         keyBinding.Value.state = activationKey.state;
//            io.ProduceFor( keyBinding.Key, "binding_action", keyBinding.Value );
        }
    }

    public void RegisterBindingForInbox ( Inbox inbox, KeyBinding keyBinding ) {
        keyBindings.Add( new KeyValuePair< Inbox, KeyBinding >( inbox, keyBinding ) );
        keyBindings = keyBindings.OrderByDescending( k => k.Value.priority ).ToList();
    }

    public void UnregisterInbox ( Inbox inbox ) {
        keyBindings = keyBindings.Where( kvp => kvp.Key != inbox ).ToList();
    }
     */
}
