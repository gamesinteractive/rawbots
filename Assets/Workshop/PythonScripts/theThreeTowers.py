import library

with open('../../BotDefinitions/theThreeTowers.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	#################### First Tower #########################

	#Creating platforms
	obj.addHexPlat("platform000","0,0,0","0,0,0")
	obj.addHexPlat("platform001","0,42,0","0,0,0")

	#creating platforms
	obj.addHexPlat("platform002","0,84,0","0,0,0")
	obj.addHexPlat("platform003","0,126,0","0,0,0")

	#Creating columns
	obj.addHexColumn("column000","47,0,0","270,0,0")
	obj.addHexColumn("column001","-47,0,0","270,180,0")
	obj.addHexColumn("column002","-23.4,0,-40.7","270,120,0")
	obj.addHexColumn("column003","23.4,0,-40.7","270,60,0")
	obj.addHexColumn("column004","23.4,0,40.7","270,300,0")
	obj.addHexColumn("column005","-23.4,0,40.7","270,240,0")

	#Creating columns
	obj.addHexColumn("column006","47,42,0","270,0,0")
	obj.addHexColumn("column007","-47,42,0","270,180,0")
	obj.addHexColumn("column008","-23.4,42,-40.7","270,120,0")
	obj.addHexColumn("column009","23.4,42,-40.7","270,60,0")
	obj.addHexColumn("column010","23.4,42,40.7","270,300,0")
	obj.addHexColumn("column011","-23.4,42,40.7","270,240,0")

	#Creating columns
	obj.addHexColumn("column012","47,84,0","270,0,0")
	obj.addHexColumn("column013","-47,84,0","270,180,0")
	obj.addHexColumn("column014","-23.4,84,-40.7","270,120,0")
	obj.addHexColumn("column015","23.4,84,-40.7","270,60,0")
	obj.addHexColumn("column016","23.4,84,40.7","270,300,0")
	obj.addHexColumn("column017","-23.4,84,40.7","270,240,0")

	#Connecting platform000 to columns
	obj.extend("platform000","01","column000","01")
	obj.extend("platform000","02","column001","01")
	obj.extend("platform000","03","column002","01")
	obj.extend("platform000","04","column003","01")
	obj.extend("platform000","05","column004","01")
	obj.extend("platform000","06","column005","01")

	#Connecting platform001 to columns
	obj.extend("platform001","20","column000","00")
	obj.extend("platform001","21","column001","00")
	obj.extend("platform001","22","column002","00")
	obj.extend("platform001","23","column003","00")
	obj.extend("platform001","24","column004","00")
	obj.extend("platform001","25","column005","00")

	#Connecting platform001 to columns
	obj.extend("platform001","01","column006","01")
	obj.extend("platform001","02","column007","01")
	obj.extend("platform001","03","column008","01")
	obj.extend("platform001","04","column009","01")
	obj.extend("platform001","05","column010","01")
	obj.extend("platform001","06","column011","01")

	#Connecting platform002 to columns
	obj.extend("platform002","20","column006","00")
	obj.extend("platform002","21","column007","00")
	obj.extend("platform002","22","column008","00")
	obj.extend("platform002","23","column009","00")
	obj.extend("platform002","24","column010","00")
	obj.extend("platform002","25","column011","00")

	#Connecting platform002 to columns
	obj.extend("platform002","01","column012","01")
	obj.extend("platform002","02","column013","01")
	obj.extend("platform002","03","column014","01")
	obj.extend("platform002","04","column015","01")
	obj.extend("platform002","05","column016","01")
	obj.extend("platform002","06","column017","01")

	#Connecting platform003 to columns
	obj.extend("platform003","20","column012","00")
	obj.extend("platform003","21","column013","00")
	obj.extend("platform003","22","column014","00")
	obj.extend("platform003","23","column015","00")
	obj.extend("platform003","24","column016","00")
	obj.extend("platform003","25","column017","00")

	################### Second Tower #######################

	#Creating platforms
	obj.addHexPlat("platform004","141.0255,0,0","0,0,0")
	obj.addHexPlat("platform005","141.0255,42,0","0,0,0")

	#creating platforms
	obj.addHexPlat("platform006","141.0255,84,0","0,0,0")
	obj.addHexPlat("platform007","141.0255,126,0","0,0,0")

	#Creating columns
	obj.addHexColumn("column018","188.0255,0,0","270,0,0")
	obj.addHexColumn("column019","94.02547,0,0","270,180,0")
	obj.addHexColumn("column020","117.6255,0,-40.7","270,120,0")
	obj.addHexColumn("column021","164.4255,0,-40.7","270,60,0")
	obj.addHexColumn("column022","164.4255,0,40.7","270,300,0")
	obj.addHexColumn("column023","117.6255,0,40.7","270,240,0")

	#Creating columns
	obj.addHexColumn("column024","188.0255,42,0","270,0,0")
	obj.addHexColumn("column025","94.02547,42,0","270,180,0")
	obj.addHexColumn("column026","117.6255,42,-40.7","270,120,0")
	obj.addHexColumn("column027","164.4255,42,-40.7","270,60,0")
	obj.addHexColumn("column028","164.4255,42,40.7","270,300,0")
	obj.addHexColumn("column029","117.6255,42,40.7","270,240,0")

	#Creating columns
	obj.addHexColumn("column030","188.0255,84,0","270,0,0")
	obj.addHexColumn("column031","94.02547,84,0","270,180,0")
	obj.addHexColumn("column032","117.6255,84,-40.7","270,120,0")
	obj.addHexColumn("column033","164.4255,84,-40.7","270,60,0")
	obj.addHexColumn("column034","164.4255,84,40.7","270,300,0")
	obj.addHexColumn("column035","117.6255,84,40.7","270,240,0")

	#Connecting platform004 to columns
	obj.extend("platform004","01","column018","01")
	obj.extend("platform004","02","column019","01")
	obj.extend("platform004","03","column020","01")
	obj.extend("platform004","04","column021","01")
	obj.extend("platform004","05","column022","01")
	obj.extend("platform004","06","column023","01")

	#Connecting platform005 to columns
	obj.extend("platform005","20","column018","00")
	obj.extend("platform005","21","column019","00")
	obj.extend("platform005","22","column020","00")
	obj.extend("platform005","23","column021","00")
	obj.extend("platform005","24","column022","00")
	obj.extend("platform005","25","column023","00")

	#Connecting platform005 to columns
	obj.extend("platform005","01","column024","01")
	obj.extend("platform005","02","column025","01")
	obj.extend("platform005","03","column026","01")
	obj.extend("platform005","04","column027","01")
	obj.extend("platform005","05","column028","01")
	obj.extend("platform005","06","column029","01")

	#Connecting platform006 to columns
	obj.extend("platform006","20","column024","00")
	obj.extend("platform006","21","column025","00")
	obj.extend("platform006","22","column026","00")
	obj.extend("platform006","23","column027","00")
	obj.extend("platform006","24","column028","00")
	obj.extend("platform006","25","column029","00")

	#Connecting platform006 to columns
	obj.extend("platform006","01","column030","01")
	obj.extend("platform006","02","column031","01")
	obj.extend("platform006","03","column032","01")
	obj.extend("platform006","04","column033","01")
	obj.extend("platform006","05","column034","01")
	obj.extend("platform006","06","column035","01")

	#Connecting platform007 to columns
	obj.extend("platform007","20","column030","00")
	obj.extend("platform007","21","column031","00")
	obj.extend("platform007","22","column032","00")
	obj.extend("platform007","23","column033","00")
	obj.extend("platform007","24","column034","00")
	obj.extend("platform007","25","column035","00")

	################### Third Tower ########################

	#Creating platforms
	obj.addHexPlat("platform008","70.43096,0,-121.9841","0,0,0")
	obj.addHexPlat("platform009","70.43096,42,-121.9841","0,0,0")

	#creating platforms
	obj.addHexPlat("platform010","70.43096,84,-121.9841","0,0,0")

	#Creating columns
	obj.addHexColumn("column036","117.431,0,-121.9841","270,0,0")
	obj.addHexColumn("column037","23.43097,0,-121.9841","270,180,0")
	obj.addHexColumn("column038","47.03096,0,-162.6841","270,120,0")
	obj.addHexColumn("column039","93.83096,0,-162.6841","270,60,0")
	obj.addHexColumn("column040","93.83097,0,-81.28407","270,300,0")
	obj.addHexColumn("column041","47.03096,0,-81.28405","270,240,0")

	#Creating columns
	obj.addHexColumn("column042","117.431,42,-121.9841","270,0,0")
	obj.addHexColumn("column043","23.43097,42,-121.9841","270,180,0")
	obj.addHexColumn("column044","47.03096,42,-162.6841","270,120,0")
	obj.addHexColumn("column045","93.83096,42,-162.6841","270,60,0")
	obj.addHexColumn("column046","93.83097,42,-81.28407","270,300,0")
	obj.addHexColumn("column047","47.03096,42,-81.28405","270,240,0")

	#Connecting platform008 to columns
	obj.extend("platform008","01","column036","01")
	obj.extend("platform008","02","column037","01")
	obj.extend("platform008","03","column038","01")
	obj.extend("platform008","04","column039","01")
	obj.extend("platform008","05","column040","01")
	obj.extend("platform008","06","column041","01")

	#Connecting platform009 to columns
	obj.extend("platform009","20","column036","00")
	obj.extend("platform009","21","column037","00")
	obj.extend("platform009","22","column038","00")
	obj.extend("platform009","23","column039","00")
	obj.extend("platform009","24","column040","00")
	obj.extend("platform009","25","column041","00")

	#Connecting platform009 to columns
	obj.extend("platform009","01","column042","01")
	obj.extend("platform009","02","column043","01")
	obj.extend("platform009","03","column044","01")
	obj.extend("platform009","04","column045","01")
	obj.extend("platform009","05","column046","01")
	obj.extend("platform009","06","column047","01")

	################### Connections ########################
	
	#Creating platforms
	obj.addHexPlat("platform011","70.45,42,-40.61","0,0,0")
	obj.addHexPlat("platform012","70.45,84,-40.61","0,0,0")

	#Connecting to towers
	obj.extend("platform011","13","platform001","11")
	obj.extend("platform011","14","platform001","12")
	obj.extend("platform011","16","platform005","18")
	obj.extend("platform011","15","platform005","17")
	obj.extend("platform011","09","platform009","08")
	obj.extend("platform011","10","platform009","07")

	obj.extend("platform012","13","platform002","11")
	obj.extend("platform012","14","platform002","12")
	obj.extend("platform012","16","platform006","18")
	obj.extend("platform012","15","platform006","17")
	obj.extend("platform012","09","platform010","08")
	obj.extend("platform012","10","platform010","07")


