<<<<<<< HEAD
import library

with open('../../Resources/Bots/simplehexfloat.txt', 'w') as f:
    obj = library.Workshop(f)
    obj.libs()
    obj.addHexPlat("simplehexfloat","0,0,0","0,0,0")
    coords = [
        [-1.732,0,0],[0.866,0,1.5],[0.866,0,-1.5],[45.1962,0,0],
        [34.33002,0,-19.8205],[22.5981,0,-39.14114],[0,0,-39.64101],
        [-22.5981,0,-39.14114],[-34.33002,0,-19.8205],[-45.1962,0,0],
        [-34.33005,0,19.8205],[-22.5981,0,39.14114],[0,0,39.64101],
        [22.5981,0,39.14114],[33.89715,0,19.57057],[35.19615,-1,20.32051],
        [35.19615,-1,-20.32051],[0,-1,-40.64101],[-35.19615,-1,-20.32051],
        [-35.1941,-1,20.32051],[0,-1,40.64101],[0,-2,39.64101],[22.5981,-2,39.14114],
        [33.89715,-2,19.57057],[45.1962,-2,0],[34.33002,-2,-19.8205],[22.5981,-2,-39.14114],
        [0,-2,-39.64101],[-22.5981,-2,-39.1411],[-34.33002,-2,-19.8205],[-45.1962,-2,0],
        [-34.33005,-2,19.8205],[-22.5981,-2,39.14114],[0.866,-2,1.5],[0.866,-2,-1.5],[-1.732,-2,0]
    ]
    usable = [22,24,26,28,30,32]

    floatCoords = [
        [[-3.5,0,0],["0,0,270"]],
        [[0,0,3.5],["0,270,90"]],
        [[3.5,0,0],["0,0,90"]],
        [[0,0,-3.5],["0,90,90"]]
    ]
    i = 0
    for point in coords:
        
        if i in usable:
            print(str(i) + "!")
            currentContName = "cont"+str(i)
            pointStr = str(point[0])+","+str(point[1]-8)+","+str(point[2])
            obj.addContinuum(currentContName,pointStr,"0,0,0");
            obj.extend("simplehexfloat",str(i),currentContName,"1")
            j=0                 
            for floatCoord in floatCoords:                
                currentFloatName = "fltdev"+str(i)+str(j)                
                floatPos = str(point[0]+floatCoord[0][0])+","+str(point[1]+floatCoord[0][1]-8)+","+str(point[2]+floatCoord[0][2])                                                
                obj.addFloatingDevice(currentFloatName, floatPos, str(floatCoord[1][0]))                
                if j == 0:
                    obj.extend(currentFloatName,"0",currentContName,"0")
                elif j == 1:
                    obj.extend(currentFloatName,"0",currentContName,"2")
                elif j == 2:
                    obj.extend(currentFloatName,"0",currentContName,"3")
                elif j == 3:
                    obj.extend(currentFloatName,"0",currentContName,"5")

                j = j + 1                        
        i = i + 1

    obj.addContinuum("jetcont01","0,-3,0","0,0,0")
    obj.addJet("bottomjet","0,-5,-2","90,0,0")
    obj.addJet("leftjet","-2,-3,-2","0,90,90")
    obj.addJet("rightjet","2,-3,-2","0,90,90")
    obj.extend("simplehexfloat","35","jetcont01","1")
    obj.extend("rightjet","1","jetcont01","0")
    obj.extend("leftjet","0","jetcont01","3")
    obj.extend("bottomjet","1","jetcont01","4")

    obj.addInputSampler("jetinputcontrol01", controls = { "positive" : "F"},attack="30", release="25", invert="False")
    obj.addSampleMapper("jetthrustmap01","0","250")
    obj.consume("jetthrustmap01","sample","jetinputcontrol01", "sample")

    obj.consume("leftjet","thrust","jetthrustmap01","mapped")
    obj.consume("rightjet","thrust","jetthrustmap01","mapped")
    obj.consume("bottomjet","thrust","jetthrustmap01","mapped")
=======
import library

with open('../../Resources/Bots/simplehexfloat.txt', 'w') as f:
    obj = library.Workshop(f)
    obj.libs()
    obj.addHexPlat("simplehexfloat","0,0,0","0,0,0")
    coords = [
        [-1.732,0,0],[0.866,0,1.5],[0.866,0,-1.5],[45.1962,0,0],
        [34.33002,0,-19.8205],[22.5981,0,-39.14114],[0,0,-39.64101],
        [-22.5981,0,-39.14114],[-34.33002,0,-19.8205],[-45.1962,0,0],
        [-34.33005,0,19.8205],[-22.5981,0,39.14114],[0,0,39.64101],
        [22.5981,0,39.14114],[33.89715,0,19.57057],[35.19615,-1,20.32051],
        [35.19615,-1,-20.32051],[0,-1,-40.64101],[-35.19615,-1,-20.32051],
        [-35.1941,-1,20.32051],[0,-1,40.64101],[0,-2,39.64101],[22.5981,-2,39.14114],
        [33.89715,-2,19.57057],[45.1962,-2,0],[34.33002,-2,-19.8205],[22.5981,-2,-39.14114],
        [0,-2,-39.64101],[-22.5981,-2,-39.1411],[-34.33002,-2,-19.8205],[-45.1962,-2,0],
        [-34.33005,-2,19.8205],[-22.5981,-2,39.14114],[0.866,-2,1.5],[0.866,-2,-1.5],[-1.732,-2,0]
    ]
    usable = [22,24,26,28,30,32]

    floatCoords = [
        [[-3.5,0,0],["0,0,270"]],
        [[0,0,3.5],["0,270,90"]],
        [[3.5,0,0],["0,0,90"]],
        [[0,0,-3.5],["0,90,90"]]
    ]
    i = 0
    for point in coords:
        
        if i in usable:
            print(str(i) + "!")
            currentContName = "cont"+str(i)
            pointStr = str(point[0])+","+str(point[1]-8)+","+str(point[2])
            obj.addContinuum(currentContName,pointStr,"0,0,0");
            obj.extend("simplehexfloat",str(i),currentContName,"1")
            j=0                 
            for floatCoord in floatCoords:                
                currentFloatName = "fltdev"+str(i)+str(j)                
                floatPos = str(point[0]+floatCoord[0][0])+","+str(point[1]+floatCoord[0][1]-8)+","+str(point[2]+floatCoord[0][2])                                                
                obj.addFloatingDevice(currentFloatName, floatPos, str(floatCoord[1][0]))                
                if j == 0:
                    obj.extend(currentFloatName,"0",currentContName,"0")
                elif j == 1:
                    obj.extend(currentFloatName,"0",currentContName,"2")
                elif j == 2:
                    obj.extend(currentFloatName,"0",currentContName,"3")
                elif j == 3:
                    obj.extend(currentFloatName,"0",currentContName,"5")

                j = j + 1                        
        i = i + 1
>>>>>>> javier
