import library

with open('../../Resources/Bots/fish.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core001","4,0,0","0,0,0")

	obj.addContinuum("continuum001","0,0,0","0,0,0")
	obj.addContinuum("continuum002","2,0,0","0,0,0")
	obj.addContinuum("continuum004","4,0,2","0,0,0")
	obj.addContinuum("continuum005","4,0,-2","0,0,0")
	obj.addContinuum("continuum006","6,0,0","0,0,0")
	obj.addContinuum("continuum007","-2,0,0","0,0,0")

	obj.addElbow("right_fin","4,0,4","0,90,0",initial_values=[["angle","0"]])
	obj.addElbow("left_fin","4,0,-4","0,270,0",initial_values=[["angle","0"]])

	obj.addFin("fin001","4,0,6","90,270,0")
	obj.addFin("fin002","4,0,-6","90,90,0")

	obj.addHydroJet("hydrojet001","-2,0,2","90,90,0")
	obj.addHydroJet("hydrojet002","-2,0,-2","90,90,0")

	obj.extend("continuum001","0","continuum002","3")
	obj.extend("continuum002","0","core001","3")
	obj.extend("core001","2","continuum004","5")
	obj.extend("core001","5","continuum005","2")
	obj.extend("core001","0","continuum006","3")
	obj.extend("right_fin","0","continuum004","2")
	obj.extend("left_fin","0","continuum005","5")
	obj.extend("fin001","0","right_fin","1")
	obj.extend("fin002","0","left_fin","1")
	obj.extend("continuum007","0","continuum001","3")
	obj.extend("hydrojet001","0","continuum007","2")
	obj.extend("hydrojet002","1","continuum007","5")

	obj.addOscillator(name = "right_osc",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "1",
		amplitude_range = "0,10,1",
		amplitude_value = "30",
		offset_range= "-1500,1500,1",
		offset_value = "0",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "left_osc",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "1",
		amplitude_range = "0,10,1",
		amplitude_value = "30",
		offset_range= "-1500,1500,1",
		offset_value = "0",
		phase_range = "0,10,0.1")

	obj.consume("right_fin","angle","right_osc","sample")
	obj.consume("left_fin","angle","left_osc","sample")

	obj.addGraphChunk("Input_Stuff","""
#keyboard
kb01 as keyboard_sensor

kb01_ev output_for kb01
kb01_ev as keyboard_event

#inputmanager
input001 as input_manager

input001_event input_for input001
input001_event as input_event

input001_subs input_for input001
input001_subs as input_subscription

#input setup
forward_motion as input_setup
forward_motion . positive W
forward_motion . negative S
forward_motion . sensitivity 5
forward_motion . gravity 0
forward_motion . axis True

forward_motion_subs output_for forward_motion
forward_motion_subs as setup_subscriptor

forward_motion_action output_for forward_motion
forward_motion_action as setup_action

#input linear_mapper
forward_map as linear_mapper
forward_map . map [0,1000,1]
forward_raw input_for forward_map
forward_raw as linear_mapper_raw
forward_mapped output_for forward_map
forward_mapped as linear_mapper_mapped
forward_raw consumes forward_motion_action

#input linear_mapper
left_map as linear_mapper
left_map . map [0,-1000,1]
left_raw input_for left_map
left_raw as linear_mapper_raw
left_mapped output_for left_map
left_mapped as linear_mapper_mapped
left_raw consumes forward_motion_action

#input setup
side_motion as input_setup
side_motion . positive A
side_motion . negative D
side_motion . sensitivity 5
side_motion . gravity 0
side_motion . axis True

side_motion_subs output_for side_motion
side_motion_subs as setup_subscriptor

side_motion_action output_for side_motion
side_motion_action as setup_action

#input linear_mapper
right_map as linear_mapper
right_map . map [0,-1000,1]
right_raw input_for right_map
right_raw as linear_mapper_raw
right_mapped output_for right_map
right_mapped as linear_mapper_mapped
right_raw consumes side_motion_action

input001_event consumes kb01_ev
input001_subs consumes forward_motion_subs
input001_subs consumes side_motion_subs

motor27_angle consumes right_mapped
motor26_angle consumes left_mapped

jet_thrust_control as input_setup

jet_thrust_control . name jet_thrust_control
jet_thrust_control . positive E
jet_thrust_control . negative Q
jet_thrust_control . sensitivity 4
jet_thrust_control . gravity 0
jet_thrust_control . axis True

jet_thrust_control_subscriptor output_for jet_thrust_control
jet_thrust_control_subscriptor as setup_subscriptor

jet_thrust_control_action output_for jet_thrust_control
jet_thrust_control_action as setup_action

jet_thrust_control_map as linear_mapper
jet_thrust_control_map . map [0,2000,1]

jet_thrust_control_map_raw input_for jet_thrust_control_map
jet_thrust_control_map_raw as linear_mapper_raw

jet_thrust_control_map_mapped output_for jet_thrust_control_map
jet_thrust_control_map_mapped as linear_mapper_mapped

jet_thrust_control_map_raw consumes jet_thrust_control_action

input001_subs consumes jet_thrust_control_subscriptor

hydrojet001_thrust consumes jet_thrust_control_map_mapped
hydrojet002_thrust consumes jet_thrust_control_map_mapped

""")



