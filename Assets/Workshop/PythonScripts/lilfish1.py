import library

with open('../../BotDefinitions/lilfish1.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addContinuum("continuum001","0,0,0","0,0,0")
	obj.addMotor("motor001","0,0,2","270,0,0")
	#obj.addElbow("right_fin","0,0,2","0,90,0",initial_values=[["angle","0"]])
	obj.addFin("fin001","0,0,4","90,270,0")

	obj.extend("motor001","0","continuum001","2")
	obj.extend("fin001","0","motor001","1")

	obj.addOscillator(name = "fin_osc",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "1",
		amplitude_range = "0,10,1",
		amplitude_value = "180",
		offset_range= "-1500,1500,1",
		offset_value = "0",
		phase_range = "0,10,0.1")

	obj.consume("motor001","angle","fin_osc","sample")

#	#keyboard
#kb01 as keyboard_sensor
#
#kb01_ev output_for kb01
#kb01_ev as keyboard_event
#
##inputmanager
#input001 as input_manager
#
#input001_event input_for input001
#input001_event as input_event
#
#input001_subs input_for input001
#input001_subs as input_subscription
#
##input setup
#side_motion as input_setup
#side_motion . positive A
#side_motion . negative D
#side_motion . sensitivity 1
#side_motion . gravity 1
#side_motion . axis True
#
#side_motion_subs output_for side_motion
#side_motion_subs as setup_subscriptor
#
#side_motion_action output_for side_motion
#side_motion_action as setup_action
#
##input mapper
#right_side_map as mapper
#right_side_map . map [0,1000,1]
#right_side_raw input_for right_side_map
#right_side_raw as mapper_raw
#right_side_mapped output_for right_side_map
#right_side_mapped as mapper_mapped
#right_side_raw consumes side_motion_action
#input001_event consumes kb01_ev
#input001_subs consumes side_motion_subs
#
#motor001_velocity consumes right_side_mapped



