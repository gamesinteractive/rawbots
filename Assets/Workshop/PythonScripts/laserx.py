import library

with open('../../Resources/Bots/laserx.txt', 'w') as f:
    obj = library.Workshop(f)
    obj.libs()

    obj.addOscillator(name = "sine_osc1",
    pos = "0,1,0",
    rot= "0,0,0",
    type="Sine",
    invert= "False",
    frequency_value = "0.5",
    amplitude_value = "1",
    offset_value = "0",
    phase_value = "0.1")

    obj.addOscillator(name = "sine_osc2",
    pos = "0,1,0",
    rot= "0,0,0",
    type="Sine",
    invert= "False",
    frequency_value = "0.5",
    amplitude_value = "1",
    offset_value = "0",
    phase_value = "0.2")

    obj.addOscillator(name = "sine_osc3",
    pos = "0,1,0",
    rot= "0,0,0",
    type="Sine",
    invert= "False",
    frequency_value = "0.5",
    amplitude_value = "1",
    offset_value = "0",
    phase_value = "0.3")

    obj.addInputSampler("lasercontrolinput",{"positive" : "X"}, "10")

    obj.addSampleMapper("lasercontrolmap","0","1")

    obj.consume("lasercontrolmap", "sample","lasercontrolinput", "sample")


    rangeMin = -8;
    rangeMinAbs = abs(rangeMin)
    rangeMax = 12;
    step = 4;

    domain = range(rangeMin,rangeMax,step)
    rotationStr = "0,0,45";
    laserRot = "90,0,0" 
    obj.addContinuum("conty1"+str(rangeMinAbs),"0,0,0",rotationStr)
    obj.addLaser("laser0", "0,0,2",laserRot,"1")
    obj.extend("laser0","0","conty1"+str(rangeMinAbs),"5")
    obj.consume("laser0","length","sine_osc3","sample")
    #obj.consume("laser0","length","lasercontrolmap","mapped")

    for x in domain:
        y1 = x;
        y2 = x*-1;
        if y1!=y2:        
            obj.addContinuum("conty1"+str(x+rangeMinAbs),str(x)+","+str(y1)+",0",rotationStr)
            obj.addContinuum("conty2"+str(x+rangeMinAbs),str(x)+","+str(y2)+",0",rotationStr)
            
            obj.addLaser("lasery1"+str(x+rangeMinAbs), str(x)+","+str(y1)+",2", laserRot, "0.5")            
            obj.addLaser("lasery2"+str(x+rangeMinAbs), str(x)+","+str(y2)+",2", laserRot, "0.5")

            if x % 8 == 0:
                obj.consume("lasery1"+str(x+rangeMinAbs),"length","sine_osc1","sample")
                obj.consume("lasery2"+str(x+rangeMinAbs),"length","sine_osc1","sample")
            else:
                obj.consume("lasery1"+str(x+rangeMinAbs),"length","sine_osc2","sample")
                obj.consume("lasery2"+str(x+rangeMinAbs),"length","sine_osc2","sample")

            obj.extend("lasery1"+str(x+rangeMinAbs),"0","conty1"+str(x+rangeMinAbs),"5")
            obj.extend("lasery2"+str(x+rangeMinAbs),"0","conty2"+str(x+rangeMinAbs),"5")

        if x > -rangeMinAbs:            
            obj.extend("conty1"+str(x+rangeMinAbs),"3","conty1"+str(x+rangeMinAbs-step),"0")
            if x == 0:
                obj.extend("conty1"+str(x+rangeMinAbs),"1","conty2"+str(x+rangeMinAbs-step),"4")
            elif x == step:
                obj.extend("conty2"+str(x+rangeMinAbs),"1","conty1"+str(x+rangeMinAbs-step),"4")
            else:
                obj.extend("conty2"+str(x+rangeMinAbs),"1","conty2"+str(x+rangeMinAbs-step),"4") 

