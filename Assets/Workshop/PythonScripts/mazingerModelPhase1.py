import library

with open('../../BotDefinitions/mazingerModelPhase1.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	#Core
	#obj.addCore("master","0,-4,0","0,0,0")

	#obj.addPointLight("parker","0,0,0","0,0,0")
	#obj.addBody_gen04_3("plate00","0,0,0","0,0,90",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])
	#obj.addMagnet("magnet001","0,0,0","0,0,0")
	#obj.addWall("unmuro","0,0,0","0,0,0")

#	obj.addHexEarth("pieceofdirt","0,0,0","0,0,0")
	obj.addAntenna("antenna001","0,0,0","0,0,0")
#	obj.addContinuum("box01","0,0,0","0,0,0")
#	obj.addHexPlat("plataforma","0,0,0","0,0,0")
#	obj.addHexColumn("columna","0,0,0","0,0,0")
#	obj.addWall("muro","0,0,0","0,0,0")
#	obj.addSensor("sensor01","0,0,0","0,0,0")
#	obj.addLaser("laser001","0,0,0","0,0,0")