import library

with open('../../BotDefinitions/hexagonNoWall.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	#Creating platforms
	obj.addHexPlat("platform000","0,0,0","0,0,0")
	obj.addHexPlat("platform001","0,42,0","0,0,0")

	#Creating columns
	obj.addHexColumn("column000","47,0,0","270,0,0")
	obj.addHexColumn("column001","-47,0,0","270,180,0")
	obj.addHexColumn("column002","-23.4,0,-40.7","270,120,0")
	obj.addHexColumn("column003","23.4,0,-40.7","270,60,0")
	obj.addHexColumn("column004","23.4,0,40.7","270,300,0")
	obj.addHexColumn("column005","-23.4,0,40.7","270,240,0")

	#Connecting platform000 to columns
	obj.extend("platform000","01","column000","01")
	obj.extend("platform000","02","column001","01")
	obj.extend("platform000","03","column002","01")
	obj.extend("platform000","04","column003","01")
	obj.extend("platform000","05","column004","01")
	obj.extend("platform000","06","column005","01")

	#Connecting platform001 to columns
	obj.extend("platform001","20","column000","00")
	obj.extend("platform001","21","column001","00")
	obj.extend("platform001","22","column002","00")
	obj.extend("platform001","23","column003","00")
	obj.extend("platform001","24","column004","00")
	obj.extend("platform001","25","column005","00")