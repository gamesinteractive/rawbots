class Continuum:
    def __init__(self,name,pos,rot):
        self.name = name
        self.pos = pos
        self.rot = rot

class Workshop:
    def __init__(self,file):
        self.file = file
    def libs(self):
        includes = \
"""
"""        
    def title(self,title):
        var = "\n################################################\n##"+title+"\n################################################\n"
        self.file.write(var)
    def addContinuum(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as continuum\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as continuum_part\n\n"

        for element in ["0", "1", "2", "3", "4", "5"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as continuum_slot_"+element+"\n"

        self.file.write(text)
    def addCore(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as core\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as core_part\n\n"


        text = text + name+"_pos output_for "+name+ "\n"
        text = text + name+"_pos as core_pos\n\n"


        for element in ["0", "1", "2", "3", "4", "5"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as core_slot_"+element+"\n"


        self.file.write(text)

    def addMotor(self,name,
        pos = "0,0,0",
        rot ="0,0,0",
        angle_range = "-180,180,1",
        velocity_range = "-200,200,1",
        initial_values =[["angle","0"]],
        usepart = False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as motor\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as motor_part\n\n"

        for element in ["0", "1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as motor_slot_"+element+"\n"


        for keypair in initial_values:
            text = text +name+"_"+keypair[0]+" . value "+keypair[1]+"\n"


        text = text + name+"_angle input_for "+name+ "\n"
        text = text + name+"_angle as motor_angle\n\n"

        text = text + name+"_angle . range ["+angle_range+"]\n"

        text = text + name+"_current_angle output_for "+name+ "\n"
        text = text + name+"_current_angle as motor_current_angle\n\n"


        text = text + name+"_velocity input_for "+name+ "\n"
        text = text + name+"_velocity as motor_velocity\n\n"

        text = text +name+"_velocity . range ["+velocity_range+"]\n"
        self.file.write(text)
    def extend(self,extension,ext_slot,base,base_slot):
        text =extension+"_slot_"+ext_slot+" extends "+base+"_slot_"+base_slot+"\n"
        self.file.write(text)
    def trails(self,extension,ext_slot,base,base_slot):
        text =extension+"_slot_"+ext_slot+" trails "+base+"_slot_"+base_slot+"\n"
        self.file.write(text)
    def addPiston(self,name,pos = "0,0,0",
        rot ="0,0,0",
        position_range = "0,1,0.1",
        initial_values = [["position","1"]],
        usepart = False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as piston\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as piston_part\n\n"

        for element in ["0", "1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as piston_slot_"+element+"\n"
        text = text + name+"_position input_for "+name+ "\n"
        text = text + name+"_position as piston_position \n\n"


        text = text +name+"_position . range ["+position_range+"]\n"
        for keypair in initial_values:
            text = text +name+"_"+keypair[0]+" . value "+keypair[1]+"\n"


        self.file.write(text)
    def addOscillator(self,name,
        pos= "0,0,0",
        rot ="0,0,0",
        type="Triangle",
        invert = "False",
        amplitude_range = "0,180,1",
        amplitude_value = "45",
        frequency_range = "0,10,0.1",
        frequency_value = "1",
        phase_range = "0,10,0,1",
        phase_value = "0",
        offset_range = "0,180,1",
        offset_value = "0",
        time_scale = "1"):
        self.title(name);
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as oscillator\n\n"
        text = text +name+"_sample output_for "+name+"\n"
        text = text +name+"_sample as oscillator_sample\n"

        text = text +name+"_amplitude input_for "+name+"\n"        
        text = text +name+"_amplitude . value "+amplitude_value+"\n"
        text = text +name+"_amplitude as oscillator_amplitude\n"

        text = text +name+"_time_scale input_for "+name+"\n"
        text = text +name+"_time_scale . value "+time_scale+"\n"
        text = text +name+"_time_scale as oscillator_time_scale\n"

        text = text +name+"_frequency input_for "+name+"\n"        
        text = text +name+"_frequency . value "+frequency_value+"\n"
        text = text +name+"_frequency as oscillator_frequency\n"



        text = text +name+"_phase input_for "+name+"\n"
        text = text +name+"_phase . range ["+phase_range+"]\n"
        text = text +name+"_phase . value "+phase_value+"\n"
        text = text +name+"_phase as oscillator_phase\n"



        text = text +name+"_offset input_for "+name+"\n"        
        text = text +name+"_offset . value "+offset_value+"\n"
        text = text +name+"_offset as oscillator_offset\n"



        text = text +name+"_type input_for "+name+"\n"
        text = text +name+"_type . value "+type+"\n"
        text = text +name+"_type as oscillator_type\n"



        text = text +name+"_invert input_for "+name+"\n"
        text = text +name+"_invert . value "+invert+"\n"
        text = text +name+"_invert as oscillator_invert\n"



        self.file.write(text)
    def consume(self,consumer,input,producer,output):
        text =consumer+"_"+input+" consumes "+producer+"_"+output+"\n"
        self.file.write(text)

    def addWheel(self,name,pos,rot):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as wheel\n\n"
        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as wheel_slot_"+element+"\n"

        self.file.write(text)
    def addWideWheel(self,name,pos,rot):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as wide_wheel\n\n"
        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as wide_wheel_slot_"+element+"\n"

        self.file.write(text)

    def addWideWheelSmall(self,name,pos,rot):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as wide_wheel_small\n\n"
        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as wide_wheel_small_slot_"+element+"\n"

        self.file.write(text)

    def addJet(self,name,pos,rot):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text+name+" as jet\n\n"

        text+= name+"_thrust input_for " + name + "\n"
        text+= name+"_thrust as jet_thrust"  + "\n"

        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as jet_slot_"+element+"\n"

        self.file.write(text)

    def addPistol(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as pistol\n\n"

        text = text + name + "_shoot input_for "+name+"\n"
        text = text + name + "_shoot as pistol_shoot\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as pistol_part\n\n"


        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as pistol_slot_"+element+"\n"
        self.file.write(text)
    def addCannon(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as cannon\n\n"

        text = text + name + "_shoot input_for "+name+"\n"
        text = text + name + "_shoot as cannon_shoot\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as cannon_part\n\n"

        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as cannon_slot_"+element+"\n"
        self.file.write(text)

    def addShotgun(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as shotgun\n\n"

        text = text + name + "_shoot input_for "+name+"\n"
        text = text + name + "_shoot as shotgun_shoot\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as shotgun_part\n\n"

        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as shotgun_slot_"+element+"\n"
        self.file.write(text)

    def addBody_gen01_1(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen01_1\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen01_1_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen01_1"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen01_2(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen01_2\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen01_2_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen01_2"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen01_3(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen01_3\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen01_3_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen01_3"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen01_4(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen01_4\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen01_4_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen01_4"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_cvr_tlt_gen01_05(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as bdy_cvr_tlt_gen01_05\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as bdy_cvr_tlt_gen01_05_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as bdy_cvr_tlt_gen01_05"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen02_1(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen02_1\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen02_1_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen02_1"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen02_2(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen02_2\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen02_2_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen02_2"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen02_3(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen02_3\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen02_3_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen02_3"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen03_1(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen03_1\n\n"
        for element in ["0","1","2"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen03_1_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen03_1"+keypair[0]+"\n"
        self.file.write(text)


    def addBody_gen03_2(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen03_2\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen03_2_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen03_2"+keypair[0]+"\n"
        self.file.write(text)


    def addBody_gen03_3(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen03_3\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen03_3_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen03_3"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen03_4(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen03_4\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen03_4_slot_"+element+"\n"

        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen03_4"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen03_5(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen03_5\n\n"
        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen03_5_slot_"+element+"\n"



        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen03_5"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen03_6(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen03_6\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen03_6_slot_"+element+"\n"



        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen03_6"+keypair[0]+"\n"
        self.file.write(text)


    def addBody_gen04_1(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen04_1\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen04_1_slot_"+element+"\n"



        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen04_1"+keypair[0]+"\n"
        self.file.write(text)

    def addBody_gen04_3(self,name,pos,rot,setColors=False,initial_values=[["_channel_a",""],["_channel_b",""],["_channel_c",""]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as body_gen04_3\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as body_gen04_3_slot_"+element+"\n"



        if setColors:
            for keypair in initial_values:
                text = text + name+keypair[0] +" input_for "+name+"\n"
                text = text + name+keypair[0] +" . value "+keypair[1]+"\n"
                text = text + name+keypair[0] +" as body_gen04_3"+keypair[0]+"\n"
        self.file.write(text)


    def addSwitch(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as switch\n\n"


        text = text +name+"_trigger output_for "+name+"\n"
        text = text +name+"_trigger . property trigger\n"
        text = text +name+"_trigger as switch_trigger\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as switch_part\n\n"

        for element in ["0","1","2","3"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as switch_slot_"+element+"\n"



        self.file.write(text)

    def addMagnet(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as magnet\n\n"


        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as magnet_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as magnet_slot_"+element+"\n"


        self.file.write(text)

    def addHook(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as hook\n\n"


        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as hook_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as hook_slot_"+element+"\n"


        self.file.write(text)


    def addElevator(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as elevator\n\n"


        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as elevator_part\n\n"

        for element in ["0","1","2"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as elevator_slot_"+element+"\n"

        text = text +name+"_position input_for "+name+"\n"
        text = text +name+"_position . property position\n"
        text = text +name+"_position . label position\n"
        text = text +name+"_position as elevator_position\n"


        self.file.write(text)


    def addGenerator(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as generator\n\n"


        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as generator_part\n\n"

        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as generator_slot_"+element+"\n"

        text = text +name+"_trigger input_for "+name+"\n"
        text = text +name+"_trigger . property trigger\n"
        text = text +name+"_trigger . label trigger\n"
        text = text +name+"_trigger as generator_trigger\n"


        self.file.write(text)



    def addSpotLight(self,name,pos,rot,initial_values = [["light_state","1"],["iris_state","1"]],usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as spot_light\n\n"


        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as spot_light_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as spot_light_slot_"+element+"\n"


        text = text +name+"_iris_state input_for "+name+"\n"
        text = text +name+"_iris_state . property iris\n"
        text = text +name+"_iris_state . label iris\n"
        text = text +name+"_iris_state as spot_iris_state\n"

        for keypair in initial_values:
            text = text +name+"_"+keypair[0]+" . value "+keypair[1]+"\n"




        self.file.write(text)



    def addPushButton(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as pushbutton\n\n"


        text = text +name+"_trigger output_for "+name+"\n"
        text = text +name+"_trigger . property trigger\n"
        text = text +name+"_trigger as pushbutton_trigger\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as pushbutton_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as pushbutton_slot_"+element+"\n"


        self.file.write(text)


    def addSensor(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as sensor\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as sensor_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as sensor_slot_"+element+"\n"


        self.file.write(text)


    def addSensorSonar(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as sensor_sonar\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as sensor_sonar_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as sensor_sonar_slot_"+element+"\n"


        self.file.write(text)

    def addSensorMicrophone(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as sensor_microphone\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as sensor_microphone_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as sensor_microphone_slot_"+element+"\n"


        self.file.write(text)


    def addFoot(self,name,pos,rot):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as foot\n\n"
        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as foot_slot_"+element+"\n"


        self.file.write(text)

    def addElbow(self,name,
        pos = "0,0,0",
        rot="0,0,0",
        angle_range ="-90,90,1",
        velocity_range = "-100,100,1",
        initial_values = [["angle","0"]],
        usepart = False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as elbow\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as elbow_part\n\n"

        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as elbow_slot_"+element+"\n"

        for keypair in initial_values:
            text = text +name+"_"+keypair[0]+" . value "+keypair[1]+"\n"



        text = text + name+"_angle input_for "+name+ "\n"
        text = text + name+"_angle as elbow_angle\n\n"

        text = text +name+"_angle . range ["+angle_range+"]\n"

        text = text + name+"_current_angle output_for "+name+ "\n"
        text = text + name+"_current_angle as elbow_current_angle\n\n"






#        text = text + name+"_velocity input_for "+name+ "\n"
#        text = text + name+"_velocity as elbow_velocity\n\n"
#
#
#        text = text +name+"_velocity . range ["+velocity_range+"]\n"
#
#


        self.file.write(text)

    def addBalancer(self,name,pos,kp="100",ki="0",kd="0"):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" as attitude_controller"+"\n\n"
        text = text+name+"_control output_for "+name+"\n"
        text = text+name+"_control as attitude_controller_control"+"\n\n"
        text = text+name+"_kp input_for "+name+"\n"
        text = text+name+"_kp . value "+kp+"\n"
        text = text+name+"_kp as attitude_controller_kp\n\n"
        text = text+name+"_ki input_for "+name+"\n"
        text = text+name+"_ki . value "+ki+"\n"
        text = text+name+"_ki as attitude_controller_ki\n\n"
        text = text+name+"_kd input_for "+name+"\n"
        text = text+name+"_kd . value "+kd+"\n"
        text = text+name+"_kd as attitude_controller_kd\n\n"
        text = text+name+"_master input_for "+name+"\n"
        text = text+name+"_master as attitude_controller_master\n\n"
        text = text+name+"_slave input_for "+name+"\n"
        text = text+name+"_slave as attitude_controller_slave\n\n"
        self.file.write(text)

    def addTimeLine(self,name,pos = "0,0,0",rot = "0,0,0",asset = "default",scale = "1"):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as timeline\n\n"
        text = text+name+" . savedfile "+asset+"\n"
        text = text+name+" . scale "+scale+"\n"



        #for keypair in initial_values:
        #text = text +name+"_"+keypair[0]+" . value "+keypair[1]+"\n"

        text = text +name+"_sample output_for "+name+"\n"
        text = text +name+"_sample as timeline_sample_def\n"
        self.file.write(text)


    def addTimeLineOperant(self,name,pos = "0,0,0",rot = "0,0,0",
        control_keys = ["angle1","angle2"]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as timeline_operant\n\n"
        #text = name+" . lenght 10\n"

        for key in control_keys:
# this output goes to the bridge and later to the movil part
            text = text +name+"_"+key+"_output output_for "+name+"\n"
            text = text +name+"_"+key+"_output . property part_id_"+key+"\n"
            text = text +name+"_"+key+"_output as timeline_output_def\n"

# this input is feeded by the motor or movil part, this must be set outside
            text = text +name+"_"+key+"_input input_for "+name+"\n"
            text = text +name+"_"+key+"_input . property part_id_"+key+"\n"
            text = text +name+"_"+key+"_input as timeline_input_def\n"

# this input is used to trigger the timeline
            text = text +name+"_trigger input_for "+name+"\n"
            text = text +name+"_trigger . property trigger\n"
            text = text +name+"_trigger as timeline_trigger\n"


# this are for the communication between timeline and bridge
            text = text +name+"_"+key+"_save output_for "+name+"\n"
            text = text +name+"_"+key+"_save . property data_frame\n"
            text = text +name+"_"+key+"_save as timeline_save_def\n"

            text = text +name+"_"+key+"_load input_for "+name+"\n"
            text = text +name+"_"+key+"_load . property data_frame\n"
            text = text +name+"_"+key+"_load as timeline_load_def\n"







        for key in control_keys:
            text = text + name+"_"+key+"_data . position ["+pos+"]\n"
            text = text + name+"_"+key+"_data . rotation ["+rot+"]\n"
            text = text + name+"_"+key+"_data as bridge\n\n"
            #text = text + name+"_"+key+"_data . name "+key+"\n\n"
            #text = text + name+"_"+key+"_data . sequence [0,0,0,0,0,0,0,1]\n\n"

            #this is the communication between timeline and the central output.
            text = text +name+"_"+key+"_sequence_out output_for "+name+"_"+key+"_data\n"
            text = text +name+"_"+key+"_sequence_out . property data_frame\n"
            text = text +name+"_"+key+"_sequence_out as bridge_sequence_out\n"

            text = text +name+"_"+key+"_sequence_in input_for "+name+"_"+key+"_data\n"
            text = text +name+"_"+key+"_sequence_in . property data_frame\n"
            text = text +name+"_"+key+"_sequence_in as bridge_sequence_in\n"

            # this output goes to the movil part, this is the one you must set outside
            text = text + name+"_"+key+"_data_out output_for "+name+"_"+key+"_data\n"
            text = text + name+"_"+key+"_data_out . property part_id_"+key+"\n"
            text = text + name+"_"+key+"_data_out as bridge_out\n"

            # this input is feeded by the timeline
            text = text + name+"_"+key+"_data_in input_for "+name+"_"+key+"_data\n"
            text = text + name+"_"+key+"_data_in . property part_id_"+key+"\n"
            text = text + name+"_"+key+"_data_in as bridge_in\n"




            text = text + name+"_"+key+"_sequence_in consumes "+name+"_"+key+"_save\n"
            text = text + name+"_"+key+"_load consumes "+name+"_"+key+"_sequence_out\n"
            text = text + name+"_"+key+"_data_in consumes "+name+"_"+key+"_output\n"

            for fr in ["0","1","2","3","4","5","6","7"]:
                text = text + name+"_"+key+"_frame_"+fr+" . position ["+pos+"]\n"
                text = text + name+"_"+key+"_frame_"+fr+" . rotation ["+rot+"]\n"

                text = text + name+"_"+key+"_frame_"+fr+" . id "+key+"\n"
                text = text + name+"_"+key+"_frame_"+fr+" . index "+fr+"\n"
                text = text + name+"_"+key+"_frame_"+fr+" . value 0\n"
                text = text + name+"_"+key+"_frame_"+fr+" . valid 0\n"

                text = text + name+"_"+key+"_frame_"+fr+" as timeline_frame\n\n"

                text = text + name+"_"+key+"_frame_"+fr+"_out output_for "+name+"_"+key+"_frame_"+fr+"\n"
                text = text + name+"_"+key+"_frame_"+fr+"_out . property frame\n"
                text = text + name+"_"+key+"_frame_"+fr+"_out as timeline_frame_out\n"

                text = text + name+"_"+key+"_frame_"+fr+"_in input_for "+name+"_"+key+"_frame_"+fr+"\n"
                text = text + name+"_"+key+"_frame_"+fr+"_in . property frame\n"
                text = text + name+"_"+key+"_frame_"+fr+"_in as timeline_frame_in\n"

                # for each frame we made a double link to the bridge

                text = text +name+"_"+key+"_frame_"+fr+"_sequence_out output_for "+name+"_"+key+"_data\n"
                text = text +name+"_"+key+"_frame_"+fr+"_sequence_out . property frame\n"
                text = text +name+"_"+key+"_frame_"+fr+"_sequence_out as bridge_sequence_out\n"

                text = text +name+"_"+key+"_frame_"+fr+"_sequence_in input_for "+name+"_"+key+"_data\n"
                text = text +name+"_"+key+"_frame_"+fr+"_sequence_in . property frame\n"
                text = text +name+"_"+key+"_frame_"+fr+"_sequence_in as bridge_sequence_in\n"

                text = text +name+"_"+key+"_frame_"+fr+"_sequence_in consumes "+name+"_"+key+"_frame_"+fr+"_out\n"
                text = text + name+"_"+key+"_frame_"+fr+"_in consumes "+name+"_"+key+"_frame_"+fr+"_sequence_out\n"











        self.file.write(text)

    def addOscilloscope(self,name,pos = "0,0,0",rot = "0,0,0",
        scale_range = "-90,90,0.1",
        sample_range = "-90,90,1",
        initial_values = [["scale","10"],["sample","10"]]):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as oscilloscope\n\n"

        for keypair in initial_values:
            text = text +name+"_"+keypair[0]+" . value "+keypair[1]+"\n"



        text = text + name+"_sample input_for "+name+ "\n"
        text = text + name+"_sample as oscilloscope_sample\n\n"


        text = text+name+"_sample . range ["+sample_range+"]\n"





        text = text + name+"_scale input_for "+name+ "\n"
        text = text + name+"_scale as oscilloscope_scale\n\n"


        text = text+name+"_scale . range ["+scale_range+"]\n"



        self.file.write(text)

    def addKeyBoardControl(self):
        text = \
"""
km01 as keyboard_vehicle
km01 . label machine_a
km01 . position [6,-6,0]

km01_right_control output_for km01
km01_right_control as vehicle_right_output

km01_left_control output_for km01
km01_left_control as vehicle_left_output

km_binding01 as keybinding
km_binding01 . position [-4,3,0]
km_binding01 . activation A
km_binding01 . continuous True
km_binding01 . priority 4
km_binding01 . action TurnLeft

km_binding02 as keybinding
km_binding02 . position [-4,0,0]
km_binding02 . activation D
km_binding02 . continuous True
km_binding02 . priority 3
km_binding02 . action TurnRight

km_binding03 as keybinding
km_binding03 . position [-4,3,0]
km_binding03 . activation W
km_binding03 . continuous True
km_binding03 . priority 2
km_binding03 . action IncreaseSpeed

km_binding04 as keybinding
km_binding04 . position [-4,0,0]
km_binding04 . activation S
km_binding04 . continuous True
km_binding04 . priority 1
km_binding04 . action ReduceSpeed

ktkm01 as binding_event
ktkm01 . position [6,-3,0]

ktkm01_stroke input_for ktkm01
ktkm01_stroke as stroke_input


km_binding01 binds ktkm01
km_binding02 binds ktkm01
km_binding03 binds ktkm01
km_binding04 binds ktkm01
ktkm01_stroke consumes kb00_risen_action
ktkm01 activates km01
"""
        self.file.write(text)


    def addJetKeyBoardControl(self):
        text = \
"""
km01 as keyboard_jet
km01 . label machine_a
km01 . position [6,-6,0]

km01_right_control output_for km01
km01_right_control as jet_right_output

km01_left_control output_for km01
km01_left_control as jet_left_output

km_binding01 as keybinding
km_binding01 . position [-4,3,0]
km_binding01 . activation A
km_binding01 . continuous True
km_binding01 . priority 4
km_binding01 . action TurnLeft

km_binding02 as keybinding
km_binding02 . position [-4,0,0]
km_binding02 . activation D
km_binding02 . continuous True
km_binding02 . priority 3
km_binding02 . action TurnRight

km_binding03 as keybinding
km_binding03 . position [-4,3,0]
km_binding03 . activation W
km_binding03 . continuous True
km_binding03 . priority 2
km_binding03 . action IncreaseSpeed

km_binding04 as keybinding
km_binding04 . position [-4,0,0]
km_binding04 . activation S
km_binding04 . continuous True
km_binding04 . priority 1
km_binding04 . action ReduceSpeed

ktkm01 as binding_event
ktkm01 . position [6,-3,0]

ktkm01_stroke input_for ktkm01
ktkm01_stroke as stroke_input


km_binding01 binds ktkm01
km_binding02 binds ktkm01
km_binding03 binds ktkm01
km_binding04 binds ktkm01
ktkm01_stroke consumes kb00_risen_action
ktkm01 activates km01
"""
        self.file.write(text)
    def addFist(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as fist\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as fist_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as fist_slot_"+element+"\n"


        self.file.write(text)

    def addHelix(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as helix\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as helix_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as helix_slot_"+element+"\n"


        self.file.write(text)

    def addTripleHelix(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as triple_helix\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as triple_helix_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as triple_helix_slot_"+element+"\n"


        self.file.write(text)

    def addMidiInput(self,name,pos="0,0,0",rot="0,0,0",controls=[["name","0"]],notes=[["name","0"]]):
        self.title(name);
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as midi_input\n\n"

        for control in controls:
            text = text + name+"_"+control[0]+" output_for "+name+"\n"
            text = text + name+"_"+control[0]+" . label control_"+control[1]+"\n"
            text = text + name+"_"+control[0]+" . property control_"+control[1]+"\n"
            text = text + name+"_"+control[0]+" as midi_input_sample\n\n"
        self.file.write(text)

        for note in notes:
            text = text + name+"_"+note[0]+" output_for "+name+"\n"
            text = text + name+"_"+note[0]+" . label note_on_"+note[1]+"\n"
            text = text + name+"_"+note[0]+" . property note_on_"+note[1]+"\n"
            text = text + name+"_"+note[0]+" as midi_input_sample_note_on\n\n"

            text = text + name+"_"+note[0]+" output_for "+name+"\n"
            text = text + name+"_"+note[0]+" . label note_off_"+note[1]+"\n"
            text = text + name+"_"+note[0]+" . property note_off_"+note[1]+"\n"
            text = text + name+"_"+note[0]+" as midi_input_sample_note_off\n\n"

        self.file.write(text)

    def addMidiMap(self,name,pos="0,0,0",rot="0,0,0",map="0,127,0",control="0"):
        self.title(name);
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as midi_map\n\n"

        text = text+name+" . map ["+map+"]\n\n"
        text = text+name+" . control "+control+"\n\n"

        text = text + name+"_input input_for "+name+"\n"
        text = text + name+"_input . label control_"+control+"\n"
        text = text + name+"_input as midi_map_input\n\n"

        text = text + name+"_sample output_for "+name+"\n"
        text = text + name+"_sample as midi_map_sample\n\n"
        self.file.write(text)



    def addCSColumn(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as cs_column\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as cs_column_part\n\n"

        for element in ["0", "1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as cs_column_slot_"+element+"\n"


        self.file.write(text)


    def addCSArc(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as cs_arc\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as cs_arc_part\n\n"

        for element in ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as cs_arc_slot_"+element+"\n"


        self.file.write(text)


    def addCSBigRail(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as cs_big_rail\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as cs_big_rail_part\n\n"

        for element in ["0","1","2","3","4","5","6","7","8","9","10","11"]:#,"12","13","14","15","16","17","18","19","20","21"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as cs_big_rail_slot_"+element+"\n"


        self.file.write(text)

    def addCSRail(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as cs_rail\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as cs_rail_part\n\n"

        for element in ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"]:#,"16","17","18","19","20","21"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as cs_rail_slot_"+element+"\n"


        self.file.write(text)


    def addCSHangar(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as cs_hangar\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as cs_hangar_part\n\n"

        for element in ["0","1","2","3","4","5","6","7","8","9","10","11"]:#,"12","13","14","15","16","17","18","19","20","21"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as cs_hangar_slot_"+element+"\n"


        self.file.write(text)


    def addExtractor(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as extractor\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as extractor_part\n\n"

        for element in ["0"]:#,"1","2","3","4","5","6","7","8","9","10","11"]:#,"12","13","14","15","16","17","18","19","20","21"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as extractor_slot_"+element+"\n"

    # this input is used to trigger the timeline
        text = text +name+"_trigger input_for "+name+"\n"
        text = text +name+"_trigger . property trigger\n"
        text = text +name+"_trigger as extractor_trigger\n"




        self.file.write(text)

    def addBot(self,name,pos,rot,bot_name):
        self.title(name)
        text = name + " . position ["+pos+"]\n"
        text = text + name+" . rotation ["+rot+"]\n"
        text = text + name +" . bot_definition "+ bot_name +"\n"
        text = text + name+" as bot\n\n"

        text = text + name+"_self output_for "+name+ "\n"
        text = text +name+"_self . property self\n"
        text = text +name+"_self . label self\n"
        text = text + name+"_self as bot_self\n\n"

        self.file.write(text)

    def addCrystal(self,name,pos,rot):
        self.title(name)
        text = name + " . position ["+pos+"]\n"
        text = text + name+" . rotation ["+rot+"]\n"
        text = text + name+" as crystal\n\n"





        self.file.write(text)

    def addCamera(self,name,
        pos = "0,0,0",
        rot ="0,0,0",):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as camera\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as camera_slot_"+element+"\n"




        self.file.write(text)


    def addFloatingDevice(self,name,
        pos = "0,0,0",
        rot ="0,0,0",):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as floating_device\n\n"

        for element in ["0","1","2"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as floating_device_slot_"+element+"\n"





        self.file.write(text)


    def addHover(self,name,
        pos = "0,0,0",
        rot ="0,0,0",):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as hover\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as hover_slot_"+element+"\n"





        self.file.write(text)

    def addGrenade(self,name,
        pos = "0,0,0",
        rot ="0,0,0",):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as grenade\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as grenade_slot_"+element+"\n"





        self.file.write(text)



    def addSpeaker(self,name,
        pos = "0,0,0",
        rot ="0,0,0",):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as speaker\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as speaker_slot_"+element+"\n"





        self.file.write(text)

    def addPointLight(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as point_light\n\n"


        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as point_light_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as point_light_slot_"+element+"\n"



        self.file.write(text)

    def addHexEarth(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as hex_earth\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as hex_earth_part\n\n"

        for element in ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as hex_earth_slot_"+element+"\n"



        self.file.write(text)

    def addHexPlat(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as hex_plat\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as hex_plat_part\n\n"

        for element in ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as hex_plat_slot_"+element+"\n"



        self.file.write(text)

    def addHexColumn(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as hex_column\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as hex_column_part\n\n"

        for element in ["00", "01", "02", "03", "04", "05"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as hex_column_slot_"+element+"\n"



        self.file.write(text)

    def addWall(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as wall\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as wall_part\n\n"

        for element in ["00", "01", "02", "03", "04", "05", "06", "07"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as wall_slot_"+element+"\n"


        self.file.write(text)

    def addLaser(self,name,pos,rot,length,usepart=True):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as laser\n\n"


        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as laser_part\n\n"

        text += name + "_length input_for "+name+"\n"
        text += name + "_length as laser_length\n"
        text += name + "_length . value " + length + "\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as laser_slot_"+element+"\n"
        self.file.write(text)

    def addIsland(self,name,
        pos = "0,0,0",
        rot ="0,0,0",):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as island\n\n"
        self.file.write(text)

    def addHydroJet(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as hydro_jet\n\n"

        text = text + name+"_thrust input_for "+name+ "\n"
        text = text + name+"_thrust as hydro_jet_thrust\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as hydro_jet_part\n\n"

        for element in ["0","1"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as hydro_jet_slot_"+element+"\n"
        self.file.write(text)

    def addFin(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as fin\n\n"


        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as fin_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as fin_slot_"+element+"\n"


        self.file.write(text)

    def addAntenna(self,name,pos,rot,usepart=False):
        self.title(name)
        text = name+" . position ["+pos+"]\n"
        text = text+name+" . rotation ["+rot+"]\n"
        text = text +name+" as antenna\n\n"

        if usepart:
            text = text + name+"_part output_for "+name+ "\n"
            text = text + name+"_part as antenna_part\n\n"

        for element in ["0"]:
            text = text + name+"_slot_"+element+" slot_for "+name+ "\n"
            text = text + name+"_slot_"+element+" as antenna_slot_"+element+"\n"


        self.file.write(text)

    def addPlasmaCannon(self, name, pos, rot):
        self.title(name)
        text = name + " . position [" + pos + "]\n"
        text+= name + " . rotation " + "[" + rot + "]\n"
        text+= name + " as plasma_cannon\n\n"

        text+= name + "_shoot input_for " + name + "\n"
        text+= name + "_shoot as plasma_cannon_shoot\n\n"

        text+= name + "_feed input_for " + name + "\n"
        text+= name + "_feed as plasma_cannon_feed\n\n"

        text+= name + "_slot_0 slot_for " + name + "\n"
        text+= name + "_slot_0 as plasma_cannon_slot_0\n"
        self.file.write(text)

    def addFluxCapacitor(self, name, pos, rot, level = "1"):
        self.title(name)
        text = name + " . position [" + pos + "]\n"
        text+= name + " . rotation [" + rot + "]\n"
        text+= name + " as flux_capacitor\n"

        text+= name + "_level input_for " + name +"\n"
        text+= name + "_level as flux_capacitor_level\n"
        text+= name + "_level . value " + level + "\n"

        text+= name + "_part output_for " + name + "\n"
        text+= name + "_part as flux_capacitor_part\n"

        text+= name + "_slot_0 slot_for " + name + "\n"
        text+= name + "_slot_0 as flux_capacitor_slot_0\n"
        self.file.write(text)

    def addGraphChunk(self, title, chunk):
        self.title(title)
        self.file.write(chunk)


    def addInputSampler(self, name, controls = { "positive" : "A", "negative" : "D"},attack="1", release="1", invert="False"):
        self.title(name)
        text =  name + " as input_sampler\n"
        text += name + " . name " + name + "\n"
        text += name + " . positive " + controls["positive"] + "\n"
        if "negative" in controls:
            text += name + " . negative " + controls["negative"] + "\n"

        text += name + " . attack " + attack + "\n"
        text += name + " . release " + release + "\n"
        text += name + " . invert " + invert + "\n"

        text += name + "_sample output_for " + name + "\n"
        text += name + "_sample as input_sampler_sample \n"

        self.file.write(text)

    def addSampleMapper(self, name, mapmin, mapmax, samplemin="0", samplemax="1", invert="False"):
        self.title(name)
        text =  name+ " as sample_mapper\n"
        text += name+" . sample_min "+samplemin+"\n"
        text += name+" . sample_max "+samplemax+"\n"
        text += name+" . mapped_min "+mapmin+"\n"
        text += name+" . mapped_max "+mapmax+"\n"
        text += name+" . invert "+invert+"\n"

        text += name+"_sample input_for " +name+"\n"
        text += name+"_sample as sample_mapper_sample\n"

        text += name+"_mapped output_for " +name+"\n"
        text += name+"_mapped as sample_mapper_mapped\n"

        text += name+"_inverted output_for " +name+"\n"
        text += name+"_inverted as sample_mapper_inverted\n"

        self.file.write(text)