import library

with open('../../Resources/Bots/pa_boat.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	def getCoords(partname):
		return str(podboat[partname]['position'][0])+","+str(podboat[partname]['position'][1])+","+str(podboat[partname]['position'][2])	
	def getRot(partname):
		return str(podboat[partname]['rotation'][0])+","+str(podboat[partname]['rotation'][1])+","+str(podboat[partname]['rotation'][2])	


	podboat = {
		"floatleft" : {
			"position" : [-2,0,-2],
			"rotation" : [0,90,90],
			"type" : "floatingdevice",
			"extend" : [{
				"slot" : 0,
				"tailname" : "contfront",
				"tailslot" : 5	
			}]
		},
		"floatright" : {
			"position" : [-2,0,2],
			"rotation" : [0,90,-90],
			"type" : "floatingdevice",
			"extend" : [{
				"slot" : 0,
				"tailname" : "contfront",
				"tailslot" : 2	
			}]			
		},
		"floatback" : {
			"position" : [3,0,0],
			"rotation" : [0,90,180],
			"type" : "floatingdevice"
		},
		"contfront" : {
			"position" : [-2,0,0],
			"rotation" : [0,0,0],
			"type" : "continuum",
			"extend" : [{
				"slot" : 0,
				"tailname" : "contmiddle",
				"tailslot" : 3
			}]			
		},
		"camelbow" : {
			"position" : [-2,2,0],
			"rotation" : [0,90,-90],
			"type" : "elbow",
			"extend" : [{
				"slot" : 0,
				"tailname" : "contfront",
				"tailslot" : 1
			}]			
		},
		"contmiddle" : {
			"type" : "continuum",
			"position" : [0,0,0],
			"rotation" : [0,0,0],
			"extend" : [{
				"slot" : 1,
				"tailname" : "conttop",
				"tailslot" : 4	
			},
			{
				"slot" : 0,
				"tailname" : "floatback",
				"tailslot" : 2
			}]				
		},
		"conttop" : {
			"type" : "continuum",		
			"position" : [0,2,0],
			"rotation" : [0,0,0],
			"extend" : []
		},
		"motor01" : {
			"type" : "motor",
			"position" : [3,-2,0],
			"rotation" : [0,0,180],
			"extend" : [{
				"slot" : 1,
				"tailname" : "floatback",
				"tailslot" : 0
			}]
		},
		"jet01" : {
			"type" : "hydrojet",
			"position" : [3,-4,0],
			"rotation" : [0,0,90],
			"extend" : [{
				"slot" : 0,
				"tailname" : "motor01",
				"tailslot" : 0
			}]
		}
	}


	for part in podboat:
		if podboat[part]["type"] == "hydrojet":
			obj.addHydroJet(part, getCoords(part), getRot(part))
		elif podboat[part]["type"] == "core":
			obj.addCore(part, getCoords(part), getRot(part))
		elif podboat[part]["type"] == "continuum":
			obj.addContinuum(part, getCoords(part), getRot(part))
		elif podboat[part]["type"] == "floatingdevice":
			obj.addFloatingDevice(part, getCoords(part), getRot(part))
		elif podboat[part]["type"] == "elbow":
			obj.addElbow(part, getCoords(part), getRot(part))
		elif podboat[part]["type"] == "fin":
			obj.addFin(part, getCoords(part), getRot(part))
		elif podboat[part]["type"] == "motor":
			obj.addMotor(part, getCoords(part), getRot(part))
		if "extend" in podboat[part]:
			for extendInfo in podboat[part]["extend"]:
				obj.extend(part,str(extendInfo["slot"]),extendInfo["tailname"],str(extendInfo["tailslot"]))