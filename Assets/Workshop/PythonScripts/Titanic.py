import library

with open('../../BotDefinitions/titanic.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()
	obj.addCore("master","0,0,0","0,0,0")
	obj.addContinuum("box001","4,0,0","0,0,0")
	obj.addContinuum("box002","-4,0,0","0,0,0")
	obj.addContinuum("box003","0,0,4","0,0,0")
	obj.addContinuum("box004","0,0,-4","0,0,0")

	obj.addFloatingDevice("float001","6,0,0","0,0,90")
	obj.addFloatingDevice("float002","-7,0,0","0,0,-90")
	obj.addFloatingDevice("float003","0,0,7","0,90,-90")
	obj.addFloatingDevice("float004","0,0,-7","0,90,90")


	obj.extend("master","0","box001","3")
	obj.extend("master","3","box002","0")
	obj.extend("master","2","box003","5")
	obj.extend("master","5","box004","2")


	obj.extend("box001","0","float001","0")
	obj.extend("box002","3","float002","0")
	obj.extend("box003","2","float003","0")
	obj.extend("box004","5","float004","0")