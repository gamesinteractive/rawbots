import library

with open('../../BotDefinitions/max5.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("master","0,0,0","0,0,0")


	obj.extend("middlebox","3","master","0")
	obj.addContinuum("middlebox","2,0,0","0,0,0")


	obj.extend("box1","3","middlebox","0")
	obj.addContinuum("box1","4,0,0","0,0,0")

	obj.extend("upperbox","4","middlebox","1")
	obj.addContinuum("upperbox","2,4,0","0,0,0")

	obj.addContinuum("leg1","8,0,3","0,0,0")
	obj.extend("leg1","5","box1","2")

	obj.addMotor("leg1motor","8,0,5","-90,180,0")
	obj.extend("leg1motor","1","leg1","2")

	obj.addWheel("wheel1","8,0,7","0,180,0")
	obj.extend("wheel1","0","leg1motor","0")
	

	obj.addContinuum("leg2","8,0,-3","0,0,0")
	obj.extend("leg2","2","box1","5")

	obj.addMotor("leg2motor","8,0,-5","-90,0,0")
	obj.extend("leg2motor","1","leg2","2")

	obj.addWheel("wheel2","8,0,-7","0,0,0")
	obj.extend("wheel2","0","leg2motor","0")


	obj.addContinuum("leg3","-4,0,3","0,0,0")
	obj.extend("leg3","5","master","2")

	obj.addMotor("leg3motor","-4,0,5","-90,180,0")
	obj.extend("leg3motor","1","leg3","2")
	obj.addWheel("wheel3","-4,0,7","0,180,0")
	obj.extend("wheel3","0","leg3motor","0")	


	obj.addContinuum("leg4","-4,0,-3","0,0,0")
	obj.extend("leg4","2","master","5")

	obj.addMotor("leg4motor","-4,0,-5","-90,0,0")
	obj.extend("leg4motor","1","leg4","2")
	obj.addWheel("wheel4","-4,0,-7","0,0,0")
	obj.extend("wheel4","0","leg4motor","0")

	obj.addBody_gen03_1("front_shield","-2,1,0","180,-90,0")
	obj.extend("front_shield","1","master","1")

	obj.addBody_gen03_1("back_shield","6,1,0","180,90,0")
	obj.extend("back_shield","1","box1","1")

	obj.addBody_gen03_2("shield2","7,1,3","-90,0,0")
	obj.extend("shield2","0","leg1","1")
	obj.addBody_gen03_2("shield3","7,1,-3","-90,0,0")
	obj.extend("shield3","0","leg2","1")
	obj.addBody_gen03_3("shield4","-5,4,0","90,90,0")
	obj.extend("shield4","0","upperbox","3")


#	obj.addJet("jet001", "0,6,0","-90,-90,0")
#	obj.extend("jet001", "1","upperbox","1")
#
#	obj.addKeyBoardControl()
#	obj.consume("leg1motor","velocity","km01","left_control")
#	obj.consume("leg3motor","velocity","km01","left_control")
#	obj.consume("leg2motor","velocity","km01","left_control")
#	obj.consume("leg4motor","velocity","km01","right_control")
#
#	obj.addMidiInput("midi001",controls=[["right_map","10"],["left_map","11"],["downspeed_map","12"],["upspeed_map","13"],["fire_map","14"]])
#	obj.addMidiMap("right_map",map="0,1,10",control="10")
#	obj.addMidiMap("left_map",map="0,1,11",control="11")
#	obj.addMidiMap("downspeed_map",map="0,1,12",control="12")
#	obj.addMidiMap("upspeed_map",map="0,1,13",control="13")
#	obj.addMidiMap("fire_map",map="0,1,14",control="14")
#	
#	obj.consume("right_map","input","midi001","right_map")
#	obj.consume("left_map","input","midi001","left_map")
#	obj.consume("upspeed_map","input","midi001","upspeed_map")
#	obj.consume("downspeed_map","input","midi001","downspeed_map")
#	obj.consume("fire_map","input","midi001","fire_map")
	
