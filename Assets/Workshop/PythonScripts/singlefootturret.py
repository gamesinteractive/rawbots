import library

with open('../../Resources/Bots/singlefootturret.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addMotor("motorbase","0,2,0","0,0,0")
	obj.addElbow("elbow1","0,4,0","0,0,90","-90,90,1","-100,100,1")
	obj.addFoot("foot01","0,-2,0","0,0,0")
	obj.addPlasmaCannon("plasma1","0,7,0","0,0,0")
	obj.addContinuum("cc1","0,0,0","0,0,270")
	obj.addFluxCapacitor("cap1","2,0,0","0,0,270")
	obj.addFluxCapacitor("cap2","-2,0,0","0,0,90")

	obj.consume("plasma1", "feed","cap1","part")
	obj.consume("plasma1","feed", "cap2", "part")

	obj.extend("motorbase","1","elbow1","1")
	obj.extend("plasma1","0","elbow1","0")

	obj.extend("foot01","0","cc1","4")
	obj.extend("cc1","1","motorbase","0")

	obj.extend("cap1","0","cc1","5")
	obj.extend("cap2","0","cc1","2")

	obj.addGraphChunk("input definition", """
		#keyboard
kb01 as keyboard_sensor

kb01_ev output_for kb01
kb01_ev as keyboard_event

#inputmanager
input001 as input_manager

input001_event input_for input001
input001_event as input_event

input001_subs input_for input001
input001_subs as input_subscription

#input setup
side_motion as input_setup
side_motion . positive A
side_motion . negative D
side_motion . sensitivity 1
side_motion . gravity 0
side_motion . axis True

side_motion_subs output_for side_motion
side_motion_subs as setup_subscriptor

side_motion_action output_for side_motion
side_motion_action as setup_action

#input linear_mapper
right_side_map as linear_mapper
right_side_map . map [0,180,1]
right_side_raw input_for right_side_map
right_side_raw as linear_mapper_raw
right_side_mapped output_for right_side_map
right_side_mapped as linear_mapper_mapped
right_side_raw consumes side_motion_action

input001_event consumes kb01_ev
input001_subs consumes side_motion_subs

motorbase_angle consumes right_side_mapped



#input setup elbowanglecontrol
elbow_angle_input as input_setup
elbow_angle_input . positive W
elbow_angle_input . negative S
elbow_angle_input . sensitivity 1
elbow_angle_input . gravity 0
elbow_angle_input . axis True

elbow_angle_input_subs output_for elbow_angle_input
elbow_angle_input_subs as setup_subscriptor

elbow_angle_input_action output_for elbow_angle_input
elbow_angle_input_action as setup_action


#elbowInputMapper
elbow_angle_map as linear_mapper
elbow_angle_map . map [0,90,1]

elbow_angle_raw input_for elbow_angle_map
elbow_angle_raw as linear_mapper_raw

elbow_angle_mapped output_for elbow_angle_map
elbow_angle_mapped as linear_mapper_mapped

elbow_angle_raw consumes elbow_angle_input_action

input001_subs consumes elbow_angle_input_subs

elbow1_angle consumes elbow_angle_mapped

######################################################

fire_setup as input_setup

fire_setup . name fire_setup
fire_setup . positive Space
fire_setup . sensitivity 0
fire_setup . gravity 1
fire_setup . axis False

fire_setup_subscriptor output_for fire_setup
fire_setup_subscriptor as setup_subscriptor

fire_setup_action output_for fire_setup
fire_setup_action as setup_action

input001_subs consumes fire_setup_subscriptor

plasma1_shoot consumes fire_setup_action""")