import library

with open('../../BotDefinitions/FloatingIsland.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	#Creating terrain
	obj.addHexEarth("terrain000","0,0,0","0,0,0")
	obj.addHexEarth("terrain001","70,0,40","0,0,0")
	obj.addHexEarth("terrain002","0,0,81","0,0,0")

	#Creating hover
	obj.addHover("hover000","-22,-20,-42","0,270,0")
	obj.addHover("hover001","22,-20,-42","0,270,0")
	obj.addHover("hover002","-25,-20,-41","0,330,0")
	obj.addHover("hover003","-48,-20,-2","0,330,0")
	obj.addHover("hover004","-22,-20,123","0,90,0")
	obj.addHover("hover005","22,-20,123","0,90,0")
	obj.addHover("hover006","-25,-20,122","0,30,0")
	obj.addHover("hover007","-48,-20,83","0,30,0")
	obj.addHover("hover008","95,-20,-1","0,210,0")
	obj.addHover("hover009","118,-20,38","0,210,0")
	obj.addHover("hover010","118,-20,42","0,150,0")
	obj.addHover("hover011","95,-20,81","0,150,0")

	#Connecting terrain000 to terrain002
	obj.extend("terrain000","04","terrain002","00")
	obj.extend("terrain000","05","terrain002","01")
	obj.extend("terrain000","16","terrain002","12")
	obj.extend("terrain000","17","terrain002","13")

	#Connecting terrain000 to terrain001
	obj.extend("terrain000","06","terrain001","03")
	obj.extend("terrain000","07","terrain001","02")
	obj.extend("terrain000","18","terrain001","15")
	obj.extend("terrain000","19","terrain001","14")

	#Connecting terrain001 to terrain002
	obj.extend("terrain001","10","terrain002","09")
	obj.extend("terrain001","11","terrain002","08")
	obj.extend("terrain001","22","terrain002","21")
	obj.extend("terrain001","23","terrain002","20")

	#Connecting terrain000 to hovers
	obj.extend("terrain000","12","hover000","0")
	obj.extend("terrain000","13","hover001","0")
	obj.extend("terrain000","14","hover002","0")
	obj.extend("terrain000","15","hover003","0")

	#Connecting terrain002 to hovers
	obj.extend("terrain002","16","hover004","0")
	obj.extend("terrain002","17","hover005","0")
	obj.extend("terrain002","23","hover006","0")
	obj.extend("terrain002","22","hover007","0")

	#Connecting terrain001 to hovers
	obj.extend("terrain001","21","hover008","0")
	obj.extend("terrain001","20","hover009","0")
	obj.extend("terrain001","19","hover010","0")
	obj.extend("terrain001","18","hover011","0")

