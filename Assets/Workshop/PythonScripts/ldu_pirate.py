import library

with open('../../Resources/Bots/ldu-pirate.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core00","0,0,0","0,0,0")

	#obj.addFloatingDevice("floater001","4,0,0","0,0,90")
	#obj.addFin("floater001","4,0,0","0,0,0")
	#obj.addWideWheelSmall("center_right_wheel","4,0,0","0,90,0")
	#obj.extend("floater001","0","core00","0")

	#obj.addFloatingDevice("floater002","-4,0,0","0,0,270")
	#obj.addFin("floater002","-4,0,0","0,180,0")
	#obj.addWideWheelSmall("center_left_wheel","-4,0,0","0,90,0")
	#obj.extend("floater002","0","core00","3")

	obj.addContinuum("back_wheel_support","0,0,-4","0,0,0")
	obj.extend("back_wheel_support","2","core00","5")

	obj.addFloatingDevice("floater003","4,0,-4","0,0,90")
	#obj.addWideWheelSmall("back_right_wheel","4,0,-4","0,90,0")
	obj.extend("floater003","0","back_wheel_support","0")

	obj.addFloatingDevice("floater004","-4,0,-4","0,0,270")
	#obj.addWideWheelSmall("back_left_wheel","-4,0,-4","0,90,0")
	obj.extend("floater004","0","back_wheel_support","3")

	obj.addContinuum("front_wheel_support","0,0,4","0,0,0")
	obj.extend("front_wheel_support","5","front_support","2")

	#obj.addFloatingDevice("floater005","4,0,4","0,0,90")
	#obj.addWideWheelSmall("front_right_wheel","4,0,4","0,90,0")
	obj.extend("floater005","0","front_wheel_support","0")

	#obj.addFloatingDevice("floater006","-4,0,4","0,0,270")
	#obj.addWideWheelSmall("front_left_wheel","-4,0,4","0,90,0")
	obj.extend("floater006","0","front_wheel_support","3")

	obj.addContinuum("front_support","0,0,2","0,0,0")
	obj.extend("front_support","2","front_wheel_support","5")
	obj.extend("core00","2","front_support","5")

	obj.addContinuum("hip","0,2,2","0,0,0")
	obj.addMotor("helicoptermotor","0,8,2","0,0,0", initial_values=[["velocity","2000"]])
	obj.extend("hip","4","front_support","1")
	obj.extend("helicoptermotor","0","hip","1")
	obj.addContinuum("continuum001","0,15,2","0,0,0")
	obj.extend("helicoptermotor","1","continuum001","4")

	obj.addContinuum("shield_support","0,2,4","0,0,0")
	obj.extend("shield_support","4","front_wheel_support","1")

	#obj.extend("floater003","1","floater001","2")
	#obj.extend("floater001","1","floater005","2")
	#obj.extend("floater004","1","floater002","2")
	#obj.extend("floater002","1","floater006","2")

	#obj.addFin("fin001","-6,15,2","20,180,0")
	obj.addFin("fin002","0,15,-8","20,90,0")
	#obj.addFin("fin003","6,15,2","20,0,0")
	obj.addFin("fin004","0,15,12","20,270,0")
	obj.addFin("fin005","0,15,20","90,270,0")
	#obj.addFin("fin006","0,0,-6","90,90,0")

	#obj.addFin("fin007","0,-2,-4","0,0,270")
	#obj.extend("fin007","0","front_wheel_support","5")

	#obj.addFin("fin007","4,0,-7","10,90,0")
	#obj.addFin("fin008","-4,0,-7","10,90,0")
	#obj.addFin("fin009","4,0,7","10,270,0")
	#obj.addFin("fin010","-4,0,7","10,270,0")
	obj.addJet("jet01","4,0,7","0,270,90")
	obj.addJet("jet02","-4,0,7","0,270,90")

	#obj.extend("floater003","2","fin007","0")
	#obj.extend("floater004","2","fin008","0")
	#obj.extend("floater005","1","fin009","0")
	#obj.extend("floater006","1","fin010","0")

	obj.extend("front_wheel_support","0","jet01","0")
	obj.extend("front_wheel_support","3","jet02","1")

	obj.extend("front_wheel_support","2","fin005","0")
	#obj.extend("back_wheel_support","5","fin006","0")

	#obj.extend("continuum001","3","fin001","0")
	obj.extend("continuum001","5","fin002","0")
	#obj.extend("continuum001","0","fin003","0")
	obj.extend("continuum001","2","fin004","0")

	obj.addContinuum("iconicbelly01","0,2,-4","0,0,0")
	obj.addContinuum("iconicbelly02","0,4,-4","0,0,0")
	obj.addContinuum("iconicbelly03","0,4,-6","0,0,0")

	obj.extend("back_wheel_support","1","iconicbelly01","4")
	obj.extend("iconicbelly01","1","iconicbelly02","4")
	obj.extend("iconicbelly02","3","iconicbelly03","0")

	obj.addElbow("iconicelbow01","2,4,-4","0,180,0", initial_values=[["angle","90"]])
	obj.addElbow("iconicelbow02","-2,4,-4","0,0,0", initial_values=[["angle","-90"]])

	obj.extend("iconicbelly02","0","iconicelbow01","0")
	obj.extend("iconicbelly02","3","iconicelbow02","0")

	obj.addSpotLight("iconiclight01","-4,4,-4","0,0,90")
	obj.addSpotLight("iconiclight02","4,4,-4","0,0,-90")

	obj.extend("iconiclight01","0","iconicelbow02","1")
	obj.extend("iconiclight02","0","iconicelbow01","1")

	obj.addBody_cvr_tlt_gen01_05("carrier","0,4,-6","0,180,0",setColors=True,initial_values=[["_channel_a","#FFB619"],["_channel_b","#2E4130"],["_channel_c","#0E1528"]])

	obj.extend("carrier","0","iconicbelly03","3")
