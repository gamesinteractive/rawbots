import library

with open('../../Resources/Bots/realspider.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addContinuum("continuum0","-2,0,-4","0,0,0")
	obj.addContinuum("continuum1","0,0,2","0,0,0")
	obj.addContinuum("continuum2","0,0,-4","0,0,0")
	obj.addContinuum("continuum3","22,0,0","0,0,0")
	obj.addContinuum("continuum4","-2,0,0","0,0,0")
	obj.addContinuum("continuum5","0,0,4","0,0,0")
	obj.addContinuum("continuum6","2,0,2","0,0,0")
	obj.addContinuum("continuum7","2,0,4","0,0,0")
	obj.addContinuum("continuum8","22,0,4","0,0,0")
	obj.addContinuum("continuum9","0,0,-2","0,0,0")
	obj.addContinuum("continuum10","2,0,-4","0,0,0")
	obj.addContinuum("continuum11","-2,0,-2","0,0,0")
	obj.addContinuum("continuum12","2,0,-2","0,0,0")
	obj.addContinuum("continuum13","-2,0,2","0,0,0")
	obj.addContinuum("continuum14","-2,0,4","0,0,0")
	obj.addContinuum("continuum15","2,0,0","0,0,0")
	obj.addContinuum("continuum16","0,0,0","0,0,0")
	obj.addContinuum("continuum17","22,0,-4","0,0,0")
	obj.addContinuum("continuum18","-22,0,4","0,0,0")
	obj.addContinuum("continuum19","-22,0,-4","0,0,0")
	obj.addContinuum("continuum20","-22,0,0","0,0,0")

	#horizontal elbows
	obj.addElbow("elbow24","-4,0,4","0,0,0")
	obj.addElbow("elbow31","-4,0,0","0,0,0")
	obj.addElbow("elbow35","-4,0,-4","0,0,0")

	obj.addElbow("elbow36","-6,0,-4","90,0,0",initial_values=[["angle","-60"]])
	obj.addElbow("elbow37","-6,0,4","90,0,0",initial_values=[["angle","-60"]])
	obj.addElbow("elbow38","-6,0,0","90,0,0",initial_values=[["angle","-60"]])

	obj.addElbow("elbow22","-11,0,4","90,0,0",initial_values=[["angle","90"]])
	obj.addElbow("elbow33","-11,0,-4","90,0,0",initial_values=[["angle","90"]])
	obj.addElbow("elbow27","-11,0,0","90,0,0",initial_values=[["angle","90"]])

	obj.addElbow("elbow40","-16,0,4","90,0,0",initial_values=[["angle","60"]])
	obj.addElbow("elbow41","-16,0,-4","90,0,0",initial_values=[["angle","60"]])
	obj.addElbow("elbow42","-16,0,0","90,0,0",initial_values=[["angle","60"]])

	#horizontal elbows
	obj.addElbow("elbow30","4,0,0","360,180,0")
	obj.addElbow("elbow32","4,0,-4","360,180,0")
	obj.addElbow("elbow28","4,0,4","360,180,0")

	obj.addElbow("elbow23","6,0,-4","90,180,0",initial_values=[["angle","-60"]])
	obj.addElbow("elbow25","6,0,4","90,180,0",initial_values=[["angle","-60"]])
	obj.addElbow("elbow26","6,0,0","90,180,0",initial_values=[["angle","-60"]])

	obj.addElbow("elbow29","11,0,4","90,180,0",initial_values=[["angle","90"]])
	obj.addElbow("elbow34","11,0,0","90,180,0",initial_values=[["angle","90"]])
	obj.addElbow("elbow21","11,0,-4","90,180,0",initial_values=[["angle","90"]])

	obj.addElbow("elbow43","16,0,4","90,180,0",initial_values=[["angle","60"]])
	obj.addElbow("elbow44","16,0,-4","90,180,0",initial_values=[["angle","60"]])
	obj.addElbow("elbow45","16,0,0","90,180,0",initial_values=[["angle","60"]])

	obj.addHook("hook01","-25,0,4","0,0,270")
	obj.addHook("hook02","-25,0,-4","0,0,270")
	obj.addHook("hook03","-25,0,0","0,0,270")
	obj.addHook("hook04","25,0,4","0,0,90")
	obj.addHook("hook05","25,0,-4","0,0,90")
	obj.addHook("hook06","25,0,0","0,0,90")

	obj.extend("continuum14","5","continuum13","2")
	obj.extend("continuum13","5","continuum4","2")
	obj.extend("continuum4","5","continuum11","2")
	obj.extend("continuum11","5","continuum0","2")
	obj.extend("continuum5","5","continuum1","2")
	obj.extend("continuum1","5","continuum16","2")
	obj.extend("continuum16","5","continuum9","2")
	obj.extend("continuum9","5","continuum2","2")
	obj.extend("continuum7","5","continuum6","2")
	obj.extend("continuum6","5","continuum15","2")
	obj.extend("continuum15","5","continuum12","2")
	obj.extend("continuum12","5","continuum10","2")

	obj.extend("continuum0","0","continuum2","3")
	obj.extend("continuum2","0","continuum10","3")
	obj.extend("continuum11","0","continuum9","3")
	obj.extend("continuum9","0","continuum12","3")
	obj.extend("continuum4","0","continuum16","3")
	obj.extend("continuum16","0","continuum15","3")
	obj.extend("continuum13","0","continuum1","3")
	obj.extend("continuum1","0","continuum6","3")
	obj.extend("continuum14","0","continuum5","3")
	obj.extend("continuum5","0","continuum7","3")

	obj.extend("continuum0","3","elbow35","0")
	obj.extend("continuum4","3","elbow31","0")
	obj.extend("continuum14","3","elbow24","0")

	obj.extend("continuum10","0","elbow32","0")
	obj.extend("continuum15","0","elbow30","0")
	obj.extend("continuum7","0","elbow28","0")

	obj.extend("elbow35","1","elbow36","0")
	obj.extend("elbow31","1","elbow38","0")
	obj.extend("elbow24","1","elbow37","0")

	obj.extend("elbow32","1","elbow23","0")
	obj.extend("elbow30","1","elbow26","0")
	obj.extend("elbow28","1","elbow25","0")

	obj.extend("elbow36","1","elbow33","0")
	obj.extend("elbow38","1","elbow27","0")
	obj.extend("elbow37","1","elbow22","0")

	obj.extend("elbow23","1","elbow21","0")
	obj.extend("elbow26","1","elbow34","0")
	obj.extend("elbow25","1","elbow29","0")

	obj.extend("elbow22","1","elbow40","0")
	obj.extend("elbow33","1","elbow41","0")
	obj.extend("elbow27","1","elbow42","0")

	obj.extend("elbow29","1","elbow43","0")
	obj.extend("elbow21","1","elbow44","0")
	obj.extend("elbow34","1","elbow45","0")

	obj.extend("elbow40","1","continuum18","3")
	obj.extend("elbow41","1","continuum19","3")
	obj.extend("elbow42","1","continuum20","3")

	obj.extend("elbow43","1","continuum8","0")
	obj.extend("elbow44","1","continuum17","0")
	obj.extend("elbow45","1","continuum3","0")

	obj.extend("continuum18","0","hook01","0")
	obj.extend("continuum19","0","hook02","0")
	obj.extend("continuum20","0","hook03","0")

	obj.extend("continuum8","3","hook04","0")
	obj.extend("continuum17","3","hook05","0")
	obj.extend("continuum3","3","hook06","0")

	obj.addOscillator(name = "right",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "1",
		amplitude_range = "0,10,1",
		amplitude_value = "10",
		offset_range= "-1500,1500,1",
		offset_value = "0",
		phase_value = "0",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "left",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "1",
		amplitude_range = "0,10,1",
		amplitude_value = "10",
		offset_range= "-1500,1500,1",
		offset_value = "0",
		phase_value = "0.5",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "leftup",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "1",
		amplitude_range = "0,10,1",
		amplitude_value = "-10",
		offset_range= "-1500,1500,1",
		offset_value = "0",
		phase_value = "0",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "rightup",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "1",
		amplitude_range = "0,10,1",
		amplitude_value = "10",
		offset_range= "-1500,1500,1",
		offset_value = "10",
		phase_value = "0.5",
		phase_range = "0,10,0.1")


	#obj.consume("elbow24","angle","right","sample")
	#obj.consume("elbow31","angle","left","sample")
	#obj.consume("elbow35","angle","right","sample")
#
	#obj.consume("elbow32","angle","left","sample")
	#obj.consume("elbow30","angle","right","sample")
	#obj.consume("elbow28","angle","left","sample")
#
	#obj.consume("elbow36","angle","leftup","sample")
	#obj.consume("elbow37","angle","righttup","sample")
	#obj.consume("elbow38","angle","leftup","sample")
#
	#obj.consume("elbow23","angle","rightup","sample")
	#obj.consume("elbow25","angle","leftup","sample")
	#obj.consume("elbow26","angle","rightup","sample")

