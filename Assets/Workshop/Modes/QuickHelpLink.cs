using UnityEngine;
using System.Collections;

public class QuickHelpLink : MonoBehaviour {

    bool opened = false;

    void OnMouseEnter () {
        opened = false;
        transform.localScale = Vector3.one * 1.01f;
    }

    void OnMouseExit () {
        transform.localScale = Vector3.one * 1.00f;
    }

    void OnMouseDown () {
        transform.localScale = Vector3.one * 0.98f;
    }

    void OnMouseUp () {
        if ( !opened ) {
            opened = true;
            transform.localScale = Vector3.one * 1f;
            Application.OpenURL( "http://www.rawbots.net/page/quick_reference" );
        }
    }

}
