using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ExtendPartsMode : MonoBehaviour {

// increased the length of voltaic arcs at around line 286 -z26

    private BulletLayers partMask;
    public GameCamera gameCamera;
    private Part firstPart;
    private Part secondPart;
    Builder.SlotsMarkerWrapper firstPartSlots = new Builder.SlotsMarkerWrapper();
    Builder.SlotsMarkerWrapper secondPartSlots = new Builder.SlotsMarkerWrapper();
    public GameObject soundFailedAsset;
    public GameObject soundSuccessAsset;
    private Part posiblePartExtendsMode;

    public enum BuilderState {
        None,
        OnePart,
        TwoParts,
        Connecting
    }


    BuilderState currentState = BuilderState.None;

    void Awake () {
        var mode = new GameModeManager.Mode(){
            id = GameModeManager.ModeId.PartExtendConnect,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Alpha2, state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }};
        GameModeManager.RegisterMode( mode );
    }

    // Use this for initialization
    void Start () {
        partMask =BulletLayers.Part;
    }

    // Update is called once per frame
    void Update () {
        ExtendsPartsMachine();
    }

    void ExtendsPartsMachine () {

        var builderActive = GameModeManager.InMode( m => m == GameModeManager.ModeId.PartExtendConnect );
        BulletRaycastHitDetails partHit;
        var ray = gameCamera.GetComponent<Camera>().ScreenPointToRay( Input.mousePosition );
        var clickUp = Input.GetMouseButtonDown( 0 );
        var click = Input.GetMouseButton( 0 );


        var partHitStatus = Bullet.Raycast( ray, out partHit, 1500,partMask, partMask );

        switch ( currentState ) {
            case BuilderState.None:
                if ( builderActive ) {


                    if ( partHitStatus ) {
                        HighlightElegiblePartSecondMode( partHit, Highlight.HighlightType.Normal );
                    }
                    else {
                        DisableHighlightToElegiblePartSecondMode();
                    }
                    if ( partHitStatus && clickUp ) {
                        DisableHighlightToElegiblePartSecondMode();
                        var validPart = SelectFirstPart( partHit );
                        if ( validPart ) {
                            Builder.SetHighLightToPartAndSubpart( firstPart.gameObject, true, Highlight.HighlightType.Blue );
                            Builder.instance.SwitchSlot( 0, firstPartSlots );

                            Sec.ForPermission( firstPart.proxy.entity, Sec.Permission.connect, () => {
                                currentState = BuilderState.OnePart;
                            }, () => {
                                Console.PushError("", new string[]{  "access denied - check permissions" } );
                                firstPart.isProtected = false;
                                currentState = BuilderState.None;
                                Builder.SetHighLightToPartAndSubpart( firstPart.gameObject, false );
                                firstPart.proxy.RemoveUnUsedSlots();
                                Builder.CleanSlotsMarker( firstPartSlots.slots );
                            } );
                        }
                    }
                }
                else {
                    DisableHighlightToElegiblePartSecondMode();
                }

                break;
            case BuilderState.OnePart:
                if ( builderActive ) {


                    if ( partHitStatus ) {
                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if(subPart != null)part = subPart.part;

                        //var part = Part.FindOwner( partHit.collider.gameObject.transform, partMask );
                        if ( part != firstPart ) {
                            HighlightElegiblePartSecondMode( partHit, Highlight.HighlightType.Yellow );
                        }
                    }
                    else {
                        DisableHighlightToElegiblePartSecondMode();
                    }

                    if ( partHitStatus ) {
                        SwitchSlotFirstPart( partHit );
                    }
                    if ( click && partHitStatus && clickUp ) {

                        var validPart = SelectSecondPart( partHit, true );
                        if ( validPart ) {
                            DisableHighlightToElegiblePartSecondMode();
                        
                            Sec.ForPermission( secondPart.proxy.entity, Sec.Permission.connect, () => {
                                Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, true, Highlight.HighlightType.Green );
                                Builder.instance.SwitchSlot( 0, secondPartSlots );
                                currentState = BuilderState.TwoParts;
                            }, () => {
                                Console.PushError("", new string[]{  "access denied - check permissions" } );
                                currentState = BuilderState.None;
                                firstPart.isProtected = false;
                                secondPart.isProtected = false;
                                Builder.SetHighLightToPartAndSubpart( firstPart.gameObject, false );
                                Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, false );

                                firstPart.proxy.RemoveUnUsedSlots();
                                secondPart.proxy.RemoveUnUsedSlots();
                                Builder.CleanSlotsMarker( firstPartSlots.slots );
                                Builder.CleanSlotsMarker( secondPartSlots.slots );

                            } );
                        }
                    }
                }
                else {
                    currentState = BuilderState.None;
                    firstPart.isProtected = false;
                    Builder.SetHighLightToPartAndSubpart( firstPart.gameObject, false );
                    firstPart.proxy.RemoveUnUsedSlots();
                    Builder.CleanSlotsMarker( firstPartSlots.slots );
                    Builder.CleanSlotsMarker( secondPartSlots.slots );
                }
                break;
            case BuilderState.TwoParts:
                if ( builderActive ) {



                    if ( partHitStatus ) {

                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if(subPart != null)part = subPart.part;

                        if ( part == secondPart ) {
                            SwitchSlotSecondPart( partHit );
                            if ( click && clickUp ) {
                                Builder.SetHighLightToPartAndSubpart( firstPart.gameObject, false );
                                Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, false );


                                Builder.CleanSlotsMarker( firstPartSlots.slots );
                                Builder.CleanSlotsMarker( secondPartSlots.slots );

                                ConnectSlots();
                                secondPart.isProtected = false;
                                firstPart.isProtected = false;
                                currentState = BuilderState.None;
                            }

                        }
                        else if ( part == firstPart ) {
                            SwitchSlotFirstPart( partHit );
                        }
                        else if ( click && clickUp ) {
                            if ( secondPart != default(Part) ) {
                                secondPart.isProtected = false;
                                Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, false );

                                Builder.CleanSlotsMarker( secondPartSlots.slots );
                                secondPart.proxy.RemoveUnUsedSlots();

                            }
                            var validPart = SelectSecondPart( partHit, true );
                            if ( validPart ) {
                            
                                Sec.ForPermission( secondPart.proxy.entity, Sec.Permission.connect, () => {
                                    Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, true, Highlight.HighlightType.Green );
                                    Builder.instance.SwitchSlot( 0, secondPartSlots );
                                }, () => {
                                    Console.PushError("", new string[]{  "access denied - check permissions" } );
                                    secondPart.isProtected = false;
                                    Builder.SetHighLightToPartAndSubpart( firstPart.gameObject, false );
                                    Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, false );


                                    firstPart.proxy.RemoveUnUsedSlots();
                                    secondPart.proxy.RemoveUnUsedSlots();
                                    Builder.CleanSlotsMarker( firstPartSlots.slots );
                                    Builder.CleanSlotsMarker( secondPartSlots.slots );
                                } );
                            }
                            else {
                                currentState = BuilderState.OnePart;
                            }

                        }
                    }
                }
                else {
                    currentState = BuilderState.None;

                    Builder.SetHighLightToPartAndSubpart( firstPart.gameObject, false );
                    Builder.SetHighLightToPartAndSubpart( secondPart.gameObject, false );
                    secondPart.isProtected = false;
                    firstPart.isProtected = false;
                    firstPart.proxy.RemoveUnUsedSlots();
                    secondPart.proxy.RemoveUnUsedSlots();
                    Builder.CleanSlotsMarker( firstPartSlots.slots );
                    Builder.CleanSlotsMarker( secondPartSlots.slots );

                }
                break;
            case BuilderState.Connecting:
                if ( builderActive ) {

                }
                else {

                    currentState = BuilderState.None;

                    firstPart.proxy.RemoveUnUsedSlots();
                    secondPart.proxy.RemoveUnUsedSlots();
                }
                break;
            default:
                break;
        }
    }

    public bool SelectFirstPart ( BulletRaycastHitDetails partHit ) {
        var part = partHit.bulletRigidbody.GetComponent<Part>();
        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
        if(subPart != null)part = subPart.part;
        if ( part == default(Part) ) {
            return false;
        }
        if ( part.GetComponent<PhysicalObject>() == null ) {
            return false;
        }
        firstPart = part;
        firstPart.proxy.SetSlots();
        firstPartSlots.slotIndex = 0;
        firstPartSlots.slots = firstPart.proxy.GetAvailableSlots().Select( p => p.nodeView ).ToList();

        if ( firstPartSlots.slots.Count > 0 ) {
//            //ghostPiece = firstPart.proxy.CreateGhostPiece();
//            CreateGhostTree( firstPart.nodeView );
//            var ghostRigidBody = ghostPiece.AddComponent<Rigidbody>();
//            ghostRigidBody.isKinematic = true;
//
//            ghostPiece.SetActiveRecursively( false );
//            angle = 0;
            firstPart.isProtected = true;
            return true;
        }
        else {
            return false;
        }
    }

    private void ConnectSlots () {

        var secondSlotView = secondPartSlots.slots[ secondPartSlots.slotIndex ].proxy.nodeView;
        var firstSlotView = firstPartSlots.slots[ firstPartSlots.slotIndex ].proxy.nodeView;

//right below is the setting that checks the maximum length for a voltaic arc -z26 vanilla = 10
        if ( Vector3.Distance( firstSlotView.transform.position, secondSlotView.transform.position ) > 16 ) {

            currentState = BuilderState.None;
            secondPart.proxy.RemoveUnUsedSlots();
            firstPart.proxy.RemoveUnUsedSlots();
            Instantiate( soundFailedAsset, firstPart.transform.position, firstPart.transform.rotation );
        }
        else {


            var localToEntityFirstPart = firstPart.proxy.entity.Heads( p => p == "local_to" ).FirstOrDefault();
            var localToEntitySecondPart = secondPart.proxy.entity.Heads( p => p == "local_to" ).FirstOrDefault();
            if ( localToEntityFirstPart != localToEntitySecondPart ) {

                secondPart.proxy.RecalculateClusters();
                secondPart.proxy.SetAsGroupPositionReference();
            }

            var secondSlotEntity = secondPartSlots.slots[ secondPartSlots.slotIndex ].proxy.entity;
            var firstSlotEntity = firstPartSlots.slots[ firstPartSlots.slotIndex ].proxy.entity;
            Builder.SetPartEntityData( secondSlotEntity, firstSlotEntity );

            new Oracle.Relationship( firstSlotEntity, "extends", secondSlotEntity );
            firstPart.proxy.CreateVoltaicArcsAndJoints();
            secondPart.proxy.RemoveUnUsedSlots();
            firstPart.proxy.RemoveUnUsedSlots();

            currentState = BuilderState.None;
            Instantiate( soundSuccessAsset, firstPart.transform.position, firstPart.transform.rotation );
        }
    }

    public void HighlightElegiblePartSecondMode ( BulletRaycastHitDetails partHit, Highlight.HighlightType type ) {
        var part = partHit.bulletRigidbody.GetComponent<Part>();
        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
        if(subPart != null)part = subPart.part;
        if ( part.GetComponent<PhysicalObject>() == default( PhysicalObject ) ) {
            DisableHighlightToElegiblePartSecondMode();
            return;
        }

        if ( part == posiblePartExtendsMode ) {
            return;
        }

        if ( posiblePartExtendsMode != null ) {
            Builder.SetHighLightToPartAndSubpart( posiblePartExtendsMode.gameObject, false );
        }
        Builder.SetHighLightToPartAndSubpart( part.gameObject, true, type );
        posiblePartExtendsMode = part;
    }

    public void DisableHighlightToElegiblePartSecondMode () {
        if ( posiblePartExtendsMode != null ) {
            Builder.SetHighLightToPartAndSubpart( posiblePartExtendsMode.gameObject, false );
            posiblePartExtendsMode = default(Part);
        }
    }

    public void SwitchSlotFirstPart ( BulletRaycastHitDetails partHit ) {

        var part = partHit.bulletRigidbody.GetComponent<Part>();
        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
        if(subPart != null)part = subPart.part;
        if ( part != firstPart ) {
            return;
        }

        var leftClick = Input.GetMouseButtonUp( 1 );

        if ( leftClick ) {
            Builder.instance.SwitchSlot( 1, firstPartSlots );
        }

    }

    public bool SelectSecondPart ( BulletRaycastHitDetails partHit, bool allowSameBot = false ) {
        var part = partHit.bulletRigidbody.GetComponent<Part>();
        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
        if(subPart != null)part = subPart.part;
        var master = firstPart.proxy.entity.Heads( p => p == "local_to" ).FirstOrDefault();
        var secondMaster = part.proxy.entity.Heads( p => p == "local_to" ).FirstOrDefault();
        if ( part == firstPart || ( master == secondMaster && !allowSameBot ) ) {
            return false;
        }
        part.proxy.SetSlots();
        var otherPartSlots = part.proxy.GetAvailableSlots();
        secondPartSlots.slotIndex = 0;
        secondPartSlots.slots = otherPartSlots.Select( p => p.nodeView ).ToList();
        if ( secondPartSlots.slots.Count > 0 ) {
            secondPart = part;
            secondPart.isProtected = true;
            return true;
        }
        else {
            return false;
        }
    }

    public void SwitchSlotSecondPart ( BulletRaycastHitDetails partHit ) {
        var part = partHit.bulletRigidbody.GetComponent<Part>();
        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
        if(subPart != null)part = subPart.part;
        if ( part != secondPart ) {
            return;
        }

        var leftClick = Input.GetMouseButtonUp( 1 );

        if ( leftClick ) {
            Builder.instance.SwitchSlot( 1, secondPartSlots );
        }
    }




}
