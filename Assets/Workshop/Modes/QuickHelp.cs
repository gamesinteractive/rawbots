using UnityEngine;
using System.Collections;

public class QuickHelp : MonoBehaviour {

    public GameObject card;
    public GameObject link;
	bool on = false;

    void Start () {
        card.SetActive(false);
        link.SetActive(false);
    }

	void Update () {
	    if ( Input.GetKey( KeyCode.F1 ) ) {
            if ( !on ) {
                on = true;
                card.SetActive(true);
                link.SetActive(true);
            }
        }
        else {
            if ( on ) {
                on = false;
                card.SetActive(false);
                link.SetActive(false);
            }
        }
	}


}
