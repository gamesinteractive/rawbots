using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//SUPER HAX DELETE IF NOT IN USE

public class InGameActivate : MonoBehaviour {

    public List<Bot> bots;
    float t;

    void Start () {
        t = Time.time;
    }

    // Update is called once per frame
    void Update () {
        if ( Input.GetKeyDown( KeyCode.P ) && bots.Count > 0 && Time.time - t > 2 ) {
            var next = bots[ 0 ];
            next.enabled = true;
            bots.Remove( next );
            t = Time.time;
        }
    }
}
