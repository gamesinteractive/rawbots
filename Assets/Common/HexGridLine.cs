using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class HexGridLine : MonoBehaviour {

    public HexGrid grid;
    public Transform head;
    public Transform tail;
    public LineRenderer line;
    public bool headInWorld = false;
    float marchingSpeed = 0.5f;

    void Update () {
        if ( head == null || tail == null ) {
            Destroy( gameObject );
        }
        else {
            var headPosition = head.position;
            var depth = new Vector3( 0, 0, 0 );
            if ( headInWorld ) {
                depth = Vector3.zero;
                var headScreen = grid.camera.gameCamera.GetComponent<Camera>().WorldToScreenPoint( headPosition );
                headScreen.z = grid.camera.transform.InverseTransformPoint( tail.position ).z;
                headPosition = grid.camera.GetComponent<Camera>().ScreenToWorldPoint( headScreen );
            }
            var to = ( headPosition - tail.position );
            var dir = to.normalized;
            var dist = to.magnitude;
            transform.position = to / 2;
            line.SetVertexCount( 2 );
            if ( dist < 3f ) {
                line.SetPosition( 0, headPosition - dir * 0.5f + depth );
                line.SetPosition( 1, tail.position + dir * 0.5f + depth );
            }
            else {
                line.SetPosition( 0, headPosition - dir * 1 + depth );
                line.SetPosition( 1, tail.position + dir * 1 + depth );
            }
            var offset = line.material.mainTextureOffset;
            offset.x -= Time.deltaTime * marchingSpeed;
            line.material.mainTextureScale = new Vector2( to.magnitude / 2, 1 );
            line.material.mainTextureOffset = offset;
        }
    }
}
