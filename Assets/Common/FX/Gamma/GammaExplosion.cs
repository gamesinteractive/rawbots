using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;

public class GammaExplosion : MonoBehaviour {

    public Light[] lights;
    public AudioSource implosion;
    public AudioSource explosion;
    public GameObject sparksFX;
    public btRigidBody btRB;
    float attractionForce;
    float repulsionForce;
    float maxDetectionRange = 12;
    float maxDamageRange = 1000;

    IEnumerator Start () {

        attractionForce = 300 * (1 / Bullet.instance.worldScale);
        repulsionForce = 800 * (1 / Bullet.instance.worldScale);
        Destroy( gameObject, 4 );
        StartCoroutine( SearchNDestroy() );
        yield return new WaitForSeconds( 0.5f );
        var sparks = Instantiate( sparksFX, transform.position, transform.rotation ) as GameObject;
        Destroy( sparks, 5 );
        explosion.Play();
        foreach ( var light in lights ) {
            light.enabled = true;
        }
        yield return new WaitForSeconds( 0.5f );
        foreach ( var light in lights ) {
            light.enabled = false;
        }
    }

    IEnumerator SearchNDestroy () {
        implosion.Play();
        var bodies = Bullet.OverlapSphere(btRB, BulletLayers.Part,BulletLayers.Part,transform.position.ToBulletVector3(),maxDetectionRange);
        bodies.ForEach( r => {
            var rPos = transform.position - r.transform.position;
            if ( rPos.magnitude < maxDetectionRange ) {
                var force = ( rPos ).normalized * attractionForce;
                r.AddForce( force, Vector3.zero );
//                Debug.Log(" Atracting " + r.name + " " + force.magnitude);
            }
        } );
        yield return new WaitForSeconds(0.25f);
        bodies.ForEach( r => {
            if(r == null) {return;} //In case bodies were destroyed during WaitForSeconds()
            var rPos = transform.position - r.transform.position;
            var repulsion = Util.MapValue0X( rPos.magnitude, maxDamageRange, repulsionForce , repulsionForce * 100 );
            var force = rPos.normalized * repulsion;
//            Debug.Log(" Repulsing " + r.name + " " + force.magnitude);
            r.AddForce( -force, Vector3.zero );
        } );

        bodies.ForEach( r => {
            if(r == null) {return;} //In case bodies were destroyed during WaitForSeconds()
            var rPos = transform.position - r.transform.position;
            var repulsion = Util.MapValue0X( rPos.magnitude, maxDamageRange, repulsionForce, repulsionForce * 100 );
            var force = rPos.normalized * repulsion;
            r.AddForce( -force, Vector3.zero );
//            Debug.Log(" Damaging " + r.name + " " + force.magnitude);
            r.SendMessage( "OnHit", force.magnitude * 2.1f, SendMessageOptions.DontRequireReceiver );
        } );
        yield return 0;

    }

}
