using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class HexActionTile : HexGridTileTypeHandler {

    float value = 0;
    Property property;
    string[] labels = new string[2]{string.Empty,string.Empty};
    Texture2D auxIcon;

    public override void Init (Entity etype) {
        tile.actionTarget.entity.ForProperty<float>( tile.entity.id, v => value = v );
        property = tile.actionTarget.entity.properties.Where( p => p.Name == tile.entity.id ).FirstOrDefault();
        switch ( tile.entity.id ) {
            case "pin":
                labels[ 0 ] = "pin";
                labels[ 1 ] = "unpin";
                if ( property == default(Property) ) {
                    tile.actionTarget.entity.SetProperty("pin",0);
                    property = tile.actionTarget.entity
                        .properties.Where( p => p.Name == tile.entity.id )
                        .FirstOrDefault();
                    tile.label.text = "pin";
                }
                break;
        }
        onConsole = () => {
            tile.ForGrid(grid => {
                tile.actionTarget.entity.ForProperty<float>( tile.entity.id, v => value = v );
                value = value == 0 ? 1 : 0;
                tile.actionTarget.entity.SetProperty( tile.entity.id, value );
                var layer = value == 0 ? "grid" : "pinned";
                tile.actionTarget.SetLayer(layer);
                UpdateLabel();
            });
        };
        auxIcon = LoadIcon(labels[1]);
        SetMaterial();
        UpdateLabel();
    }

    void UpdateLabel(){
        tile.label.text = labels[(int)value];
        tile.iconRenderer.material.SetTexture( "_MainTex", value == 0 ? icon : auxIcon );
    }

    public override void SetMaterial( ) {
        tile.renderer.sharedMaterial = tile.grid.actionMaterial;
    }

    public override void RequestContextualMenu () {
        onConsole();
    }
    
    public override void DeleteStep (bool step) {
        return;
    }
	
    public override void RequestOnConsole () {
        return;
    }
    
}
