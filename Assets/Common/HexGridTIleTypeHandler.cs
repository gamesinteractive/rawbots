using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class HexGridTileTypeHandler : MonoBehaviour {
    public Entity etype = default(Entity);
    public Inbox inbox;
    public Outbox outbox;
    public IOComponent io;
    public List< string > options = new List< string >();
    public System.Action onConsole = () => {};
    public HexGridTile tile;
    public HexGridTile consumeOfTile;
    public HexGridLine consumeOfLine;
    public Vector3 touchOffset;
    public bool mouseMoved;
    public Texture2D icon;
    protected bool iconLoaded = false;

    public string description = "";

    public virtual void Init ( Entity etype ) {
        this.etype = etype;
    }

    public virtual void SetMaterial () {
    }

    public virtual void OnDestroyEventHandler () {
    }

    public virtual void RequestOnConsole () {
    }

    public virtual void RequestContextualMenu () {
        tile.ForGrid( grid => {
            grid.GenerateTilesActions( tile );
        } );
    }

    public virtual void OnRightClick () {
    }

    void OnDestroy () {
        OnDestroyEventHandler();
    }

    public void InitializeIO () {
    //This is called only when the created tile is an operant tile.
    //(Either by spawning one manually or by loading a program in the vp grid)
    //After all, an operand can only have a few unique tiles under it (no duplicates allowed) so it would make sense
    //to prepare all i/o tiles the moment the operand tile is displayed. -z26

        var types = new List< Entity >();//Fill the "options" list with the names of the inputs and outputs of the operand -z26
        tile.entity.ForHeads( Util.AsIs, head => types.Add( head ) );
        foreach ( var e in types.SelectMany( e => e.Tails( p => p == "input_for" ) )
                 .Where( e => !e.Heads( p => p == "hidden_from" ).Any() ) ) {
            e.ForProperty< string >( "property", property => options.Add( ">" + property ) );
        }
        foreach ( var e in types.SelectMany( e => e.Tails( p => p == "output_for" ) )
                 .Where( e => !e.Heads( p => p == "hidden_from" ).Any() ) ) {
            e.ForProperty< string >( "property", property => options.Add( "<" + property ) );
        }
        onConsole = () => {
            tile.grid.console.ForInput( "add", tile.value.text, options, input => {
                var availableGrid = FindObjectOfType( typeof( HexGrid ) ) as HexGrid;
                if ( availableGrid != default( HexGrid ) ) {
                    var newTiling = availableGrid.FindAvailableSpaceAround( tile.tiling );
                    if ( input.Any( c => c == '>' ) ) { //Commenting out this whole if statement doesn't break much, but does allow
                        var property = input.Split( '>' )[ 1 ]; //spawning of duplicate inputs/outputs in some circumstances -z26
                        availableGrid.CreateInput( types.SelectMany( e => e.Tails( p => p == "input_for" ) )
                          .Where( t => ( t.GetProperty< string >( "property" ) as string ) == property )
                          .First(), tile.entity, newTiling, tile.tiling );
                    } else if ( input.Any( c => c == '<' ) ) {
                        var property = input.Split( '<' )[ 1 ];
                        availableGrid.CreateOutput( types.SelectMany( e => e.Tails( p => p == "output_for" ) )
                           .Where( t => ( t.GetProperty< string >( "property" ) as string ) == property )
                           .First(), tile.entity, newTiling, tile.tiling );
                    }
                }
            } );
        };
    }

    List< HexGridLine > allConsumes = new List< HexGridLine >();

    public void Consumes () {
        foreach ( var c in allConsumes ) {
            if ( c != null ) {
                Destroy( c.gameObject );
            }
        }
        allConsumes.Clear();
        var consumes = tile.entity.Heads( p => p == "consumes" );
        foreach ( var consume in consumes ) {
            consumeOfTile = tile.grid.tiles[ consume ];
            consumeOfLine = ( Instantiate( tile.consumeLineAsset ) as GameObject ).GetComponent< HexGridLine >();
            consumeOfLine.grid = tile.grid;
            consumeOfLine.transform.parent = transform;
            consumeOfLine.tail = this.transform;
            consumeOfLine.head = consumeOfTile.transform;
            allConsumes.Add( consumeOfLine );
        }
    }

    public virtual void DeleteStep ( bool step ) {
        if ( tile.entity == null ) {
            return;
        } else {
            tile.DestroyTile();
        }
    }

    public void DestroyIOs () {
        if ( io != null ) {
            var ios = tile.entity.Tails( p => p == "input_for" || p == "output_for" ).ToList();
            foreach ( var s in ios ) {
                var otherTile = tile.grid.tiles[ s ];
                otherTile.handler.DeleteStep( false );
            }
            tile.DestroyTile();
        }
    }

    public HexGridTile GetTileAtTouchPosition ( TouchData touch ) {
        var targetTiling = tile.grid.ToTiling( transform.parent
          .InverseTransformPoint( Util.TouchToWorld( transform.position, touch ) - touchOffset ) );
        return tile.grid.FindTileAt( targetTiling );
    }

    public void ConnectIOs ( HexGridTile input, HexGridTile output ) {
        Sec.ForPermission( input.entity, Sec.Permission.connect, () => {
            Sec.ForPermission( output.entity, Sec.Permission.connect, () => {
                if ( input != default( HexGridTile ) && output != default( HexGridTile ) ) {
                    if ( tile.grid.ioValidations[ output.handler.etype.id ].Any( v => v == input.handler.etype.id ) ) {
                        new Relationship( input.entity, "consumes", output.entity );
                        output.entity.proxy.GetComponent< Outbox >().inboxes
                            .Add( input.entity.proxy.GetComponent< Inbox >() );
                        input.handler.Consumes();
                    } else {
                        Console.PushError("connect", new string[]{  "Incompatible IO" } );
                    }
                }
            }, () => {
                Console.PushError("connect", new string[]{  "access denied - check permissions" } );
            } );
        }, () => {
            Console.PushError("connect", new string[]{  "access denied - check permissions" } );
        } );
    }

    public void EditPermissions () {
        if ( tile.tileType != HexGridTileType.Empty ) {
            tile.grid.console.ForPasscode( tile.entity, () => {
                List<string> perms = new List<string>();
                foreach ( var p in Sec.PermissionList() ) {
                    perms.Add( "+" + p );
                    perms.Add( "-" + p );
                }
                tile.grid.console.ForInput( "permission", "", perms, input => {
                    uint ePerms = 0;
                    tile.entity.ForProperty<float>( "permissions", value => ePerms = ( uint )value );
                    var parts = input.Split( ' ' );
                    if ( parts.Length == 1 ) {
                        ePerms = Sec.SetPermission( ePerms, parts[ 0 ] );
                        tile.entity.SetProperty( "permissions", ( float )ePerms );
                    }
                } );
            }, () => {
                Console.PushError("tile", new string[]{ "access denied - invalid password" } );
            } );
        }
    }

    protected Color ApplyAlphaFactor ( Renderer renderer, float factor ) {
        var color = renderer.material.GetColor( "_TintColor" );
        color.a += color.a * factor;
        renderer.material.SetColor( "_TintColor", color );
        return color;
    }

    public virtual void OnMouseEnterCustom ( TouchData touch) {
        //ApplyAlphaFactor( tile.renderer, 1.3f ); this line causes a strange bug where tiles become transparent
        //roughly 1 second after you finished selecting them. -z26
        if ( iconLoaded ) {
            ApplyAlphaFactor( tile.iconRenderer, -0.8f );
            tile.label.characterSize = 1.2f;
            tile.label.GetComponent<Renderer>().material.color = tile.selectedLabelColor;
            tile.label.GetComponent<Renderer>().enabled = true;
        }
    }


    //Gets called by GridCamera.cs, but unlike OnMouseEnterCustom isn't called every frame a tile is selected.
    public void hoverDescription() {
        // if (tile.grid.state == HexGrid.GridState.Dragging) { doesnt work cause the way tiledescript works.
        //     return;
        // }

         if (tile.entity != null) {
        tile.entity.ForInferredProperty< string >( "description", Util.AsIs, d => description = d );
        }

        //show comment instead of description on hover.
        if (this is HexInputTile) {
            tile.entity.ForInferredProperty< string >( "property", Util.AsIs, p => {
                if (p == "comment") {
                    tile.entity.ForInferredProperty< string >( "value", Util.AsIs, v => description = v );
                }
            });
        }

        if(description != "") {
                tile.grid.camera.tileDescript.showDescript(tile,description);
        }
    }


    public virtual void OnMouseExitCustom ( TouchData touch ) {
        SetMaterial();
        if ( iconLoaded ) {
            tile.iconRenderer.material.SetColor( "_TintColor", tile.originalIconColor );
            tile.label.GetComponent<Renderer>().enabled = false;
            tile.label.characterSize = 1.0f;
            tile.label.GetComponent<Renderer>().material.color = tile.originalLabelColor;
        }

        tile.grid.camera.tileDescript.showDescript(null,""); //z26
    }

    public virtual void OnMouseMoveCustom ( TouchData touch ) {
    }

    public virtual void OnMouseDownCustom ( TouchData touch ) {
        transform.localScale *= 0.95f;
        mouseMoved = false;
        if ( touch.touchId == 1 ) {
            touchOffset = Util.TouchToWorld( transform.position, touch ) - transform.position;
        }
    }

    public virtual void OnMouseUpCustom ( TouchData touch ) {
        transform.localScale = tile.localScale;
        if ( touch.touchId == 1 ) {
            var leftControl = Input.GetKey( KeyCode.LeftControl );
            var leftAlt = Input.GetKey( KeyCode.LeftAlt );
            var leftShift = Input.GetKey( KeyCode.LeftShift );
            var rightShift = Input.GetKey( KeyCode.RightShift );
            if ( touch.dragging ) {
                if ( tile.entity != null ) {
                    Destroy( tile.ghostTile );
                    if ( leftControl ) {
                        tile.targetPosition = tile.grid.FromTiling( tile.tiling );
                    } else {
                        tile.targetPosition = transform.parent
                            .InverseTransformPoint( Util.TouchToWorld( transform.position, touch ) - touchOffset );
                        tile.tiling = tile.grid.ToTiling( tile.targetPosition );
                        tile.targetPosition = tile.grid.FromTiling( tile.tiling );
                        tile.entity.SetProperty( "tiling", Util.Vector3ToProperty( tile.tiling ) );
                    }
                }
            } else {
                if ( leftControl ) {
                    if ( leftShift ) {
                        OnMouseExitCustom( touch );
                        DeleteStep(true);//this deletes tiles when clicking on them. The boolean says whenever
                        //to only cut the input->output connections first click, or to delete everything at once. -z26
                    } else {
                        RequestOnConsole();
                    }
                } else {
                    if ( rightShift && !leftShift && !leftAlt ) {
                        RequestContextualMenu();
                    } else if ( leftAlt && !leftShift ) {
                        EditPermissions();
                    } else if ( leftAlt && leftShift ) {
                        if ( tile.entity != default(Entity) ) {
                            tile.grid.console.ForPasscode( tile.entity, () => tile.grid.console.SetNewPasscode( tile.entity ), () => {} );
                        }
                    } else {
                        RequestOnConsole();
                    }
                }
            }
        } else if ( touch.touchId == 2 && !mouseMoved ) {
            OnRightClick();
        }
    }

    public virtual void OnMouseDragCustom ( TouchData touch ) {
        mouseMoved = true;
        if ( touch.touchId == 1 ) {
            if ( tile.ghostTile == null ) {
                tile.ghostTile = Instantiate( tile.ghostTileAsset ) as GameObject;
                tile.ghostTile.transform.parent = transform.parent;
            }
            tile.targetPosition = transform.parent
                .InverseTransformPoint( Util.TouchToWorld( transform.position, touch ) - touchOffset );
            var ghostTiling = tile.grid.ToTiling( tile.targetPosition );
            tile.ghostTile.transform.localPosition = tile.grid.FromTiling( ghostTiling );
            tile.ghostTile.transform.localRotation = transform.localRotation * Quaternion.Euler( 0, 0, 90 );
       }
    }

    //Mouse button held over tile.
    public virtual void OnMouseStayCustom ( TouchData touch ) {
    }

    public Texture2D LoadIcon ( string name ) {
        if ( name == string.Empty ) {
            return null;
        }
        var texture = Resources.Load( "TileIcons/" + name ) as Texture2D;
        if ( texture != null ) {
            tile.iconRenderer.material.SetTexture( "_MainTex", texture );
            tile.iconRenderer.enabled = true;
            tile.label.GetComponent<Renderer>().enabled = false;
            iconLoaded = true;
            return texture;
        }
        return null;
    }
}
