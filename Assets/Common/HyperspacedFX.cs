using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;
using Oracle;

public class HyperspacedFX : MonoBehaviour {

    public Material ghostMaterial;
    List< Renderer > allRenderers = new List< Renderer >();

    public static void Create ( PartProxy proxy ) {
        var fx = ( Instantiate( Resources.Load( "FX/HyperspacedFX", typeof( GameObject ) ) ) as GameObject )
            .GetComponent< HyperspacedFX >();
        var parts = proxy.Parts()
            .Where( p => p.nodeView != null )
            .Select( p => p.nodeView.GetComponent< Part >() )
            .Where( p => p != null ).ToList();
        var ghosts = parts.Select( p => new GameObject() ).ToList();
        for ( var i = 0; i < parts.Count; ++i ) {
            ghosts[ i ].transform.parent = fx.transform;
            Util.ClonePiece( ghosts[ i ].gameObject, parts[ i ].gameObject, LayerMask.NameToLayer( "sensor" ) );
            var renderers = ghosts[ i ].GetComponentsInChildren< Renderer >();
            foreach ( var renderer in renderers ) {
                renderer.material = fx.ghostMaterial;
                fx.allRenderers.Add( renderer );
            }
        }
    }

	IEnumerator Start () {

        var wave = new Waveform();
        wave.amplitude = 0.5f;
        wave.frequency = 10;
        wave.offset = 0.5f;
        var color = ghostMaterial.GetColor( "_TintColor" );
        var time = 0f;

        while ( time < 1 ) {
            for ( var i = 0; i < allRenderers.Count; ++i ) {
                color.a = wave.Sample().sample;
                allRenderers[ i ].material.SetColor( "_TintColor", color );
            }
            wave.amplitude -= Time.deltaTime;
            wave.offset -= Time.deltaTime;
            wave.frequency -= Time.deltaTime / 5;
            time += Time.deltaTime;
            yield return 0;
        }

        Destroy( gameObject );
	}

}
