using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class HexElementTile : HexGridTileTypeHandler {

    static List<HexElementTile> elementTiles = new List<HexElementTile>();

    public override void Init (Entity etype) {
        if ( tile.tileType == HexGridTileType.HexElement ) {
            elementTiles.Add(this);
            onConsole = () => {
                tile.ForGrid(grid => {
                    Builder.activeElement = (HexElement)System.Enum.Parse(typeof(HexElement),tile.label.text);
                    for(int i = 0 ; i < elementTiles.Count ; ++i){
                        elementTiles[i].SetMaterial();
                    }
                });
            };
        }
        SetMaterial();
    }

    public override void OnDestroyEventHandler () {
        base.OnDestroyEventHandler();
        elementTiles.Remove(this);
    }

    public override void SetMaterial( ) {
        if(tile.label.text == Builder.activeElement.ToString()){
            tile.renderer.sharedMaterial = tile.grid.outputMaterial;
        }else{
            tile.renderer.sharedMaterial = tile.grid.inputMaterial;
        }
    }
    
    public override void DeleteStep (bool step) {
        return;
    }
    
	public override void OnMouseEnterCustom (TouchData touch)
	{
		base.OnMouseEnterCustom (touch);
		Builder.instance.selectingElement = true;
	}
	public override void OnMouseExitCustom (TouchData touch)
	{
		base.OnMouseExitCustom (touch);
		Builder.instance.selectingElement = false;
	}
	
    public override void RequestOnConsole () {
        var num6 = Input.GetKey( KeyCode.Alpha6 );
        if(num6 && Gaia.scene > 0){
            onConsole();
        }
    }

    public override void RequestContextualMenu () {
        return;
    }
    
}
