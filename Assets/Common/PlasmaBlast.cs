using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class PlasmaBlast : MonoBehaviour {

    public float energy;
    public Transform particles;
    public PhysicalObject physics;
    public List< AudioClip > clips;
    public Transform initialPosHolder;

    public BulletRigidBody bulletRigidbody;
    public float timer;
    void Start () {
        GetComponent<AudioSource>().clip = clips[ Random.Range( 0, clips.Count ) ];
        GetComponent<AudioSource>().pitch = Random.Range( 0.9f, 1.1f );
        GetComponent<AudioSource>().Play();
        bulletRigidbody.AddOnCollisionDelegate(OnCollision);
        timer = Time.time;
    }

    public void OnCollision ( Vector3 relativeVelocity,BulletRigidBody other,Vector3 contactPoint,Vector3 lineOfAction ) {
        var damage = energy;
        var damageForce = damage * transform.up * 0.25f; //Set to XFM 0.5 value - PL
        other.gameObject.SendMessage( "OnHit", damage * 8.02f, SendMessageOptions.DontRequireReceiver ); //Set to XFM 0.5 value - PL
       
        other.AddForce(damageForce, contactPoint);
        var rotation = Quaternion.LookRotation(Util.FastNormalize( lineOfAction));
        Impact( contactPoint, rotation );
    }

    public void Impact ( Vector3 position, Quaternion rotation ) {
        GameObject.Instantiate( Resources.Load( "FX/BlastImpact" ), position, rotation );
        Destroy(this.gameObject);
    }

    void Update () {
        if((Time.time - timer) > 5f)Impact(transform.position,transform.rotation);
        if ( bulletRigidbody.GetVelocity().sqrMagnitude > 0.5f ) {
            var rotation = Quaternion.LookRotation(bulletRigidbody.GetVelocity() );
            particles.rotation = Quaternion.Slerp( particles.rotation, rotation, Time.deltaTime * 2 );
        }
        GetComponent<AudioSource>().pitch = bulletRigidbody.GetVelocity().magnitude / 150;
    }

    void OnWaterEnter ( Water water ) {
        Impact( transform.position, transform.rotation );
    }
}
