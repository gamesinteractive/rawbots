using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;
using System.Text.RegularExpressions;

public class HexGridTile : MonoBehaviour {

    public Entity entity;
    public new Renderer renderer;
    public Renderer iconRenderer;
    public GameObject ghostTileAsset;
    public GameObject inputLineAsset;
    public GameObject outputLineAsset;
    public GameObject consumeLineAsset;
    public GameObject partLineAsset;
    public TextMesh label;
    public TextMesh value;
    public HexGrid grid;
    public Vector3 tiling;
    public PID positionPID;
    public Vector3 targetTiling;
    public Vector3 targetPosition;
    public Vector3 spawnPosition;
    public Quaternion spawnRotation;
    public Material originalMaterial;
    public HexGridTile ioTarget;
    public HexGridTile actionTarget;
    public HexGridTileTypeHandler handler;
    public GameObject ghostTile;
    public Vector3 localScale;
    public HexGridTileType tileType;
    public Color originalIconColor;
    public Color originalLabelColor;
    public Color selectedLabelColor;

    public static List<string> actions = new List<string>(){"pin"};

    void Start () {
        localScale = transform.localScale;
        originalMaterial = renderer.sharedMaterial;
        originalIconColor = iconRenderer.material.GetColor( "_TintColor" );
        originalLabelColor = label.GetComponent<Renderer>().material.color;

        var etype = default(Entity);
        if ( entity == null ) {
            transform.localPosition = grid.FromTiling( tiling );
            handler = gameObject.AddComponent<HexGridEmptyTile>();
            handler.tile = this;
            handler.Init( etype );
            return;
        }
        entity.ForProperty<float>("pin", value => {
            if (value == 1){
                SetLayer("pinned");
            }
        });
        targetPosition = grid.FromTiling( tiling );
        gameObject.name = entity.id;
        var headAs = entity.Heads( p => p == "as" ).FirstOrDefault();

        if ( headAs == default( Entity ) ) {
            label.text = entity.id;
            switch ( tileType ) {
                case HexGridTileType.HexElement:
                    handler = gameObject.AddComponent<HexElementTile>();
                    break;
                case HexGridTileType.TileAction:
                    handler = gameObject.AddComponent<HexActionTile>();
                    break;
                case HexGridTileType.StructureType:
                    handler = gameObject.AddComponent<HexStructureTile>();
                    break;
                default:
                    handler = gameObject.AddComponent<HexAdderTile>();
                    break;
            }
            handler.tile = this;
            handler.icon = handler.LoadIcon(label.text);
        }
        else {
            label.text = entity.Heads( p => p == "as" ).First().id;
            this.entity.ForInferredProperty< string >( "tile_name", Util.AsIs, tileName => {
                label.text = tileName;
            });

            entity.ForInferredHead( "input", Util.AsIs, head => {
                handler = gameObject.AddComponent<HexInputTile>();
                handler.tile = this;
            } );

            entity.ForInferredHead( "output", Util.AsIs, head => {
                handler = gameObject.AddComponent<HexOutputTile>();
                handler.tile = this;
            } );

            entity.ForInferredHead( "operant", Util.AsIs, head => {
                etype = head;
                handler = gameObject.AddComponent<HexOperantTile>();
                handler.tile = this;
            } );

            entity.ForInferredHead( "part", Util.AsIs, head => {
                etype = head;
                handler = gameObject.AddComponent<HexPartTile>();
                handler.tile = this;
            } );


            entity.ForInferredHead( "structure", Util.AsIs, head => {
                etype = head;
                handler = gameObject.AddComponent<HexPartTile>();
                handler.tile = this;
            } );

            handler.icon = handler.LoadIcon(label.text);
        }
        handler.Init( etype );
    }

    void Update () {
        if ( entity == null ) {
            return;
        }
        positionPID.setpoint = targetPosition;
        transform.localPosition += positionPID.Compute( transform.localPosition, Time.deltaTime ) * Time.deltaTime;
    }

    public void ForGrid ( System.Action<HexGrid> action ) {
        var grids = FindObjectsOfType( typeof( HexGrid ) ) as HexGrid[];
        var activeGrid = grids.FirstOrDefault( g => g.gameObject.activeSelf );
        if ( activeGrid != default(HexGrid) ) {
            action( activeGrid );
        }
    }

    public static HexGridTile Find ( string entityId ) {
        foreach ( var tile in FindObjectsOfType( typeof( HexGridTile ) ) as HexGridTile[] ) {
            if ( tile.entity.id == entityId ) {
                return tile;
            }
        }
        return null;
    }

    public void DestroyTile () {
        if ( tileType != HexGridTileType.Part ) {
            entity.deprecated = true;
        }
        handler.OnDestroyEventHandler();
        grid.tiles.Remove( entity );
        Destroy( gameObject );
    }

    public void Hide () {
        foreach ( var r in GetComponentsInChildren< Renderer >() ) {
            r.enabled = false;
        }
        foreach ( var c in GetComponentsInChildren< Collider >() ) {
            c.enabled = false;
        }
    }

    public void Show () {
        foreach ( var r in GetComponentsInChildren< Renderer >() ) {
            r.enabled = true;
        }
        foreach ( var c in GetComponentsInChildren< Collider >() ) {
            c.enabled = true;
        }
    }

    public void SetLayer(string name){
        var layer = LayerMask.NameToLayer(name);
        gameObject.layer = layer;
        renderer.gameObject.layer = layer;
        iconRenderer.gameObject.layer = layer;
        label.gameObject.layer = layer;
        value.gameObject.layer = layer;
    }
}
