Shader "Rozgo/Terrain" {
    Properties {
  	  _TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
      _RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
      _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
    }
    SubShader {
      Tags { "Queue" = "Geometry" "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf Custom
      struct Input {
          float2 uv_MainTex;
          float2 uv_BumpMap;
          float3 viewDir;
      };
      fixed4 _TintColor;
      sampler2D _MainTex;
      sampler2D _BumpMap;
      float4 _RimColor;
      float _RimPower;
      
		inline fixed4 LightingCustom_PrePass (SurfaceOutput s, half4 light)
		{
		    fixed4 c;
		    c.rgb = s.Albedo * light.rgb;
		    c.a = s.Alpha;
		    return c;
		}
		
		inline fixed4 LightingCustom (SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten)
		{
		    half3 h = normalize ( lightDir + viewDir );
		    fixed diff = max ( 0, dot ( s.Normal, lightDir ) );
		    fixed4 c;
		    c.rgb = (s.Albedo * _LightColor0.rgb * diff) * (atten * 2);
		    c.a = s.Alpha + _LightColor0.a * atten;
		    return c;
		}
      
      void surf (Input IN, inout SurfaceOutput o) {
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb * _TintColor.rgb;
          o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
          half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
          o.Emission = _RimColor.rgb * pow (rim, _RimPower);
      }
      ENDCG
    } 
    Fallback "Diffuse"
}
  
  