using UnityEngine;
using System.Collections;

public class Wavegon : MonoBehaviour {

    public Transform limitTop;

    public float position = 70;
    public float scaleMultiplier = 0.2f;
    public float steper = 0.5f;

    static float step = 0.45f;

	void Start () {
        steper = step;
        step += 0.1f;
        if ( step > 0.55f ) {
            step = 0.45f;
        }
	}
	
	void Update () {
        
        transform.position = transform.forward * position;
        transform.localRotation = Quaternion.identity;
        var scale = Vector3.one * position * scaleMultiplier;
        transform.localScale = scale + Vector3.one * Mathf.Sin( Time.time * steper ) * 0.5f;
        //transform.localScale = scale;

        if ( transform.localPosition.sqrMagnitude > limitTop.localPosition.sqrMagnitude ) {
            GetComponent<Renderer>().enabled = false;
        }
        else {
            GetComponent<Renderer>().enabled = true;
        }
	}
}
