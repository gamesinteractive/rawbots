using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Operants {

    public class NodeProxy : AbstractComponent {

        public bool updates = false;
        public bool fixedUpdates = false;
        public XForm xform;
        public Oracle.Entity entity;
        public NodeView nodeView;
        protected string viewAsset;
        protected List< AbstractComponent > components = new List< AbstractComponent >();
        public Oracle.Entity parentEntity;

        public NodeProxy () : base( null ) {
            Proxy = this;
        }
        
        public T GetComponent< T > () where T : AbstractComponent {
            return GetComponents< T >().FirstOrDefault();
        }
        
        public IEnumerable<T> GetComponents< T > () {
            return components.Concat( new AbstractComponent[] { this } )
                .Where( c => c is T )
                .Cast< T >();
        }

        public void ForView< T > ( ref T view, System.Action action ) where T : MonoBehaviour {
            if ( nodeView != default( NodeView ) ) {
                if ( view == default( T ) ) {
                    view = nodeView.GetComponent< T >();
                    if ( view != default( T ) ) {
                        action();
                    }
                }
                else {
                    action();
                }
            }
        }

//        public void ForView< T > ( System.Action< T > action ) where T : MonoBehaviour {
//            if ( nodeView != default( NodeView ) ) {
//                var view = nodeView.GetComponent< T >();
//                if ( view != default( T ) ) {
//                    action( view );
//                }
//            }
//        }

        public virtual void CreateView () {
            if ( nodeView == null ) {
                this.entity.ForInferredProperty< string >( "view", Util.AsIs, viewAsset => {
                    this.viewAsset = viewAsset;//the next line is what directly loads all physical objects -z26
                    var asset = Resources.Load( viewAsset, typeof( GameObject ) );
                    if ( asset != default( Object ) ) {
                        nodeView = ( GameObject.Instantiate( asset ) as GameObject ).GetComponent< NodeView >();
                        nodeView.name = nodeView.name.Replace( "(Clone)", "(" + entity.id + ")" );
                        nodeView.proxy = this;
                        nodeView.transform.parent = xform.transform;
                        nodeView.transform.localRotation = Quaternion.identity;
                        entity.nodeView = nodeView;
						var bulletRigidbody = nodeView.GetComponent<BulletRigidBody>();
						
                        this.entity.ForProperty< string >( "rotation", rotation => {
                            nodeView.transform.localEulerAngles = Util.PropertyToVector3( rotation );
							bulletRigidbody.SetRotation( nodeView.transform.rotation );
							} );
                        this.entity.ForProperty< string >( "position", position => {
                            nodeView.transform.localPosition = Util.PropertyToVector3( position );
							bulletRigidbody.SetPosition( nodeView.transform.position );
                        } );
                       
                       
//                        var part = bulletRigidbody.GetComponent<Part>();
//                        if ( part != default(Part) ) {
//                            part.subparts.ForEach( sp => {
//                                var rb = sp.GetComponent<BulletRigidBody>();
//                                if ( rb ) {
//                                    rb.SetPosition( sp.transform.position );
//                                    rb.SetRotation( sp.transform.rotation );
//                                }
//
//                            } );
//                        }
                         


                        nodeView.SendMessage( "OnCreateView", this, SendMessageOptions.DontRequireReceiver );
                    }
                } );
            }
        }

        public virtual void OnViewDestroyed () {
            
        }

        public virtual void StorePosition () {

        }


        public override void OnUpdate () {
            for ( var i = 0; i < components.Count; ++i ) {
                components[ i ].OnUpdate();
            }
        }
        
        public override void OnDestroy () { 
            components.ForEach( c => c.OnDestroy() );
            // Console.PushError("Deleting", new string[] { xform.ToString() });
            //GameObject.Destroy(xform);
            //Gaia.DestroyObject(xform);
            //Gaia.Instance().entities.Remove(xform.ToString());
        }
        
        public override void OnPreBuild () {
            parentEntity = entity.Heads( p => p == "local_to" ).FirstOrDefault(); //assign the parent xform
            components.ForEach( c => c.OnPreBuild() ); 
            if ( nodeView ) {//commenting out the line below doesnt seem to break anything -z26
                nodeView.gameObject.SendMessage( "OnPreBuild", SendMessageOptions.DontRequireReceiver );
            }
        }
     
        public override void OnBuild () {
            components.ForEach( c => c.OnBuild() );
            if ( nodeView ) {
                nodeView.gameObject.SendMessage( "OnBuild", SendMessageOptions.DontRequireReceiver );
            }
        }
        
        public override void OnPostBuild () {
            components.ForEach( c => c.OnPostBuild() );
        }

        public override void OnInbox ( Inbox inbox ) {
            components.ForEach( c => c.OnInbox( inbox ) );
        }

        public override void OnInboxDestroy ( Inbox inbox ) {
            components.ForEach( c => c.OnInboxDestroy( inbox ) );
        }

        public override void OnInboxDisconnect ( Inbox inbox ) {
            inbox.Proxy.entity.SetProperty("value",inbox.Recorded);
            components.ForEach( c => c.OnInboxDisconnect( inbox ) );
        }

    }
}
