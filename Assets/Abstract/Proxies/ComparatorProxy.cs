using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

   public class ComparatorProxy : NodeProxy {

        public IOComponent io;
        float pos = 0.0f;
        float neg = 0.0f;
		float hys = 0.0f;
		float op  = 0.0f;

		public ComparatorProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
				inbox.ForQueued< float >( property == "in_+", ( outbox, value ) => pos =  Util.Sum( value ));
				inbox.ForQueued< float >( property == "in_-", ( outbox, value ) => neg =  Util.Sum ( value ));
				inbox.ForQueued< float >( property == "hysteresis", ( outbox, value ) => hys =  Util.Sum ( value ));
            } );
        }

        public override void OnUpdate () {
            base.OnUpdate();

            if (pos > (neg + hys)) {
					op = 1;
			} else if (pos < (neg - hys)) {
					op = 0;
			}

            io.Produce< float >( "out", op );
        }

    }
}
