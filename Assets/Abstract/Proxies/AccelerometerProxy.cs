using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class AccelerometerProxy : NodeProxy {

        PartProxy part;
        IOComponent io;
        Vector3 lastVelocity = Vector3.zero;
        float lowpassFactor = 2;
        Pulse setPulse;
        Quaternion attitude;
        bool setNewAttitude = false;
        GameObject gyroscope;

        public AccelerometerProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
            setPulse = new Pulse();
        }

        public override void OnPreBuild () {
            base.OnPreBuild();

            gyroscope = new GameObject("gyroscope");

            entity.ForInferredProperty< string >( "attitude", Util.AsIs, value => {
                attitude = Quaternion.Euler( Util.PropertyToVector3( value ) );
            } );
            gyroscope.transform.rotation = attitude;

        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< PartProxy >( property == "part_a", ( output, value ) => part = value );
            } );

            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< PartProxy >( property == "part_a", ( output, value ) => part = value );
            } );

            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "set_gyro", ( output, value ) => setPulse.ForPulse( value, () => {
                    setNewAttitude = true;
                } ) );
            } );
        }

        public override void OnUpdate () {
            var pitch = 0f;
            var roll = 0f;
            var yaw = 0f;
            var gravity = 0f;
            var accel_up = 0f;
            var accel_side = 0f;
            var accel_front = 0f;
            if ( part != default(PartProxy) && part.nodeView != default(NodeView) ) {

                if ( setNewAttitude ) {
                    entity.SetProperty( "attitude", Util.Vector3ToProperty( part.nodeView.transform.rotation.eulerAngles ) );
                    attitude = part.nodeView.transform.rotation;
                    gyroscope.transform.rotation = attitude;

                }
                yaw = UpdateGyro(part);

                var physicalObject = part.nodeView.GetComponent<PhysicalObject>();
                var gravityUp = physicalObject.Up();

//                Debug.DrawLine( physicalObject.transform.position, physicalObject.transform.position + physicalObject.transform.up * 10, Color.green );
//                Debug.DrawLine( physicalObject.transform.position, physicalObject.transform.position + physicalObject.transform.right * 10, Color.red );
//                Debug.DrawLine( physicalObject.transform.position, physicalObject.transform.position + physicalObject.transform.forward * 10, Color.blue );

                var sign = Mathf.Sign( Vector3.
                    Dot( gravityUp, physicalObject.transform.up ) );

                //var yawSign = Mathf.Sign( Vector3.Dot( physicalObject.transform.forward, attitude * Vector3.forward ) );
                yaw = UpdateGyro(part);
                pitch = -( Vector3.Angle( physicalObject.transform.forward, gravityUp ) - 90 );
                roll = -( Vector3.Angle( physicalObject.transform.right, gravityUp ) - 90 );

                //yaw = Vector3.Angle( physicalObject.transform.forward, attitude * Vector3.forward ) * yawSign;


                if ( sign < 0 ) {
                    var rollSign = Mathf.Sign( roll );
                    var pitchSign = Mathf.Sign( pitch );
                    roll = rollSign * 90 + ( rollSign * 90 - roll );
                    pitch = pitchSign * 90 + ( pitchSign * 90 - pitch );
                }
                gravity = physicalObject.Gravity().magnitude;
                var velocity = Vector3.Lerp( lastVelocity, physicalObject.bulletRigidbody.GetVelocity(), Time.deltaTime * lowpassFactor );
                var acceleration = ( velocity - lastVelocity ) * 1 / Time.deltaTime;
                lastVelocity = velocity;
                accel_up = Vector3.Dot( acceleration, physicalObject.transform.up );
                accel_side = Vector3.Dot( acceleration, physicalObject.transform.right );
                accel_front = Vector3.Dot( acceleration, physicalObject.transform.forward );
            }
            part = default( PartProxy );
            setNewAttitude = false;
            io.Produce<float>( "pitch", pitch );
            io.Produce<float>( "roll", roll );
            io.Produce<float>( "yaw", yaw );
            io.Produce<float>( "gravity", gravity );
            io.Produce<float>( "accel_up", accel_up );
            io.Produce<float>( "accel_side", accel_side );
            io.Produce<float>( "accel_front", accel_front );
        }

        float UpdateGyro (PartProxy part) {

            var targetPhysical = part.nodeView.GetComponent<PhysicalObject>();
            var targetUp = targetPhysical.Up ();
            //var distanceVector = (transform.position - ghostTarget.transform.position);

            var rotationAdjustment = Quaternion.FromToRotation( gyroscope.transform.up, targetUp );
            gyroscope.transform.position = targetPhysical.transform.position;
            gyroscope.transform.rotation = rotationAdjustment * gyroscope.transform.rotation;
        
            var frontAxis = gyroscope.transform.forward;

            var weirdRight = Vector3.Cross(targetUp,targetPhysical.transform.forward).normalized;
            Debug.DrawLine(targetPhysical.transform.position,targetPhysical.transform.position + weirdRight*2f,Color.red,1f);

            var plannedForward = Vector3.Cross(weirdRight,targetUp).normalized;
            Debug.DrawLine(targetPhysical.transform.position,targetPhysical.transform.position + plannedForward*2f,Color.blue,1f);


            var yawSign = Mathf.Sign( Vector3.Dot( gyroscope.transform.forward,weirdRight ) );


            return Vector3.Angle(gyroscope.transform.forward,plannedForward)*yawSign;



            //return Vector3.Angle(Tra);


//            var toCamera2 = transform.position - ghostTarget.transform.position;
//        
//            var angle = Vector3.Angle( toCamera2.normalized, targetUp );
//        
//            var firstCross = Vector3.Cross( targetUp, toCamera2 ).normalized;
//            var secondCross = Vector3.Cross( firstCross, targetUp ).normalized;
//            Debug.DrawLine( ghostTarget.transform.position, ghostTarget.transform.position + targetUp * 4f );
//            Debug.DrawLine( ghostTarget.transform.position, ghostTarget.transform.position + secondCross * 4f );
//            var thirdCross = Vector3.Cross( secondCross, frontAxis );
//        
//        
//            var calculatedYaw = Vector3.Angle( secondCross, frontAxis );//  acos2 * 180 / Mathf.PI;
//            var filteredFollowYaw = followYaw;
//            if ( filteredFollowYaw < 0 ) {
//                filteredFollowYaw += 360;
//            }
//            var localThirdCross = targetProxy.transform.InverseTransformDirection( thirdCross.normalized );
//            if ( localThirdCross.y > 0 ) {
//                calculatedYaw = 360 - calculatedYaw;
//            }
//            var yawPitchDistance = new Vector3( calculatedYaw, -90 + angle, distanceVector.magnitude );
//            //Debug.Log ("calculatedYaw " + calculatedYaw);
//            return yawPitchDistance;
        
        }




    }



}