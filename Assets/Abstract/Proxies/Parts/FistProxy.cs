using System.Collections;
using UnityEngine;

namespace Operants {

    public class FistProxy : PartProxy {

        Fist fist;

        public FistProxy () { }

        public override void OnUpdate () {
            base.OnUpdate ();
            ForView<Fist> (ref fist, () => io.Produce<float> ("predict_dmg", fist.PredictDamage()));
            ForView<Fist> (ref fist, () => io.Produce<float> ("last_hit", fist.lastHit));
            }
    }
}