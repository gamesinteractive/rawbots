using UnityEngine;
using System;
using System.Collections;

namespace Operants {

    public class SettingsProxy : NodeProxy {

        public IOComponent io;
        string remoteIP = "127.0.0.1";
        int listenPort = 8000;
        int sendPort = 9000;
        bool blockMouseSpawn = false;
        bool blockTerraform = false;

        
        
        public SettingsProxy () {
            io = new IOComponent( this );
            components.Add( io );
            Gaia.Instance().SetupOSC( remoteIP, listenPort, sendPort );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "sound_volume", ( outbox, value ) => {
                } );
                inbox.ForQueued< float >( property == "music_volume", ( outbox, value ) => {
                } );
                inbox.ForQueued< float >( property == "daytime", ( outbox, value ) => {
                    DayCycle.globalTime = Util.Sum(value);
                } );                
                inbox.For< float >( property == "osc_incoming_port", ( outbox, value ) => {
                    var port = ( int )value;
                    if ( port >= 8000 && port < 9000 ) {
                        listenPort = port;
                        Gaia.Instance().SetupOSC( remoteIP, listenPort, sendPort );
                    }
                    else {
                        Console.PushError("osc_incoming_port", new string[]{ "invalid port, use within [8000,8999]" } );
                    }
                } );
                /*Following properties by z26.
                good ideas:
                block mouse drag
                disabled operants and part string list (how to deal with bprinting?)
                meh ideas:
                block creating operants
                block connecting/disconnecting parts
                
                add zones that allow the above at specific restricted points in space.*/
                inbox.For< float >( property == "block_mouse_spawn", ( outbox, value ) => {
                    blockMouseSpawn = Convert.ToBoolean( Mathf.Clamp01( Mathf.Round(value) ));
                    Gaia.Instance().GetComponent<SpawnPartMode>().enabled = !blockMouseSpawn;
                    //disable hooks/bridges too.
                    Gaia.Instance().GetComponent<StructureAddonsManager>().enabled = !blockMouseSpawn;
                } );
                
                inbox.For< float >( property == "block_terraform", ( outbox, value ) => {
                    blockTerraform = Convert.ToBoolean( Mathf.Clamp01( Mathf.Round(value) ));
                    Gaia.Instance().GetComponent<Builder>().enabled = !blockTerraform;
                    Gaia.Instance().GetComponent<StructureAddonsManager>().enabled = !blockMouseSpawn;

                } );
            } );
        }

        void StartHostingSession () {

//             Network.incomingPassword = "";
//            bool useNat = !Network.HavePublicAddress();
            // var useNat = false;
            // Network.InitializeServer( 32, 57668, useNat ); 
        }

    }
}
