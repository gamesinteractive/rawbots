using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Operants {

    public class CameraProxy : PartProxy {

        CameraView camera;

        public CameraProxy () {
        }

        //the methods that produce the outputs are stored in the cameraview class and called by the sensortrigger class.
        //Seems convoluted, maybe the objective was to reuse sensortrigger code with other sensors than the camera. -z26
        public override void OnPreBuild () { 
            base.OnPreBuild();
            ForView< CameraView >( ref camera, () => {
                camera.produceActivity = value => io.Produce( "activity", value );
                camera.produceVisiblePart = value => io.Produce( "detected", value );
            } );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "fov", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetFOV( Util.Sum(value) ) ) );
                inbox.ForQueued< float >( property == "size", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetSize( Util.Sum(value) ) ) );
                inbox.ForQueued< float >( property == "near", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetNearPlane( Util.Sum(value) ) ) );
                inbox.ForQueued< float >( property == "far", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetFarPlane( Util.Sum(value) ) ) );
            } );
        }

    }
 
}
