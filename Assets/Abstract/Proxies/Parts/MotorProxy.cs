using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

    public class MotorProxy : PartProxy {
        
        Motor motor;

        public MotorProxy () {
            updates = false;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();

            if ( nodeView != null ) {
                motor = nodeView.GetComponent< Motor >();
                motor.rotorObject.transform.parent = motor.gameObject.transform.parent;

                motor.producePartAngle = ( angle ) => io.Produce( "angle", angle );
                auxiliar = motor.rotorObject;
            }
        }

        public override void OnPostBuild () {
            base.OnPostBuild();
        }

        public override void OnViewDestroyed () {
            base.OnViewDestroyed();
        }



        public override void StorePosition () {
            ForView< Motor >( ref motor, () => {
                motor.StoreMotorStatus();
            } );
        }

        public override void OnInbox ( Inbox inbox ) {

            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "force", ( outbox, value ) => {
                    ForView< Motor >( ref motor, () => {
                    } );
                } );
                inbox.ForQueued<float>(property == "idle", (outbox, value) => {
                    ForView<Motor>(ref motor, () => {
                        motor.SetIdle(Util.Sum(value));
                    });
                });
                inbox.ForQueued< float >( property == "angle", ( outbox, value ) => {
                    ForView< Motor >( ref motor, () => {
                        motor.SetMotorAngle( Util.Sum( value ) );
                    } );
                } );
                inbox.ForQueued< float >( property == "velocity", ( outbox, value ) => {

                    ForView< Motor >( ref motor, () => {

                        motor.SetMotorVelocity( Util.Sum( value ) );
                    } );
                } );
            } );
        }

        public override void OnInboxDestroy ( Inbox inbox ) {
            base.OnInboxDestroy( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                Util.ForPredicate( property == "angle", () => {
                    ForView< Motor >( ref motor, () => {
                        motor.motor.Stop();

                    } );
                } );
                Util.ForPredicate( property == "velocity", () => {
                    ForView< Motor >( ref motor, () => {
                        motor.motor.Stop();
                    } );
                } );
            } );
        }

        public override void ResetMovablePart () {
            ForView< Motor >( ref motor, () => {
                motor.ResetMotorJoint();
            } );
        }
  
    }
    
}
