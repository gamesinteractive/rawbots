using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Operants {

    public class HydroJetProxy : PartProxy {

        HydroJet hydroJet;

        public HydroJetProxy () {
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "thrust", ( outbox, value ) => {
                    ForView< HydroJet >( ref hydroJet, () => {
                        hydroJet.thrust = Mathf.Max( 0, Util.Sum( value ) );
                        hydroJet.fxWeight = Mathf.Clamp01( Util.MapValue0X( hydroJet.thrust, 1000, 0, 1 ) );
                    } );
                } );
            } );
        }

    }
}
