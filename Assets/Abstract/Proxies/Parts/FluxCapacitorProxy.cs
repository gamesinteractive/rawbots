using UnityEngine;
using System.Collections;
using System.Linq;

namespace Operants {

    public class FluxCapacitorProxy : PartProxy {

        public FluxCapacitor fluxCapacitor;

        public override void OnPreBuild () {
            base.OnPreBuild();
             if ( nodeView != null ) {
                fluxCapacitor = nodeView.GetComponent<FluxCapacitor>();
            }      
        }


        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "capacitance", ( outbox, value ) => {
                        fluxCapacitor.SetLevel( (int) Util.Sum( value ) );
                } );
            } );
        }

        public override void OnUpdate () {
            base.OnUpdate();
            io.Produce( "charge", fluxCapacitor.charge );
        }



    }
}
