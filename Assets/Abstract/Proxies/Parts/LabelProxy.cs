using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Operants {

    public enum FontSize{
        VerySmall,
        Small,
        Medium,
        Large,
        VeryLarge,
    }

    public class LabelProxy : NodeProxy {
        IOComponent io;
        float[] sizes = new[]{0.4f ,0.6f, 0.9f, 1.35f,2.05f};
        public string text = string.Empty;
        public Vector3 position = Vector3.zero;
        public Color color = Color.gray;
        public Vector3 size;
        public LabelProxy () {
            size = Vector3.one * sizes[(int)FontSize.Medium];
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }
        
        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< string >( property == "text", ( outbox, value ) =>  text = value);
                inbox.For< string >( property == "position", ( outbox, value ) => ValidatePosition(value) );
                inbox.For< string >( property == "size", ( outbox, value ) => {
                    var multiplier = ( FontSize )System.Enum.Parse( typeof( FontSize ), value.Split( '.' ).Last() );
                    size = Vector3.one * sizes[(int)multiplier];
                } );
                inbox.For< Color >( property == "color", ( outbox, value ) => color = value );
            } );
        }


        public void ValidatePosition(string value){
            char[] separator = {',',' ','[',']'};
            char[] brackets = {'[',']'};
            var values = value.Trim(brackets).Split(separator);
            float x = 0;
            float y = 0;
            float z = 0;
            bool success = false;

            if (values.Count() == 3) { success = true; }
            success = success && Single.TryParse(values[0], out x);
            success = success && Single.TryParse(values[1], out y);
            success = success && Single.TryParse(values[2], out z);

            if(success){
                position.x = Mathf.Clamp(x, -3 , 3)  * 4;
                position.y = Mathf.Clamp(y, -3 , 3) * 4;
                var temp = Mathf.Clamp( z , -3 ,3 );
                temp /= 2;
                position.z = position.y;
                position.y = temp;
            }else{
                Console.PushError("position",new string[]{"invalid format use: 0.0, 0.0, 0.0" });
            }
        }

        public override void OnUpdate () {
            io.Produce<NodeProxy>("label",this);
        }
        
    }
    
}
