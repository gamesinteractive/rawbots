using UnityEngine;
using System.Collections;
using System.Linq;

namespace Operants {

    public class PointLightProxy : PartProxy {

        PointLight pointLight;

        public PointLightProxy () {
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "intensity", ( outbox, value ) =>
                    ForView<PointLight>( ref pointLight, () => pointLight.SetIntensity( Util.Sum( value ) ) ) );
                inbox.For< Color >( property == "color", ( outbox, value ) =>
                    ForView<PointLight>( ref pointLight, () => pointLight.SetTintColor( value ) ) );
            } );
        }

        public override void OnUpdate () {
            base.OnUpdate();
            ForView< PointLight >( ref pointLight, () => io.Produce<float>( "intensity", pointLight.intensity ) );
        }

    }
}
