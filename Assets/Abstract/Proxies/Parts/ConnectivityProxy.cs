using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Oracle;

namespace Operants {

    public class ConnectivityProxy : NodeProxy {

        IOComponent io;
        PartProxy proxyPartA;
        PartProxy proxyPartB;
        string previousFilter = "all -subpart";
        List<string> filterParams;
        

        public ConnectivityProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            //Filter params must be generated, even if the filter input tile doen't exist.
            filterParams = PartFilter.ProcessInput(previousFilter);
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< PartProxy >( property == "part_a", ( output, value ) => proxyPartA = value );
                inbox.For< PartProxy >( property == "part_b", ( output, value ) => proxyPartB = value );
                inbox.For< string >(property == "filter", ( output, value ) => {
                    if(value != previousFilter) {
                        filterParams = PartFilter.ProcessInput(value);
                        previousFilter = value;
                    }
                } );
            } );
        }

        public override void OnUpdate () {
            var isConnected = false;
            var assembly_a = (proxyPartA != null)? proxyPartA.PhysicallyLinked() : null;
            if ( proxyPartA != null && proxyPartB != null ) {
                isConnected = assembly_a.Contains(proxyPartB);
            }
            
            if(assembly_a != null) {
                assembly_a = PartFilter.Apply(assembly_a, filterParams);
            }
            
            io.Produce< float >( "connectivity", isConnected ? 1 : 0 );
            io.Produce< float >( "partcount_a", (assembly_a != null)?  assembly_a.Count() : 0);
        }
        
    }
}
