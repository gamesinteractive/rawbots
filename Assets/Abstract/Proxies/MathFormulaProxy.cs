﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    /*By z26.
    //math parser found here. https://github.com/akuukka/ExpressionSolver 
    I modified it so that it can warn about bad formulas without throwing an exception.*/

    public class MathFormulaProxy : NodeProxy {
        public IOComponent io;
        float a;
        float b;
        float c;
        float d;
		string lastEnteredFormula;
		AK.ExpressionSolver solver = new AK.ExpressionSolver();
        AK.Expression exp;

        public MathFormulaProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;

        //Instead of throwing an exception, variables that doesnt exist are set to 0.
            solver.undefinedVariablePolicy = AK.ExpressionSolver.UndefinedVariablePolicy.DefineExpressionLocalVariable;
        }
        
        public override void OnPreBuild () {
            base.OnPreBuild();
        }
        
        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "in_a", ( outbox, value ) => a =  Util.Sum( value ));
                inbox.ForQueued< float >( property == "in_b", ( outbox, value ) => b =  Util.Sum ( value ));
                inbox.ForQueued< float >( property == "in_c", ( outbox, value ) => c =  Util.Sum( value ));
                inbox.ForQueued< float >( property == "in_d", ( outbox, value ) => d =  Util.Sum ( value ));
				inbox.For< string >( property == "formula", ( outbox, value ) => CompileFormula( value ) );
            } );
        }

		void CompileFormula (string value) {
            

			if(value != lastEnteredFormula) {

                lastEnteredFormula = value;

                var variables = new string[]{"a","b","c","d"};
				var newExp = solver.SymbolicateExpression(value, variables);

                if(newExp.errorWarning == null) {
                    exp = newExp;
                } else {
                    //I've made it so that bad formulas output NaN. Failing loudly should make it easier to find problems.
                    exp = solver.SymbolicateExpression("NaN");
                    Console.PushError("formula",new string[]{newExp.errorWarning});
                }

			}
		}

        public override void OnUpdate () {
            base.OnUpdate();    
            var r = 0.0f;       
			if(exp != null) {
				exp.SetVariable("a",(double)a);
				exp.SetVariable("b",(double)b);
				exp.SetVariable("c",(double)c);
				exp.SetVariable("d",(double)d);
				r = (float)exp.Evaluate();
			}
            io.Produce< float >( "out", r );
            io.Produce< string >( "string", r.ToString() );
        }
    }

}
