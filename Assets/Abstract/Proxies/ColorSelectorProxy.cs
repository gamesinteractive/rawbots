using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class ColorSelectorProxy : NodeProxy {

        public IOComponent io;

        Color[] colors = new Color[16];
        int selector = 0;

        public ColorSelectorProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            selector = 0;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< float >( property == "selector", ( outbox, value ) => selector = Mathf.RoundToInt( Mathf.Clamp( value, 0f, 15f ) ) );
                inbox.For< Color >( property == "color_0", ( outbox, value ) => colors[0] = value );
                inbox.For< Color >( property == "color_1", ( outbox, value ) => colors[1] = value );
                inbox.For< Color >( property == "color_2", ( outbox, value ) => colors[2] = value );
                inbox.For< Color >( property == "color_3", ( outbox, value ) => colors[3] = value );
                inbox.For< Color >( property == "color_4", ( outbox, value ) => colors[4] = value );
                inbox.For< Color >( property == "color_5", ( outbox, value ) => colors[5] = value );
                inbox.For< Color >( property == "color_6", ( outbox, value ) => colors[6] = value );
                inbox.For< Color >( property == "color_7", ( outbox, value ) => colors[7] = value );
                inbox.For< Color >( property == "color_8", ( outbox, value ) => colors[8] = value );
                inbox.For< Color >( property == "color_9", ( outbox, value ) => colors[9] = value );
                inbox.For< Color >( property == "color_10", ( outbox, value ) => colors[10] = value );
                inbox.For< Color >( property == "color_11", ( outbox, value ) => colors[11] = value );
                inbox.For< Color >( property == "color_12", ( outbox, value ) => colors[12] = value );
                inbox.For< Color >( property == "color_13", ( outbox, value ) => colors[13] = value );
                inbox.For< Color >( property == "color_14", ( outbox, value ) => colors[14] = value );
                inbox.For< Color >( property == "color_15", ( outbox, value ) => colors[15] = value );
            } );
        }

        public override void OnUpdate () {
            io.Produce< Color >( "color", colors[selector]);
        }

    }
}
