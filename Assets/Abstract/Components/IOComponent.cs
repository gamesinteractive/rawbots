using UnityEngine;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class IOComponent : AbstractComponent {

        public List< Outbox > outboxes = new List< Outbox >();

        public IOComponent ( NodeProxy proxy ) : base( proxy ) {
        }
        
        public override void OnPostBuild () {
            base.OnPostBuild();
            outboxes = new List< Outbox >();
            foreach ( var output in Outputs() ) {
                var outbox = output.GetComponent< Outbox >();
                outbox.ClearActions();
                output.entity.ForInferredProperty< string >( "property", Util.AsIs, p => {
                    outbox.property = p;
                } );
                outbox.inboxes = new List< Inbox >();
                var inputs = output.entity.Tails( p => p == "consumes" ).Select( ie => ie );
                foreach ( var input in inputs ) {
                    if ( input.proxy == null ) {
                        Debug.Log( "IO input: " + input.id + " has no proxy" );
                        input.deprecated = true;
                    }
                    else {
                        outbox.inboxes.Add( input.proxy.GetComponent< Inbox >() );
                    }
                }
                outboxes.Add( outbox );
            }
            foreach ( var input in Inputs() ) {
                var inbox = input.GetComponent< Inbox >();
                inbox.ClearActions();
                Proxy.OnInbox( inbox );
            }
        }

        public IEnumerable< NodeProxy > Inputs () {
            return Proxy.entity.Tails( p => p == "input_for" ).Select( e => e.proxy ).Where( p => p != null );
        }
    
        public IEnumerable< NodeProxy > Outputs () {
            return Proxy.entity.Tails( p => p == "output_for" ).Select( e => e.proxy ).Where( p => p != null );
        }

        public void Produce< T > ( string property, T value ) {
            for ( var i = 0; i < outboxes.Count; ++i ) {
                var outbox = outboxes[ i ];
                if ( outbox.property == property ) {
                    outbox.Produce< T >( value );
                }
            }
        }

    }
}
