using UnityEngine;
using System.Collections;
using System;
public class HookPoint : MonoBehaviour
{


	public enum Location
	{
		TopHex,
		SideHex,
		BottomHex,
		Addon,
		CenterHex
	}

	[Flags]
	public enum AllowedHex
	{
		Hex            	= 1 << 0,
		Ramp       		= 1 << 1,
		TruncatedRamp   = 1 << 2,
		HexNotRamp = Hex | Ramp | TruncatedRamp,
		HalfRamp       	= 1 << 3,
		Addon  			= 1 << 4
	}
	
	
	public HookElement allowedElement = HookElement.All;
	private  BulletLayers ghostStructureMask;
	private BulletLayers structureMask;
	public GameObject ghost;
	public AllowedHex allowedHex = AllowedHex.HexNotRamp ;
	public Location location = Location.SideHex;
	void Start ()
	{
		ghostStructureMask = BulletLayers.GhostStructure;
		structureMask = BulletLayers.Camera ;
	}

	public GameObject ShowGhost (bool on)
	{
		if (on) {

			var direction = Vector3.up;
			var position = Vector3.zero;
			var earthType = "none";
			var rayReach = 5f;
			var nodeView = transform.parent.GetComponent<StructureHooks> ().nodeView;
			var offset = Vector3.zero;
			var rotation = nodeView.transform.rotation;

			switch (location) {
			case Location.SideHex:
				position = transform.position + transform.right * 40.64101f * 0.25f;
				earthType = "Ghosts/" + Builder.instance.currentOption.entityName;
				direction = transform.right;
				rayReach = 60f*0.25f;
				break;


			case Location.TopHex:
				position = transform.position + transform.up * 21f * 0.25f;
				earthType = "Ghosts/" + Builder.instance.currentOption.entityName;
				direction = transform.up;
				rayReach = 30f*0.25f;
				break;

			case Location.BottomHex:
				position = transform.position + transform.up * -21f * 0.25f;
				earthType = "Ghosts/" + Builder.instance.currentOption.entityName;
				rayReach = 30f*0.25f;
				direction = transform.up * -1f;
				break;
			case Location.CenterHex:
				position = transform.position;// + transform.up * -21f * 0.25f;
				earthType = "Ghosts/" + Builder.instance.currentOption.entityName;
				rayReach = 30f*0.25f;
				direction = transform.up * -1f;
				break;


			case Location.Addon:
				position = transform.position;
				rotation = transform.rotation;
				earthType = "Ghosts/" + StructureAddonsManager.addOnType;
				direction = transform.up;
				rayReach = 10f*0.25f;
				break;
				
			default:
				break;
			}
			

			var ray = new Ray (transform.position - direction * 1f + offset, direction);
			//Debug.DrawLine( ray.origin, ray.origin + ray.direction * rayReach, Color.red, 5f );
			BulletRaycastHitDetails hit;
			var result = Bullet.Raycast (ray, out hit, rayReach, BulletLayers.Camera, (BulletLayers.World | BulletLayers.HexBlock | BulletLayers.Structure | BulletLayers.Water)); //added structure and water mask -z26
			GetComponent<Renderer>().material.color = Color.blue;//(the later is because water hexes could otherwise overlap terrain hexes when spawned)
			if (!result) {
				ghost = (GameObject)Instantiate (Resources.Load (earthType));
				ghost.transform.position = position;
				ghost.transform.rotation = rotation;
				GetComponent<Renderer>().material.color = Color.red;
			} else {
				//Debug.Log(  hit.bulletRigidbody.transform.name + " "+ Time.time);
			}

		} else {
			Destroy (ghost);
			GetComponent<Renderer>().material.color = Color.white;
		}
		return ghost;


	}


}
