using UnityEngine;
using System.Collections;

public class VolcanicDamage : MonoBehaviour {
    public BulletRigidBody body;
    float damage = 7;
    float lastFXInstantiated = 0;
    void Start(){
        body.AddOnCollisionDelegate(OnCollision);
    }


    public void OnCollision (Vector3 relativeVelocity,BulletRigidBody other,Vector3 contactPoint,Vector3 lineOfAction){
        if(other.collisionGroup == BulletLayers.Camera){
            return;
        }
        other.SendMessage("OnHit",damage,SendMessageOptions.DontRequireReceiver);
        if(Time.time - lastFXInstantiated > UnityEngine.Random.Range(0.1f,0.3f)){
            var fx = Instantiate (Resources.Load ("FX/HitFeedback", typeof(GameObject)), contactPoint, Quaternion.identity) as GameObject;
            fx.transform.parent = Gaia.Instance().scratch;
            fx.hideFlags = HideFlags.HideInHierarchy;
            fx.transform.localScale *= damage * 0.05f;
            fx.transform.forward = Util.FastNormalize (lineOfAction);
            fx.GetComponent<AudioSource>().volume = 0.5f;
            fx.GetComponent<AudioSource>().pitch = Random.Range (0.8f, 3f);
            lastFXInstantiated = Time.time;
        }
    }

}
