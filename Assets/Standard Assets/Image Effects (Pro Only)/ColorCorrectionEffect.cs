using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[AddComponentMenu("Image Effects/Color Correction (Ramp)")]
public class ColorCorrectionEffect : ImageEffectBase {
	public Texture rampNoGrid;
	public Texture rampGrid;
	public bool grid = false;

	// Called by camera to apply image effect
	void OnRenderImage (RenderTexture source, RenderTexture destination) {
		if (grid) {
			material.SetTexture ("_RampTex", rampGrid);
		} else {
			material.SetTexture ("_RampTex", rampNoGrid);
		}

		Graphics.Blit (source, destination, material);
	}
}
